package my.java;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

import myjava.AbstractMethod;
import myjava.BinaryOperator;
import myjava.Call;
import myjava.Callable;
import myjava.Cast;
import myjava.ClassFormalParameter;
import myjava.ClassParameterType;
import myjava.ClassType;
import myjava.Clazz;
import myjava.ConcreteMethod;
import myjava.Conditional;
import myjava.Different;
import myjava.EllipsisType;
import myjava.Equals;
import myjava.Eval;
import myjava.Expression;
import myjava.Field;
import myjava.FormalParameter;
import myjava.GenClassClazz;
import myjava.GenOperationCallable;
import myjava.GeneratedClass;
import myjava.GetGenFeatureCallable;
import myjava.IfThenElse;
import myjava.InstanceOf;
import myjava.JavaMethodCallable;
import myjava.Lambda;
import myjava.LocalVariable;
import myjava.Member;
import myjava.Method;
import myjava.NamedClass;
import myjava.NewObject;
import myjava.Null;
import myjava.PrimitiveType;
import myjava.Reference;
import myjava.ReifiedEClass;
import myjava.ReifiedEStructuralFeature;
import myjava.Return;
import myjava.Statement;
import myjava.StringLiteral;
import myjava.This;
import myjava.Throw;
import myjava.Type;
import myjava.Unit;
import myjava.Visibility;

public class TextGenerator {

	public static void writeToFile(IFolder root, GeneratedClass c) {
		String[] fqn = c.getName().split("\\.");
		if (fqn.length >= 1) {
			try {
				IFolder cur = root;
				for (int i = 0; i < fqn.length - 1; ++i) {
					cur = cur.getFolder(fqn[i]);
					if (!cur.exists()) {
						cur.create(false, false, null);
					}
				}
				IFile file = cur.getFile(fqn[fqn.length - 1] + ".java");
				PipedInputStream i = new PipedInputStream();
				PipedOutputStream o = new PipedOutputStream(i);
				Thread t = new Thread(() -> {
					System.out.println("dumping the file");
					try (BoxWriter b = new BoxWriter(new OutputStreamWriter(o, Charset.forName(file.getCharset())))) {
						TextGenerator.out(b, c);
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
				t.start();
				if (file.exists()) {
					file.setContents(i, false, false, null);
				} else {
					file.create(i, false, null);
				}
				t.join();
				file.setDerived(true, null);
			} catch (InterruptedException | IOException | CoreException e) {
				e.printStackTrace();
			}
			try {
				root.refreshLocal(IResource.DEPTH_INFINITE, null);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
	}

	public static void writeToFile(IFolder root, Unit u) {
		u.getImports().stream().filter(x -> x instanceof GeneratedClass)
				.forEach(x -> writeToFile(root, (GeneratedClass) x));
	}

	public static void out(BoxWriter w, GeneratedClass c) throws Exception {
		String name = c.getName();
		int pos = name.lastIndexOf('.');
		if (pos >= 0) {
			w.write("package").sp().write(name.substring(0, pos)).write(';').nl();
		}
		w.v(2).hov(4);
		out(w, c.getVisibility());
		if (c.isAbstract()) {
			w.sp().write("abstract");
		}
		w.sp().write("class").sp().write(name.substring(pos + 1));
		if (!c.getFormalParameters().isEmpty()) {
			forEachSep(c.getFormalParameters(), () -> w.sp().write('<').hov(), i -> out(w, i), () -> w.write(',').sp(),
					() -> w.e().write('>'));
		}
		w.sp().write('{').e().nl();
		for (Member m : c.getMembers()) {
			w.nl();
			out(w, m);
		}
		w.e().nl().write('}').nl();
	}

	public static void out(BoxWriter w, ClassFormalParameter p) throws Exception {
		w.write(p.getName());
		if (!p.getExtends().isEmpty()) {
			forEachSep(p.getExtends(), () -> w.sp().write("extends").sp().hov(), i -> out(w, i),
					() -> w.sp().write('&').sp(), () -> w.e());
		}
	}

	public static void out(BoxWriter w, Member m) throws Exception {
		if (m instanceof ConcreteMethod) {
			out(w, (ConcreteMethod) m);
		} else if (m instanceof AbstractMethod) {
			out(w, (AbstractMethod) m);
		} else if (m instanceof Field) {
			out(w, (Field) m);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static void out(BoxWriter w, ConcreteMethod m) throws Exception {
		w.v(0).v(2).hov(4);
		memberPrologue(w, m);
		methodSignature(w, m);
		w.sp().write('{').e();
		for (Statement s : m.getBody()) {
			w.nl();
			out(w, s);
		}
		w.e().nl().write('}').e();
	}

	public static void out(BoxWriter w, AbstractMethod m) throws Exception {
		w.hov(4);
		memberPrologue(w, m);
		w.write("abstract").sp();
		methodSignature(w, m);
		w.write(';').e();
	}

	private static void methodSignature(BoxWriter w, Method m) throws Exception, IOException {
		out(w, m.getReturnType());
		w.sp().write(m.getName());
		formalParameters(w, m.getFormalParameters());
		forEachSep(m.getExceptions(), () -> w.sp().write("throws").sp().hov(0), x -> out(w, x), () -> w.write(',').sp(),
				() -> w.e());
	}

	private static void formalParameters(BoxWriter w, List<FormalParameter> l) throws Exception {
		w.write('(');
		forEachSep(l, p -> {
			if (p.isFinal()) {
				w.write("final").sp();
			}
			out(w, p.getType());
			w.sp().write(p.getName());
		}, () -> w.write(',').sp());
		w.write(')');
	}

	public static void out(BoxWriter w, Field f) throws Exception {
		w.hov(4);
		memberPrologue(w, f);
		out(w, f.getType());
		w.sp().write(f.getName());
		if (f.getInitialization() != null) {
			w.sp().write('=').sp();
			out(w, f.getInitialization());
		}
		w.write(';').e();
	}

	private static void memberPrologue(BoxWriter w, Member m) throws Exception, IOException {
		out(w, m.getVisibility());
		if (m.isFinal()) {
			w.sp().write("final");
		}
		w.sp();
	}

	public static void out(BoxWriter w, Statement s) throws Exception {
		if (s instanceof IfThenElse) {
			out(w, (IfThenElse) s);
		} else if (s instanceof Throw) {
			out(w, (Throw) s);
		} else if (s instanceof Eval) {
			out(w, (Eval) s);
		} else if (s instanceof Return) {
			out(w, (Return) s);
		} else if (s instanceof LocalVariable) {
			out(w, (LocalVariable) s);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static void out(BoxWriter w, IfThenElse i) throws Exception {
		w.v(0).v(2).hov(4);
		doIfThenElse(w, i);
	}

	private static void doIfThenElse(BoxWriter w, IfThenElse i) throws IOException, Exception {
		w.write("if").sp().write('(');
		out(w, i.getCondition());
		w.write(')').sp().write('{').e();
		for (Statement s : i.getThen()) {
			w.nl();
			out(w, s);
		}
		w.e().nl();
		if (!i.getElse().isEmpty()) {
			w.v(2).hov(4).write('}').sp().write("else").sp();
			if (i.getElse().size() == 1 && i.getElse().get(0) instanceof IfThenElse) {
				doIfThenElse(w, (IfThenElse) i.getElse().get(0));
			} else {
				w.write('{').e();
				for (Statement s : i.getElse()) {
					w.nl();
					out(w, s);
				}
				w.e().nl().write('}').e();
			}
		} else {
			w.write('}').e();
		}
	}

	public static void out(BoxWriter w, Throw i) throws Exception {
		w.hov(2).write("throw").sp();
		out(w, i.getValue());
		w.write(';').e();
	}

	public static void out(BoxWriter w, Return i) throws Exception {
		w.hov(2).write("return").sp();
		out(w, i.getValue());
		w.write(';').e();
	}

	public static void out(BoxWriter w, Eval e) throws Exception {
		w.hov(2);
		out(w, e.getExpression());
		w.write(';').e();
	}

	public static void out(BoxWriter w, LocalVariable e) throws Exception {
		w.hov(2);
		if (e.isFinal()) {
			w.write("final").sp();
		}
		out(w, e.getType());
		w.sp().write(e.getName());
		if (e.getValue() != null) {
			w.sp().write('=').sp();
			out(w, e.getValue());
		}
		w.write(';').e();
	}

	public static void out(BoxWriter w, Expression e) throws Exception {
		out(w, e, 0);
	}

	private static final int PREC_LAMBDA = 50;
	private static final int PREC_CONDITIONAL = 100;
	private static final int PREC_RELATIONAL = 180;
	private static final int PREC_INSTANCEOF = 190;
	private static final int PREC_CAST = 1000;
	private static final int PREC_NAME = Integer.MAX_VALUE;
	private static final int PREC_NEW = Integer.MAX_VALUE;
	private static final int PREC_CALL = Integer.MAX_VALUE;
	private static final int PREC_THIS = Integer.MAX_VALUE;
	private static final int PREC_NULL = Integer.MAX_VALUE;
	private static final int PREC_LITERAL = Integer.MAX_VALUE;
	private static final int PREC_REIFIEDECLASS = Integer.MAX_VALUE;
	private static final int PREC_REIFIEDESTRUCTURALFEATURE = Integer.MAX_VALUE;

	public static void out(BoxWriter w, Expression e, int level) throws Exception {
		if (e instanceof InstanceOf) {
			p(w, (InstanceOf) e, level > PREC_INSTANCEOF, TextGenerator::p);
		} else if (e instanceof Reference) {
			p(w, (Reference) e, level > PREC_NAME, TextGenerator::p);
		} else if (e instanceof NewObject) {
			p(w, (NewObject) e, level > PREC_NEW, TextGenerator::p);
		} else if (e instanceof Call) {
			p(w, (Call) e, level > PREC_CALL, TextGenerator::p);
		} else if (e instanceof This) {
			p(w, (This) e, level > PREC_THIS, TextGenerator::p);
		} else if (e instanceof Null) {
			p(w, (Null) e, level > PREC_NULL, TextGenerator::p);
		} else if (e instanceof StringLiteral) {
			p(w, (StringLiteral) e, level > PREC_LITERAL, TextGenerator::p);
		} else if (e instanceof ReifiedEClass) {
			p(w, (ReifiedEClass) e, level > PREC_REIFIEDECLASS, TextGenerator::p);
		} else if (e instanceof Equals) {
			p(w, (BinaryOperator) e, level > PREC_RELATIONAL,
					(y, o) -> leftAssociativeBinOp(y, o, "==", PREC_RELATIONAL));
		} else if (e instanceof Different) {
			p(w, (BinaryOperator) e, level > PREC_RELATIONAL,
					(y, o) -> leftAssociativeBinOp(y, o, "!=", PREC_RELATIONAL));
		} else if (e instanceof Cast) {
			p(w, (Cast) e, level > PREC_CAST, TextGenerator::p);
		} else if (e instanceof Conditional) {
			p(w, (Conditional) e, level > PREC_CONDITIONAL, TextGenerator::p);
		} else if (e instanceof ReifiedEStructuralFeature) {
			p(w, (ReifiedEStructuralFeature) e, level > PREC_REIFIEDESTRUCTURALFEATURE, TextGenerator::p);
		} else if (e instanceof Lambda) {
			p(w, (Lambda) e, level > PREC_LAMBDA, TextGenerator::p);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static <T extends Expression> void p(BoxWriter w, T e, boolean paren, ThrowingBiConsumer<BoxWriter, T> c)
			throws Exception {
		if (paren) {
			w.write('(').hov(0);
		}
		c.accept(w, e);
		if (paren) {
			w.e().write(')');
		}
	}

	public static void p(BoxWriter w, InstanceOf e) throws Exception {
		out(w, e.getLeft(), PREC_INSTANCEOF);
		w.sp().write("instanceof").sp().write(name(e.getType()));
	}

	public static void p(BoxWriter w, Reference e) throws Exception {
		w.write(e.getBinder().getName());
	}

	public static void p(BoxWriter w, NewObject e) throws Exception {
		w.write("new").sp();
		out(w, e.getClazz());
		w.write('(');
		forEachSep(e.getEffectiveParameters(), () -> w.hov(0), i -> out(w, i), () -> w.write(',').sp(), () -> w.e());
		w.write(')');
	}

	public static void p(BoxWriter w, Call e) throws Exception {
		out(w, e.getTarget(), PREC_CALL);
		w.write('.');
		out(w, e.getMethod());
		w.write('(').hov();
		forEachSep(e.getEffectiveParameters(), i -> out(w, i), () -> w.write(',').sp());
		w.e().write(')');
	}

	public static void out(BoxWriter w, Callable c) throws Exception {
		if (c instanceof Method) {
			out(w, (Method) c);
		} else if (c instanceof GetGenFeatureCallable) {
			out(w, (GetGenFeatureCallable) c);
		} else if (c instanceof GenOperationCallable) {
			out(w, (GenOperationCallable) c);
		} else if (c instanceof JavaMethodCallable) {
			out(w, (JavaMethodCallable) c);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static void out(BoxWriter w, Method m) throws Exception {
		w.write(m.getName());
	}

	public static void out(BoxWriter w, GetGenFeatureCallable m) throws Exception {
		w.write(m.getFeature().getGetAccessor());
	}

	public static void out(BoxWriter w, GenOperationCallable m) throws Exception {
		w.write(m.getOperation().getName());
	}

	public static void out(BoxWriter w, JavaMethodCallable m) throws Exception {
		w.write(m.getSpecifier());
	}

	public static void p(BoxWriter w, This e) throws Exception {
		w.write("this");
	}

	public static void p(BoxWriter w, Null e) throws Exception {
		w.write("null");
	}

	public static void p(BoxWriter w, StringLiteral e) throws Exception {
		w.write('"').write(escape(e.getValue())).write('"');
	}

	public static StringBuilder escape(CharSequence s) {
		StringBuilder r = new StringBuilder();
		for (char i : s.toString().toCharArray()) {
			if (i == '"') {
				r.append("\\\"");
			} else if (i == '\'') {
				r.append("\\'");
			} else if (i == '\\') {
				r.append("\\\\");
			} else if (i == '\n') {
				r.append("\\n");
			} else if (i == '\t') {
				r.append("\\t");
			} else {
				r.append(i);
			}
		}
		return r;
	}

	public static void p(BoxWriter w, ReifiedEClass e) throws Exception {
		w.write(e.getGenClass().getQualifiedClassifierAccessor());
	}

	public static void p(BoxWriter w, Cast e) throws Exception {
		w.write('(');
		out(w, e.getType());
		w.write(')');
		out(w, e.getExpression(), PREC_CAST);
	}

	public static void leftAssociativeBinOp(BoxWriter w, BinaryOperator e, String op, int level) throws Exception {
		out(w, e.getLeft(), level);
		w.sp().write(op).sp();
		out(w, e.getRight(), level + 1);
	}

	public static void p(BoxWriter w, Conditional e) throws Exception {
		out(w, e.getCondition(), PREC_CONDITIONAL + 1);
		w.sp().write('?').sp();
		out(w, e.getThen(), PREC_CONDITIONAL + 1);
		w.sp().write(':').sp();
		out(w, e.getElse(), PREC_CONDITIONAL);
	}

	public static void p(BoxWriter w, ReifiedEStructuralFeature e) throws Exception {
		w.write(e.getFeature().getQualifiedFeatureAccessor());
	}

	public static void p(BoxWriter w, Lambda e) throws Exception {
		w.hov(2);
		formalParameters(w, e.getParameters());
		w.sp().write("->").sp();
		out(w, e.getBody(), PREC_LAMBDA);
		w.e();
	}

	public static void out(BoxWriter w, Type t) throws Exception {
		if (t instanceof PrimitiveType) {
			out(w, (PrimitiveType) t);
		} else if (t instanceof ClassType) {
			out(w, (ClassType) t);
		} else if (t instanceof ClassParameterType) {
			out(w, (ClassParameterType) t);
		} else if (t instanceof EllipsisType) {
			out(w, (EllipsisType) t);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static void out(BoxWriter w, PrimitiveType t) throws IOException {
		w.write(t.getType().getName().toLowerCase());
	}

	public static void out(BoxWriter w, ClassType t) throws Exception {
		w.write(name(t.getClazz()));
		if (!t.getEffectiveParameters().isEmpty()) {
			forEachSep(t.getEffectiveParameters(), () -> w.write('<').hov(0), i -> out(w, i), () -> w.write(',').sp(),
					() -> w.e().write('>'));
		}
	}

	public static void out(BoxWriter w, ClassParameterType t) throws IOException {
		w.write(t.getParameter().getName());
	}

	public static void out(BoxWriter w, EllipsisType t) throws Exception {
		out(w, t.getType());
		w.write("...");
	}

	private static <T> void forEachSep(Iterable<T> it, ThrowingConsumer<T> h, ThrowingRunnable sep) throws Exception {
		forEachSep(it.iterator(), ThrowingRunnable.NOP, h, sep, ThrowingRunnable.NOP);
	}

	private static <T> void forEachSep(Iterable<T> it, ThrowingRunnable beg, ThrowingConsumer<T> h,
			ThrowingRunnable sep, ThrowingRunnable end) throws Exception {
		forEachSep(it.iterator(), beg, h, sep, end);
	}

	private static <T> void forEachSep(Iterator<T> i, ThrowingRunnable beg, ThrowingConsumer<T> h, ThrowingRunnable sep,
			ThrowingRunnable end) throws Exception {
		if (i.hasNext()) {
			beg.run();
			h.accept(i.next());
			while (i.hasNext()) {
				sep.run();
				h.accept(i.next());
			}
			end.run();
		}
	}

	private static String name(Clazz c) {
		if (c instanceof NamedClass) {
			return ((NamedClass) c).getName();
		} else if (c instanceof GenClassClazz) {
			return ((GenClassClazz) c).getGenClass().getQualifiedInterfaceName();
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static void out(BoxWriter w, Visibility v) throws IOException {
		switch (v) {
		case DEFAULT:
			return;
		case PUBLIC:
			w.write("public");
			return;
		case PROTECTED:
			w.write("protected");
			return;
		case PRIVATE:
			w.write("private");
			return;
		}
	}
}
