package my.java;

@FunctionalInterface
public interface ThrowingRunnable {
	void run() throws Exception;
	
	public static final ThrowingRunnable NOP = () -> {};
}
