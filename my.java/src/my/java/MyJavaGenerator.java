package my.java;

import java.util.Arrays;

import org.eclipse.emf.codegen.ecore.genmodel.GenClass;
import org.eclipse.emf.codegen.ecore.genmodel.GenFeature;
import org.eclipse.emf.codegen.ecore.genmodel.GenOperation;

import myjava.AbstractMethod;
import myjava.Binder;
import myjava.Call;
import myjava.Callable;
import myjava.Cast;
import myjava.ClassFormalParameter;
import myjava.ClassParameterType;
import myjava.ClassType;
import myjava.Clazz;
import myjava.ConcreteMethod;
import myjava.Conditional;
import myjava.Different;
import myjava.Equals;
import myjava.Eval;
import myjava.Expression;
import myjava.ExternalClass;
import myjava.FormalParameter;
import myjava.GenClassClazz;
import myjava.GenOperationCallable;
import myjava.GeneratedClass;
import myjava.GetGenFeatureCallable;
import myjava.IfThenElse;
import myjava.InstanceOf;
import myjava.InstantiableClassType;
import myjava.JavaMethodCallable;
import myjava.JavaPrimitiveType;
import myjava.Lambda;
import myjava.LocalVariable;
import myjava.MyjavaFactory;
import myjava.NewObject;
import myjava.Null;
import myjava.PrimitiveType;
import myjava.Reference;
import myjava.ReifiedEClass;
import myjava.ReifiedEStructuralFeature;
import myjava.Return;
import myjava.Statement;
import myjava.StringLiteral;
import myjava.This;
import myjava.Throw;
import myjava.Type;
import myjava.Visibility;

public class MyJavaGenerator {

	public static Clazz createExternalClassOfGenClass(GenClass name) {
		GenClassClazz gcc = MyjavaFactory.eINSTANCE.createGenClassClazz();
		gcc.setGenClass(name);
		return gcc;
	}

	public static Clazz createExternalClassOfString(String name) {
		ExternalClass c = MyjavaFactory.eINSTANCE.createExternalClass();
		c.setName(name);
		return c;
	}

	public static ClassType createClassType(Clazz c, InstantiableClassType... p) {
		ClassType r = MyjavaFactory.eINSTANCE.createClassType();
		r.setClazz(c);
		r.getEffectiveParameters().addAll(Arrays.asList(p));
		return r;
	}

	public static PrimitiveType createPrimitiveType(JavaPrimitiveType t) {
		PrimitiveType r = MyjavaFactory.eINSTANCE.createPrimitiveType();
		r.setType(t);
		return r;
	}

	public static ClassParameterType createClassParameterType(ClassFormalParameter p) {
		ClassParameterType t = MyjavaFactory.eINSTANCE.createClassParameterType();
		t.setParameter(p);
		return t;
	}

	public static GeneratedClass createClass(Visibility v, boolean abs, String name, ClassFormalParameter... p) {
		GeneratedClass c = MyjavaFactory.eINSTANCE.createGeneratedClass();
		c.setVisibility(v);
		c.setAbstract(abs);
		c.setName(name);
		Arrays.stream(p).forEachOrdered(c.getFormalParameters()::add);
		return c;
	}

	public static ClassFormalParameter createClassFormalParam(String name, InstantiableClassType... b) {
		ClassFormalParameter p = MyjavaFactory.eINSTANCE.createClassFormalParameter();
		p.setName(name);
		Arrays.stream(b).forEachOrdered(p.getExtends()::add);
		return p;
	}

	public static AbstractMethod createAbstractMethod(Visibility v, Type r, String name, FormalParameter... p) {
		return createAbstractMethod(v, r, name, p, new InstantiableClassType[0]);
	}

	public static FormalParameter createFormalParameter(Type t, String name) {
		FormalParameter p = MyjavaFactory.eINSTANCE.createFormalParameter();
		p.setType(t);
		p.setName(name);
		return p;
	}

	public static AbstractMethod createAbstractMethod(Visibility v, Type r, String name, FormalParameter[] p,
			InstantiableClassType[] e) {
		AbstractMethod m = MyjavaFactory.eINSTANCE.createAbstractMethod();
		m.setVisibility(v);
		m.setFinal(false);
		m.setReturnType(r);
		m.setName(name);
		Arrays.stream(p).forEachOrdered(m.getFormalParameters()::add);
		Arrays.stream(e).forEachOrdered(m.getExceptions()::add);
		return m;
	}

	public static ConcreteMethod createConcreteMethod(Visibility v, Type r, String name, FormalParameter... p) {
		return createConcreteMethod(v, false, r, name, p);
	}

	public static ConcreteMethod createConcreteMethod(Visibility v, boolean fin, Type r, String name,
			FormalParameter... p) {
		return createConcreteMethod(v, fin, r, name, p, new InstantiableClassType[0]);
	}

	public static ConcreteMethod createConcreteMethod(Visibility v, boolean fin, Type r, String name,
			FormalParameter[] p, InstantiableClassType[] e) {
		ConcreteMethod m = MyjavaFactory.eINSTANCE.createConcreteMethod();
		m.setVisibility(v);
		m.setFinal(fin);
		m.setReturnType(r);
		m.setName(name);
		Arrays.stream(p).forEachOrdered(m.getFormalParameters()::add);
		Arrays.stream(e).forEachOrdered(m.getExceptions()::add);
		return m;
	}

	public static StringLiteral createStringLiteral(String s) {
		StringLiteral r = MyjavaFactory.eINSTANCE.createStringLiteral();
		r.setValue(s);
		return r;
	}

	public static Eval createEval(Expression e) {
		Eval r = MyjavaFactory.eINSTANCE.createEval();
		r.setExpression(e);
		return r;
	}

	public static Throw createThrow(Expression e) {
		Throw t = MyjavaFactory.eINSTANCE.createThrow();
		t.setValue(e);
		return t;
	}

	public static Return createReturn(Expression e) {
		Return t = MyjavaFactory.eINSTANCE.createReturn();
		t.setValue(e);
		return t;
	}

	public static This createThis() {
		return MyjavaFactory.eINSTANCE.createThis();
	}

	public static Call createCall(Expression target, Callable method, Expression... params) {
		Call c = MyjavaFactory.eINSTANCE.createCall();
		c.setTarget(target);
		c.setMethod(method);
		Arrays.stream(params).forEachOrdered(c.getEffectiveParameters()::add);
		return c;
	}

	public static NewObject createNewObject(InstantiableClassType t, Expression... params) {
		NewObject n = MyjavaFactory.eINSTANCE.createNewObject();
		n.setClazz(t);
		Arrays.stream(params).forEachOrdered(n.getEffectiveParameters()::add);
		return n;
	}

	public static IfThenElse createIfThen(Expression c, Statement... t) {
		IfThenElse i = MyjavaFactory.eINSTANCE.createIfThenElse();
		i.setCondition(c);
		Arrays.stream(t).forEachOrdered(i.getThen()::add);
		return i;
	}

	public static IfThenElse createIfThenElse(Expression c, Statement[] t, Statement[] e) {
		IfThenElse i = MyjavaFactory.eINSTANCE.createIfThenElse();
		i.setCondition(c);
		Arrays.stream(t).forEachOrdered(i.getThen()::add);
		Arrays.stream(e).forEachOrdered(i.getElse()::add);
		return i;
	}

	public static InstanceOf createInstanceOf(Expression l, Clazz r) {
		InstanceOf i = MyjavaFactory.eINSTANCE.createInstanceOf();
		i.setLeft(l);
		i.setType(r);
		return i;
	}

	public static Reference createReference(Binder b) {
		Reference r = MyjavaFactory.eINSTANCE.createReference();
		r.setBinder(b);
		return r;
	}

	public static GetGenFeatureCallable createGet(GenFeature f) {
		GetGenFeatureCallable g = MyjavaFactory.eINSTANCE.createGetGenFeatureCallable();
		g.setFeature(f);
		return g;
	}

	public static ReifiedEClass createReifiedEClass(GenClass c) {
		ReifiedEClass r = MyjavaFactory.eINSTANCE.createReifiedEClass();
		r.setGenClass(c);
		return r;
	}

	public static ReifiedEStructuralFeature createReifiedEStructuralFeature(GenFeature c) {
		ReifiedEStructuralFeature r = MyjavaFactory.eINSTANCE.createReifiedEStructuralFeature();
		r.setFeature(c);
		return r;
	}

	public static LocalVariable createLocalVariable(boolean f, Type t, String name, Expression e) {
		LocalVariable r = MyjavaFactory.eINSTANCE.createLocalVariable();
		r.setFinal(f);
		r.setType(t);
		r.setName(name);
		r.setValue(e);
		return r;
	}

	public static Equals createEqual(Expression l, Expression r) {
		Equals e = MyjavaFactory.eINSTANCE.createEquals();
		e.setLeft(l);
		e.setRight(r);
		return e;
	}

	public static Different createDifferent(Expression l, Expression r) {
		Different e = MyjavaFactory.eINSTANCE.createDifferent();
		e.setLeft(l);
		e.setRight(r);
		return e;
	}

	public static Null createNull() {
		return MyjavaFactory.eINSTANCE.createNull();
	}

	public static Cast createCast(Type t, Expression e) {
		Cast r = MyjavaFactory.eINSTANCE.createCast();
		r.setType(t);
		r.setExpression(e);
		return r;
	}

	public static GenOperationCallable createGenOperationCallable(GenOperation o) {
		GenOperationCallable r = MyjavaFactory.eINSTANCE.createGenOperationCallable();
		r.setOperation(o);
		return r;
	}

	public static JavaMethodCallable createJavaMethodCallable(String o) {
		JavaMethodCallable r = MyjavaFactory.eINSTANCE.createJavaMethodCallable();
		r.setSpecifier(o);
		return r;
	}

	public static Conditional createConditional(Expression c, Expression t, Expression e) {
		Conditional r = MyjavaFactory.eINSTANCE.createConditional();
		r.setCondition(c);
		r.setThen(t);
		r.setElse(e);
		return r;
	}

	public static Lambda createLambda(Expression b, FormalParameter... p) {
		Lambda r = MyjavaFactory.eINSTANCE.createLambda();
		r.setBody(b);
		Arrays.stream(p).forEachOrdered(r.getParameters()::add);
		return r;
	}

}
