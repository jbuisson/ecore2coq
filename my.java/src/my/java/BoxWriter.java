package my.java;

import java.io.IOException;
import java.io.Writer;
import java.util.Stack;

public class BoxWriter implements AutoCloseable {
	private static final int WIDTH = 80;
	private static final int INC = 20;

	private Writer out;
	@SuppressWarnings("unused")
	private int row;
	private int col;
	private Stack<Box> boxes;

	public BoxWriter(Writer out) {
		this.out = out;
		this.row = 0;
		this.col = 0;
		this.boxes = new Stack<>();
		this.boxes.push(new HoV(0, WIDTH));
	}

	private int nextWidth(int i) {
		int o = boxes.peek().width();
		if (o - i < INC) {
			int x = i + 2 * INC - 1;
			return x - (x % INC);
		} else {
			return o;
		}
	}

	@Override
	public void close() throws Exception {
		while (!boxes.isEmpty()) {
			e();
		}
		out.close();
	}

	public BoxWriter e() throws Exception {
		boxes.pop().close();
		return this;
	}

	public BoxWriter h() throws Exception {
		boxes.peek().open();
		boxes.push(new H(boxes.peek().width()));
		return this;
	}

	public BoxWriter v() throws Exception {
		return v(0);
	}

	public BoxWriter v(int x) throws Exception {
		boxes.peek().open();
		int i = col + x;
		boxes.push(new V(i, nextWidth(i)));
		return this;
	}

	public BoxWriter hov() throws Exception {
		return hov(0);
	}

	public BoxWriter hov(int x) throws Exception {
		boxes.peek().open();
		int i = col + x;
		boxes.push(new HoV(i, nextWidth(i)));
		return this;
	}

	public BoxWriter sp() throws IOException {
		return write(' ');
	}

	public BoxWriter nl() throws IOException {
		return write('\n');
	}

	public BoxWriter bp() throws Exception {
		boxes.peek().flush();
		return this;
	}

	public BoxWriter write(char c) throws IOException {
		try {
			if (c == '\n') {
				boxes.peek().nl();
			} else if (Character.isWhitespace(c)) {
				boxes.peek().sp();
			} else {
				boxes.peek().write(c);
			}
		} catch (Exception e) {
			throw new IOException(e);
		}
		return this;
	}

	public BoxWriter write(CharSequence s) throws IOException {
		for (int i = 0; i < s.length(); ++i) {
			write(s.charAt(i));
		}
		return this;
	}

	private interface Box extends AutoCloseable {
		void nl() throws IOException;

		void sp() throws IOException;

		void write(char c) throws IOException;

		void flush() throws IOException;

		void open() throws IOException;

		int width();
	}

	private abstract class A implements Box {
		public final int indent;
		public final int width;

		public A(int indent, int width) {
			this.indent = indent;
			this.width = width;
		}

		protected void do_nl() throws IOException {
			out.append('\n');
			row += 1;
			col = 0;
			for (int i = 0; i < indent; ++i) {
				out.append(' ');
				col += 1;
			}
		}

		@Override
		public final int width() {
			return width;
		}
	}

	private final class HoV extends A {
		private StringBuilder s = new StringBuilder();
		private boolean sp = false;
		private int state = 0;

		public HoV(int indent, int width) {
			super(indent, width);
		}

		@Override
		public void close() throws IOException {
			flush();
		}

		@Override
		public void nl() throws IOException {
			flush();
			do_nl();
			sp = false;
			state = 2;
		}

		@Override
		public void sp() throws IOException {
			flush();
			sp = true;
		}

		@Override
		public void write(char c) {
			s.append(c);
			sp = false;
		}

		@Override
		public void flush() throws IOException {
			int l = s.length();
			if (l > 0) {
				if (state == 0) {
					state = 1;
				} else if (state == 1 || col > indent) {
					if (state == 2 && col + l + 1 > width) {
						do_nl();
						state = 2;
					} else {
						out.append(' ');
						col += 1;
					}
				}
				sp = false;
				out.append(s);
				col += l;
				s = new StringBuilder();
			}
		}

		@Override
		public void open() throws IOException {
			if (sp) {
				flush();
				out.append(' ');
				col += 1;
				sp = false;
			} else {
				flush();
			}
			state = 0;
		}
	}

	private final class V extends A {
		public V(int indent, int width) {
			super(indent, width);
		}

		@Override
		public void close() {
		}

		@Override
		public void nl() throws IOException {
			if (col != indent) {
				do_nl();
			}
		}

		@Override
		public void sp() throws IOException {
			nl();
		}

		@Override
		public void write(char c) throws IOException {
			out.append(c);
			col += 1;
		}

		@Override
		public void flush() {
		}

		@Override
		public void open() {
		}
	}

	private final class H implements Box {
		public final int width;
		private boolean s = true;

		public H(int width) {
			this.width = width;
		}

		@Override
		public void close() {
		}

		@Override
		public void nl() throws IOException {
			sp();
		}

		@Override
		public void sp() throws IOException {
			if (!s) {
				out.append(' ');
				col += 1;
				s = true;
			}
		}

		@Override
		public void write(char c) throws IOException {
			out.append(c);
			col += 1;
			s = false;
		}

		@Override
		public void flush() {
		}

		@Override
		public void open() {
		}

		@Override
		public int width() {
			return width;
		}
	}
}
