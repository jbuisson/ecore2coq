theory HandwrittenGenericDataStructure

imports Main

begin

datatype 'a t = T_T

datatype u = U_U "u t"

inductive tt :: "'a \<Rightarrow> bool"

inductive uu :: "bool" where
  UU_U: "tt uu \<Longrightarrow> uu"

end