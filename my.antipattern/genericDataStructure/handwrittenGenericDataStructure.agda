data T : Set → Set where

data U : Set where
  u_U : (x : T U) → U

