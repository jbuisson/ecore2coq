package my.qvtcoverage.ui.actions;

import java.lang.reflect.Field;
import java.util.HashSet;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.m2m.internal.qvt.oml.tools.coverage.common.CoverageData;
import org.eclipse.m2m.internal.qvt.oml.tools.coverage.common.CoverageDataPersistor;
import org.eclipse.m2m.internal.qvt.oml.tools.coverage.common.NodeData;
import org.eclipse.m2m.internal.qvt.oml.tools.coverage.common.TransformationCoverageData;
import org.eclipse.m2m.qvt.oml.tools.coverage.ui.CoveragePlugin;

public class Show extends AbstractHandler {

	private static final String PLATFORM_PLUGIN = "platform:/plugin/";
	private static final String PLATFORM_RESOURCE = "platform:/resource/";
	private static final Field transfUri;
	private static final Field touched;

	static {
		try {
			transfUri = TransformationCoverageData.class.getDeclaredField("transfUri");
			transfUri.setAccessible(true);
			touched = TransformationCoverageData.class.getDeclaredField("touched");
			touched.setAccessible(true);
		} catch (NoSuchFieldException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			CoverageData data = CoverageDataPersistor.load();
			if (data != null) {
				CoverageData patched = new CoverageData();
				data.getData().stream().map(Show::fixURI).forEachOrdered(patched::add);
				CoveragePlugin.getDefault().showCoverageView(patched);
			}
			return null;
		} catch (RuntimeException e) {
			throw new ExecutionException("", e);
		}
	}

	private static TransformationCoverageData fixURI(TransformationCoverageData d) {
		try {
			String stringURI = (String) transfUri.get(d);
			if (stringURI.startsWith(PLATFORM_PLUGIN)) {
				String base = stringURI.substring(PLATFORM_PLUGIN.length());
				URI patchedURI = URI.createURI(PLATFORM_RESOURCE + base);
				TransformationCoverageData patched = new TransformationCoverageData(patchedURI);
				@SuppressWarnings("unchecked")
				HashSet<NodeData> from = (HashSet<NodeData>) touched.get(d);
				@SuppressWarnings("unchecked")
				HashSet<NodeData> to = (HashSet<NodeData>) touched.get(patched);
				to.addAll(from);
				System.err.println("coverage: remapped " + stringURI + " -> " + patchedURI.toString());
				return patched;
			} else {
				return d;
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
}
