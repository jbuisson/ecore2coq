Definition _URI: Type -> Type := fun _ => nat.
Require Import String.

Inductive _Type: Type :=
| Type_Arrow: forall (left: _Type), forall (right: _Type), _Type
| Type_Primitive: _Type.

Inductive _Term: Type -> Type :=
| Term_Abstraction: forall (A: Type), forall (type: option A), forall (boundName: string), forall (boundType: option A), forall (body: _Term A), _Term A
| Term_Application: forall (A: Type), forall (function: _Term A), forall (parameter: _Term A), _Term A
| Term_Variable: forall (A: Type), forall (binder: _URI (_Binder A)), _Term A
with _Binder: Type -> Type :=
| Binder_Definition: forall (D: Type), forall (boundName: string), forall (boundType: option D), forall (term: _Term D), _Binder D
| Binder_Abstraction: forall (A: Type), forall (type: option A), forall (boundName: string), forall (boundType: option A), forall (body: _Term A), _Binder A.

Inductive _Definition: Type -> Type :=
| Definition_Definition: forall (D: Type), forall (boundName: string), forall (boundType: option D), forall (term: _Term D), _Definition D.

Inductive _File: Type :=
| File_File: forall (definitions: list (_Definition _Type)), _File.