package my.utils.activator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;

public class UtilsActivator extends Plugin {
	private static UtilsActivator instance = null;

	public static UtilsActivator getInstance() {
		return instance;
	}

	public UtilsActivator() {
		instance = this;
	}

	public static void error(String s) {
		String pluginID = getInstance().getBundle().getSymbolicName();
		getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, s));
	}

	public static void info(String s) {
		String pluginID = getInstance().getBundle().getSymbolicName();
		getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, s));
	}
}
