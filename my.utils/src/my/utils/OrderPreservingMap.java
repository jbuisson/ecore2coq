package my.utils;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OrderPreservingMap<K, V> implements Map<K, V> {
	private final Map<K, Integer> index;
	private final List<Entry<K, V>> store;

	public OrderPreservingMap() {
		this(HashMap<K, Integer>::new, ArrayList<Entry<K, V>>::new);
	}

	public OrderPreservingMap(Supplier<Map<K, Integer>> indexSupplier) {
		this(indexSupplier, ArrayList<Entry<K, V>>::new);
	}

	public OrderPreservingMap(Supplier<Map<K, Integer>> indexSupplier, Supplier<List<Entry<K, V>>> storeSupplier) {
		this.index = indexSupplier.get();
		this.store = storeSupplier.get();
	}

	@Override
	public int size() {
		return index.size();
	}

	@Override
	public boolean isEmpty() {
		return index.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return index.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return streamValues().anyMatch((x) -> doCompare(value, x));
	}

	private Stream<V> streamValues() {
		return streamEntries().map(Entry<K, V>::getValue);
	}

	public Stream<Entry<K, V>> streamEntries() {
		return store.stream().filter((x) -> x != null);
	}

	private boolean doCompare(Object left, V right) {
		if (left == null) {
			return right == null;
		} else {
			return left.equals(right);
		}
	}

	@Override
	public V get(Object key) {
		Integer i = index.get(key);
		if (i == null) {
			return null;
		} else {
			Entry<K, V> e = store.get(i);
			if (e == null) {
				return null;
			} else {
				return e.getValue();
			}
		}
	}

	@Override
	public V put(K key, V value) {
		Integer i = index.computeIfAbsent(key, this::createLocus);
		Entry<K, V> e = new AbstractMap.SimpleEntry<>(key, value);
		Entry<K, V> prev = store.set(i, e);
		if (prev == null) {
			return null;
		} else {
			return prev.getValue();
		}
	}

	private Integer createLocus(K a) {
		int p = store.size();
		store.add(null);
		return p;
	}

	@Override
	public V remove(Object key) {
		Integer i = index.remove(key);
		if (i == null) {
			return null;
		} else {
			Entry<K, V> e = store.set(i, null);
			if (e == null) {
				return null;
			} else {
				return e.getValue();
			}
		}
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		m.entrySet().stream().forEachOrdered(this::put);
	}

	private void put(Entry<? extends K, ? extends V> e) {
		put(e.getKey(), e.getValue());
	}

	@Override
	public void clear() {
		index.clear();
		store.clear();
	}

	@Override
	public Set<K> keySet() {
		return new UnmodifiableArraySet<>(streamEntries().map(Entry::getKey).distinct());
	}

	@Override
	public Collection<V> values() {
		return streamValues().collect(Collectors.toList());
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		return new UnmodifiableArraySet<>(streamEntries().map(this::cloneEntry).distinct());
	}

	private Entry<K, V> cloneEntry(Entry<K, V> e) {
		return new AbstractMap.SimpleEntry<>(e.getKey(), e.getValue());
	}

}
