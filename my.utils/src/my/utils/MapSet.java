package my.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class MapSet<K> implements Set<K> {
	private final Map<K, Object> store;

	public MapSet(Supplier<Map<K, Object>> supplier) {
		store = supplier.get();
	}

	@Override
	public int size() {
		return store.size();
	}

	@Override
	public boolean isEmpty() {
		return store.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return store.containsKey(o);
	}

	@Override
	public Iterator<K> iterator() {
		return store.keySet().iterator();
	}

	@Override
	public Object[] toArray() {
		return store.keySet().toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return store.keySet().toArray(a);
	}

	@Override
	public boolean add(K e) {
		return store.put(e, this) == null;
	}

	@Override
	public boolean remove(Object o) {
		return store.remove(o) != null;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return c.stream().map(store::get).allMatch((x) -> x != null);
	}

	@Override
	public boolean addAll(Collection<? extends K> c) {
		return c.stream().map(this::add).collect(Collectors.toSet()).stream().anyMatch((x) -> x);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		Set<K> s = new HashSet<>(store.keySet());
		s.removeAll(c);
		return s.stream().map(this::remove).collect(Collectors.toSet()).stream().anyMatch((x) -> x);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return c.stream().map(this::remove).collect(Collectors.toSet()).stream().anyMatch((x) -> x);
	}

	@Override
	public void clear() {
		store.clear();
	}

}
