package my.utils;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.AbstractTreeIterator;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;

import my.transform.utils.MyResourceSetImpl;
import my.utils.activator.UtilsActivator;

public class WorkspaceHelper {

	public static void ensureExists(IFolder f) {
		if (!f.exists()) {
			IContainer p = f.getParent();
			if (p != null && p instanceof IFolder) {
				ensureExists((IFolder) p);
			}
			try {
				f.create(false, false, null);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
	}

	public static Predicate<IResource> fileFilter(String extension) {
		return (x) -> x instanceof IFile && x.getName().endsWith(extension);
	}

	public static boolean validate(IResource r) {
		if (r instanceof IFile) {
			ResourceSet resourceSet = new MyResourceSetImpl();
			URI uri = URI.createURI(r.getLocationURI().toString());
			Resource ecoreResource = resourceSet.getResource(uri, true);
			EcoreUtil.resolveAll(resourceSet);
			if (ecoreResource.getContents().size() < 1) {
				return false;
			}
			EObject root = ecoreResource.getContents().get(0);
			Diagnostic diagnostic = Diagnostician.INSTANCE.validate(root);
			if (diagnostic.getSeverity() != Diagnostic.OK) {
				dumpDiagnostic(diagnostic);
			}
			return diagnostic.getSeverity() == Diagnostic.OK;
		} else {
			return false;
		}
	}

	private static void dumpDiagnostic(Diagnostic diagnostic) {
		TreeIterator<Diagnostic> i = new AbstractTreeIterator<Diagnostic>(diagnostic) {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<? extends Diagnostic> getChildren(Object object) {
				return ((Diagnostic) object).getChildren().iterator();
			}
		};
		while (i.hasNext()) {
			Diagnostic d = i.next();
			UtilsActivator.info("Severity: " + d.getSeverity() + " code: " + d.getCode() + " source: " + d.getSource()
					+ " " + d.getMessage());
			// System.out.println("Severity: " + d.getSeverity() + " code: " + d.getCode() +
			// " source: " + d.getSource()
			// + " " + d.getMessage());
		}
	}

	public static List<IResource> listFiles(Predicate<IResource> filter, IProgressMonitor monitor)
			throws CoreException {
		SubMonitor sub = SubMonitor.convert(monitor, 1);
		final List<IResource> selected = new LinkedList<>();
		populate(selected, filter, ResourcesPlugin.getWorkspace().getRoot(), sub);
		sub.done();
		return selected;
	}

	private static void populate(List<? super IResource> selected, Predicate<IResource> filter, IResource root,
			SubMonitor sub) throws CoreException {
		sub.setTaskName(root.getName());
		if (filter.test(root)) {
			selected.add(root);
		}
		SubMonitor s = sub.split(1);
		if (root instanceof IContainer) {
			IResource[] members = ((IContainer) root).members();
			s.setWorkRemaining(members.length);
			for (IResource i : members) {
				populate(selected, filter, i, s);
			}
		}
		s.done();
	}

}
