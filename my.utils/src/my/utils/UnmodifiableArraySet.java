package my.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;

public class UnmodifiableArraySet<E> implements Set<E> {
	private final Object[] store;

	public UnmodifiableArraySet(Object... store) {
		this.store = Arrays.stream(store).distinct().toArray();
	}

	public UnmodifiableArraySet(Stream<E> source) {
		this.store = source.distinct().toArray();
	}

	@Override
	public final int size() {
		return store.length;
	}

	@Override
	public final boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public final boolean contains(Object o) {
		return Arrays.stream(store).parallel().anyMatch((x) -> doEquals(o, x));
	}

	private static boolean doEquals(Object o, Object x) {
		if (o == null) {
			return x == null;
		} else {
			return o.equals(x);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterator<E> iterator() {
		return Arrays.stream(store).map((x) -> (E) x).iterator();
	}

	@Override
	public Object[] toArray() {
		return Arrays.copyOf(store, store.length);
	}

	@Override
	public <T> T[] toArray(T[] a) {
		@SuppressWarnings("unchecked")
		Class<T[]> c = (Class<T[]>) a.getClass();
		return Arrays.copyOf(store, store.length, c);
	}

	@Override
	public boolean add(E e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return c.parallelStream().allMatch(this::contains);
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}

}
