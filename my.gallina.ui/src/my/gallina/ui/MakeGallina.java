package my.gallina.ui;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import my.gallina.ui.activator.GallinaActivator;
import my.transform.utils.MyResourceSetImpl;

public class MakeGallina extends AbstractHandler {
	private static final String GALLINA = "gallina";

	private static Map<?, ?> saveOptions() {
		Map<String, Object> r = new HashMap<>();
		r.put(XMLResource.OPTION_URI_HANDLER, new URIHandlerImpl.PlatformSchemeAware());
		return r;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell shell = HandlerUtil.getActiveShellChecked(event);
		FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		dialog.setFilterNames(new String[] { "Gallina models" });
		dialog.setFilterExtensions(new String[] { "*." + GALLINA });
		dialog.setOverwrite(true);
		String fileName = dialog.open();
		if (fileName != null) {
			Job job = Job.create(this.getClass().getSimpleName(), (ICoreRunnable)
					(progressMonitor) -> doExecute(fileName, progressMonitor));
			job.schedule();
		}
		return null;
	}

	private void doExecute(String target, IProgressMonitor progressMonitor) {
		Map<String, Object> extensionToFactoryMap = Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap();
		extensionToFactoryMap.put(GALLINA, new XMIResourceFactoryImpl());
		try {
			ResourceSet resourceSet = new MyResourceSetImpl();
			URI source = URI.createPlatformPluginURI("my.gallina/model/CoqLib.xmi", true);
			Resource r = resourceSet.getResource(source, true);
			URI targetURI = URI.createFileURI(target);
			Resource t = resourceSet.createResource(targetURI);
			t.getContents().add(EcoreUtil.copy(r.getContents().get(0)));
			t.save(saveOptions());
		} catch (IOException e) {
			GallinaActivator.error(e);
		} finally {
			progressMonitor.done();
		}
	}
}
