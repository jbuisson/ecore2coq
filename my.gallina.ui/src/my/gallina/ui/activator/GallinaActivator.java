package my.gallina.ui.activator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class GallinaActivator extends AbstractUIPlugin {
	private static GallinaActivator instance = null;

	public static GallinaActivator getInstance() {
		return instance;
	}

	public GallinaActivator() {
		instance = this;
	}

	public static void error(Throwable t) {
		String pluginID = getInstance().getBundle().getSymbolicName();
		getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, t.getMessage(), t));
	}
}
