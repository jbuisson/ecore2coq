package my.ecore2coq.utils;

import java.util.function.Supplier;

public class Singleton<T> {
	private T instance;

	public Singleton() {
		this.instance = null;
	}

	public synchronized T get(Supplier<T> provider) {
		if (instance == null) {
			instance = provider.get();
			if (instance == null) {
				throw new NullPointerException();
			}
		}
		return instance;
	}
}
