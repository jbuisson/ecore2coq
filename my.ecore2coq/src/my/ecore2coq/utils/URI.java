package my.ecore2coq.utils;

import java.io.Serializable;
import java.math.BigInteger;

import org.eclipse.emf.ecore.EObject;

public class URI<T extends EObject> implements Serializable {
	private static final long serialVersionUID = 8174211602878043975L;

	private final BigInteger key;

	public URI(BigInteger key) {
		this.key = key;
	}

	public BigInteger getKey() {
		return this.key;
	}
}
