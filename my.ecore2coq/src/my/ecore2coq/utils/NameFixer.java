package my.ecore2coq.utils;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import scheduledinductive.SIPackage;

public class NameFixer {
	private static String getName(String s) {
		String p = "_2f_";
		int from = s.lastIndexOf(p);
		if (from >= 0) {
			from = from + p.length();
		} else {
			from = 0;
		}
		String c = "_2e_ecore";
		if (s.endsWith(c)) {
			return s.substring(from, s.length() - c.length());
		} else {
			return s.substring(from);
		}
	}

	public static String getName(List<EObject> in) {
		return in.stream().filter((i) -> i instanceof SIPackage).map((i) -> (SIPackage) i).map(SIPackage::getName)
				.map(NameFixer::getName).findAny().orElse("unnamed");
	}

}
