package my.ecore2coq.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.function.Function;
import java.util.stream.Stream;

import my.utils.MapSet;
import my.utils.OrderPreservingMap;

/**
 * According to <a href=
 * "https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm">Wikipedia's
 * presentation of Tarjan's algorithm</a>.
 * 
 * @author Jérémy Buisson
 *
 * @param <V>
 *            type of the graph's nodes
 */
public class Tarjan<V> {
	private final Function<V, ? extends Stream<? extends V>> edges;
	private final Stack<V> s = new Stack<>();
	private final Map<V, Integer> index = new HashMap<>();
	private final Map<V, Integer> lowlink = new HashMap<>();
	private final Collection<Set<V>> result = new LinkedList<>();
	private int counter = 0;

	private Tarjan(Function<V, ? extends Stream<? extends V>> edges) {
		this.edges = edges;
	}

	public static <V> Collection<Set<V>> run(Stream<V> vertices, Function<V, ? extends Stream<? extends V>> edges) {
		Tarjan<V> t = new Tarjan<>(edges);
		t.doit(vertices);
		return Collections.unmodifiableCollection(t.result);
	}

	private void doit(Stream<V> vertices) {
		vertices.sequential().forEach((i) -> {
			if (!index.containsKey(i)) {
				strongConnect(i);
			}
		});
	}

	private void strongConnect(V v) {
		int vindex = counter;
		index.put(v, vindex);
		lowlink.put(v, vindex);
		counter += 1;
		s.push(v);
		edges.apply(v).sequential().forEach((w) -> {
			if (index.containsKey(w)) {
				if (s.contains(w)) {
					lowlink.computeIfPresent(v, (a, p) -> Math.min(p, index.get(w)));
				}
			} else {
				strongConnect(w);
				lowlink.computeIfPresent(v, (a, p) -> Math.min(p, lowlink.get(w)));
			}
		});
		if (vindex == lowlink.get(v).intValue()) {
			Set<V> component = new MapSet<>(OrderPreservingMap::new);
			for (V w = s.pop(); w != v; w = s.pop()) {
				component.add(w);
			}
			component.add(v);
			result.add(Collections.unmodifiableSet(component));
		}
	}
}
