package my.ecore2coq.utils;

import java.util.Collections;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import my.utils.MapSet;
import my.utils.OrderPreservingMap;

public class NaiveClosure<V> {
	private final Function<V, ? extends Stream<? extends V>> edges;
	private final Set<V> result;

	private NaiveClosure(Function<V, ? extends Stream<? extends V>> edges) {
		this.edges = edges;
		this.result = new MapSet<>(OrderPreservingMap::new);
	}

	private NaiveClosure(Function<V, ? extends Stream<? extends V>> edges, Supplier<Set<V>> sup) {
		this.edges = edges;
		this.result = sup.get();
	}

	public static <V> Set<V> run(Stream<V> vertices, Function<V, ? extends Stream<? extends V>> edges) {
		NaiveClosure<V> c = new NaiveClosure<>(edges);
		vertices.forEach(c::addRoot);
		return Collections.unmodifiableSet(c.result);
	}

	public static <V> Set<V> run(Stream<V> vertices, Function<V, ? extends Stream<? extends V>> edges, Supplier<Set<V>> sup) {
		NaiveClosure<V> c = new NaiveClosure<>(edges, sup);
		vertices.forEach(c::addRoot);
		return Collections.unmodifiableSet(c.result);
	}

	public void addRoot(V v) {
		if (result.add(v)) {
			edges.apply(v).forEach(this::addRoot);
		}
	}
}
