package my.ecore2coq.utils;

import java.util.Collection;

public interface Bag<E> extends Collection<E> {
}
