package my.ecore2coq.utils;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * According to
 * <a href="https://en.wikipedia.org/wiki/Topological_sorting">Wikipedia's
 * presentation of depth-first search</a>.
 * 
 * @author Jérémy Buisson
 *
 * @param <V>
 *            type of the graph's nodes
 */
public class DFSSort<V> {
	private final Set<V> mark = new HashSet<>();
	private final Set<V> tempMark = new HashSet<>();
	private final List<V> ordered = new LinkedList<>();
	private final Function<V, ? extends Stream<? extends V>> edges;

	private DFSSort(Function<V, ? extends Stream<? extends V>> edges) {
		this.edges = edges;
	}

	public static <V> List<V> run(Stream<V> vertices, Function<V, ? extends Stream<? extends V>> edges) {
		DFSSort<V> sort = new DFSSort<>(edges);
		sort.doit(vertices);
		return Collections.unmodifiableList(sort.ordered);
	}

	private void doit(Stream<V> vertices) {
		vertices.sequential().forEach(this::visit);
	}

	private void visit(V n) {
		if (tempMark.contains(n)) {
			throw new AssertionError();
		} else if (!mark.contains(n)) {
			tempMark.add(n);
			edges.apply(n).sequential().forEach(this::visit);
			mark.add(n);
			tempMark.remove(n);
			ordered.add(0, n);
		}
	}
}
