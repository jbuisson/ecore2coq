package my.ecore2coq.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.EList;

import cleanimplecore.CIEClass;
import cleanimplecore.CIEClassifier;
import cleanimplecore.CIEClassifierType;
import implecore.IEClass;
import implecore.IEClassifier;
import implecore.IEClassifierType;

public class Utils {
	public static <A, B> Function<A, B> cast(Class<B> target) {
		return target::cast;
	}

	public static <A, B> Function<A, Stream<B>> filter(Class<B> target) {
		return (a) -> Stream.of(a).filter(target::isInstance).map(cast(target));
	}

	public static Set<IEClass> selfAndSuperTypesOf(IEClass targetClass) {
		Function<IEClass, EList<? super IEClassifierType>> sup = IEClass::getESuperTypes;
		Function<IEClassifierType, IEClassifier> cl = IEClassifierType::getEClassifier;
		return genericSelfAndSuperTypesOf(targetClass, IEClass.class, sup, IEClassifierType.class, cl);
	}

	private static <C, CT> Set<C> genericSelfAndSuperTypesOf(C targetClass, Class<C> c,
			Function<C, EList<? super CT>> getSupers, Class<CT> ct, Function<CT, ? super C> getC) {
		Set<C> result = new HashSet<>();
		genericSelfAndSuperTypesOf(result, targetClass, c, getSupers, ct, getC);
		return result;
	}

	private static <C, CT> void genericSelfAndSuperTypesOf(Set<C> result, C targetClass, Class<C> c,
			Function<C, EList<? super CT>> getSupers, Class<CT> ct, Function<CT, ? super C> getC) {
		if (result.add(targetClass)) {
			getSupers.apply(targetClass).stream().flatMap(filter(ct)).map(getC).flatMap(filter(c))
					.forEach((i) -> genericSelfAndSuperTypesOf(result, i, c, getSupers, ct, getC));
		}
	}

	public static Set<CIEClass> selfAndSuperTypesOf(CIEClass targetClass) {
		Function<CIEClass, EList<? super CIEClassifierType>> sup = CIEClass::getESuperTypes;
		Function<CIEClassifierType, CIEClassifier> cl = CIEClassifierType::getEClassifier;
		return genericSelfAndSuperTypesOf(targetClass, CIEClass.class, sup, CIEClassifierType.class, cl);
	}

	public static <V> Collection<Set<V>> stronglyConnectedComponents(Stream<V> vertices,
			Function<V, ? extends Stream<? extends V>> edges) {
		return Tarjan.run(vertices, edges);
	}

	public static <V> List<V> topologicalSort(Stream<V> vertices, Function<V, ? extends Stream<? extends V>> edges) {
		return DFSSort.run(vertices, edges);
	}
	
	public static <V> Set<V> transitiveClosure(Stream<V> vertices, Function<V, ? extends Stream<? extends V>> edges) {
		return NaiveClosure.run(vertices, edges);
	}
}
