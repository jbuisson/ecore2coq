package my.ecore2coq.utils;

import java.util.Collection;

public interface NoDupList<E> extends Collection<E> {
}
