package my.ecore2coq;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;

public class Cloner extends TransformerWithMapping {
	private Cloner(MappingModel mapping) {
		super(mapping);
	}

	@Override
	protected EObject hookFindTransformedOf(EObject o) {
		return o;
	}

	public static <T extends EObject> T copy(MappingModel mapping, T source) {
		Cloner c = new Cloner(mapping);
		EObject target = c.transform(source);
		mapping.getTemporaryObjects().add(target);
		@SuppressWarnings("unchecked")
		T result = (T) target;
		return result;
	}
}
