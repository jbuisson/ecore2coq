package my.ecore2coq;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.codegen.ecore.genmodel.GenClass;
import org.eclipse.emf.codegen.ecore.genmodel.GenFeature;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenOperation;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import mapping.MappingModel;
import my.ecore2coq.utils.NameFixer;
import my.utils.Pair;
import myjava.AbstractMethod;
import myjava.Binder;
import myjava.Call;
import myjava.ClassFormalParameter;
import myjava.Clazz;
import myjava.ConcreteMethod;
import myjava.Expression;
import myjava.FormalParameter;
import myjava.GenOperationCallable;
import myjava.GeneratedClass;
import myjava.GetGenFeatureCallable;
import myjava.IfThenElse;
import myjava.InstantiableClassType;
import myjava.JavaMethodCallable;
import myjava.JavaPrimitiveType;
import myjava.LocalVariable;
import myjava.MyjavaFactory;
import myjava.Statement;
import myjava.Throw;
import myjava.Type;
import myjava.Unit;
import myjava.Visibility;
import norefecore.NREClass;
import norefecore.NREClassifier;
import norefecore.NREClassifierType;
import norefecore.NREDataType;
import norefecore.NREEnum;
import norefecore.NREStructuralFeature;
import norefecore.NREType;
import norefecore.NRETypeAlias;
import scheduledinductive.SIConstructor;
import scheduledinductive.SIDefinition;
import scheduledinductive.SIInductive;
import scheduledinductive.SIInductiveGroupDefinition;
import scheduledinductive.SIPackage;
import scheduledinductive.SIParameter;
import scheduledinductive.SISimpleDefinition;
import scheduledinductive.SIType;

public class SI2MyJava extends MyJavaGeneratorWithMapping {
	private final GenModel ecoreGenModel;
	private final GenOperation eObject_eClass;
	private final GenOperation eObject_eIsSet;
	private final Method object_equals;
	private final Method collection_stream;
	private final Method stream_map;
	private final Map<EObject, Set<EObject>> map;
	private final Map<EClass, GenClass> eClassToGenClass;
	private final Map<EStructuralFeature, GenFeature> eStructuralFeatureToGenFeature;
	private final Map<SIInductive, EClass> siInductiveToEClass;
	private final Map<SIConstructor, EClass> siConstructorToEClass;
	private final Map<SIParameter, EStructuralFeature> siParameterToEStructuralFeature;
	private final Map<SIParameter, NREStructuralFeature> siParameterToNREStructuralFeature;
	private final Map<NREClass, EClass> nreClassToEClass;
	private final Map<EPackage, GenPackage> ePackageToGenPackage;
	private final Map<Object, EObject> externals;
	private ClassFormalParameter term;
	private ClassFormalParameter def;
	private AbstractMethod getConstructor;
	private AbstractMethod apply;
	private AbstractMethod reference;
	private AbstractMethod createFresh;
	private AbstractMethod createUri;
	private AbstractMethod get;
	private AbstractMethod wrapBoolean;
	private AbstractMethod wrapByte;
	private AbstractMethod wrapShort;
	private AbstractMethod wrapInt;
	private AbstractMethod wrapLong;
	private AbstractMethod wrapBigInteger;
	private AbstractMethod wrapChar;
	private AbstractMethod wrapString;
	private AbstractMethod wrapList;
	private AbstractMethod wrapNoDupList;
	private AbstractMethod wrapSet;
	private AbstractMethod wrapBag;
	private AbstractMethod wrapMapEntry;
	private AbstractMethod none;
	private AbstractMethod some;

	public SI2MyJava(GenModel ecoreGenModel, MappingModel mapping) {
		super(mapping);
		this.ecoreGenModel = ecoreGenModel;
		eObject_eClass = getEObject_eClass(ecoreGenModel);
		eObject_eIsSet = getEObject_eIsSet(ecoreGenModel);
		object_equals = getMethod(Object.class, "equals", Object.class);
		collection_stream = getMethod(Collection.class, "stream");
		stream_map = getMethod(Stream.class, "map", Function.class);
		System.out.println("indexing");
		map = toHashMap(mapping);
		System.out.println("collecting");
		externals = new ConcurrentHashMap<>();
		eClassToGenClass = collectEClassToGenClass();
		eStructuralFeatureToGenFeature = collectEStructuralFeatureToGenFeature();
		siInductiveToEClass = collectSIInductiveToEClass();
		siConstructorToEClass = collectSIConstructorToEClass();
		siParameterToEStructuralFeature = collectSIParameterToEStructuralFeature();
		siParameterToNREStructuralFeature = collectSIParameterToNREStructuralFeature();
		nreClassToEClass = collectNREClassToEClass();
		ePackageToGenPackage = collectEPackageToGenPackage();
	}

	private static GenOperation getOperation(GenModel gm, String clazz, String op) {
		Set<GenOperation> ops = gm.getGenPackages().stream().filter(p -> p.getNSURI().equals(EcorePackage.eNS_URI))
				.map(GenPackage::getGenClasses).flatMap(List::stream).filter(x -> x.getName().equals(clazz))
				.flatMap(x -> x.getGenOperations().stream().filter(o -> o.getName().equals(op)))
				.collect(Collectors.toUnmodifiableSet());
		if (ops.size() == 1) {
			return ops.iterator().next();
		} else {
			throw new AssertionError("" + ops.size());
		}
	}

	private static GenOperation getEObject_eClass(GenModel ecoreGenModel) {
		return getOperation(ecoreGenModel, "EObject", "eClass");
	}

	private static GenOperation getEObject_eIsSet(GenModel ecoreGenModel) {
		return getOperation(ecoreGenModel, "EObject", "eIsSet");
	}

	private static Method getMethod(Class<?> cl, String name, Class<?>... p) {
		try {
			return cl.getDeclaredMethod(name, p);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new AssertionError(e);
		}
	}

	private static Map<EObject, Set<EObject>> toHashMap(MappingModel mapping) {
		Map<EObject, Set<EObject>> roots = mapping.getObjectMappings().stream()
				.flatMap(x -> x.getTargets().stream()
						.flatMap(y -> Stream.of(new Pair<>(x.getSource(), y), new Pair<>(y, x.getSource()))))
				.collect(Collectors.groupingByConcurrent(p -> p.first, ConcurrentHashMap::new,
						Collectors.mapping(x -> x.second, Collectors.toCollection(HashSet::new))));
		warshallClosure(roots);
		return roots;
	}

	private static <X> void warshallClosure(Map<X, Set<X>> m) {
		Set<X> e = Set.of();
		Object[] t = m.keySet().toArray();
		for (int k = 0; k < t.length; ++k) {
			for (int i = 0; i < t.length; ++i) {
				if (m.get(t[i]).contains(t[k])) {
					m.get(t[i]).addAll(m.getOrDefault(t[k], e));
				}
			}
		}
	}

	private static <R> Set<R> subset(Set<?> s, Class<R> c) {
		return s.parallelStream().filter(c::isInstance).map(c::cast).collect(Collectors.toSet());
	}

	private static Set<EClass> subsetEClass(Set<?> s) {
		return subset(s, EClass.class);
	}

	private static Set<EStructuralFeature> subsetEStructuralFeature(Set<?> s) {
		return subset(s, EStructuralFeature.class);
	}

	private static Set<NREStructuralFeature> subsetNREStructuralFeature(Set<?> s) {
		return subset(s, NREStructuralFeature.class);
	}

	private static <T> T getTheOne(Set<T> s) {
		if (s.size() == 1) {
			return s.iterator().next();
		} else {
			throw new IllegalArgumentException();
		}
	}

	private Map<SIInductive, EClass> collectSIInductiveToEClass() {
		return collectTToEClass(SIInductive.class);
	}

	private Map<NREClass, EClass> collectNREClassToEClass() {
		return collectTToEClass(NREClass.class);
	}

	private Map<SIConstructor, EClass> collectSIConstructorToEClass() {
		return collectTToEClass(SIConstructor.class);
	}

	private Map<SIParameter, EStructuralFeature> collectSIParameterToEStructuralFeature() {
		return map.entrySet().stream().filter(e -> e.getKey() instanceof SIParameter)
				.collect(Collectors.toConcurrentMap(e -> (SIParameter) e.getKey(),
						e -> getTheOne(subsetEStructuralFeature(e.getValue()))));
	}

	private Map<SIParameter, NREStructuralFeature> collectSIParameterToNREStructuralFeature() {
		return map.entrySet().stream().filter(e -> e.getKey() instanceof SIParameter)
				.collect(Collectors.toConcurrentMap(e -> (SIParameter) e.getKey(),
						e -> getTheOne(subsetNREStructuralFeature(e.getValue()))));
	}

	private <T> Map<T, EClass> collectTToEClass(Class<T> clazz) {
		Map<T, EClass> r = map.entrySet().stream().filter(e -> clazz.isInstance(e.getKey())).collect(
				Collectors.toConcurrentMap(e -> clazz.cast(e.getKey()), e -> getTheOne(subsetEClass(e.getValue()))));
		return r;
	}

	private Map<EClass, GenClass> collectEClassToGenClass() {
		Map<EClass, Set<GenClass>> r = Stream
				.concat(map.keySet().stream().filter(x -> x instanceof GenModel).map(x -> (GenModel) x),
						Stream.of(ecoreGenModel))
				.map(GenModel::getAllGenAndUsedGenPackagesWithClassifiers).flatMap(List::stream)
				.map(GenPackage::getGenClasses).flatMap(List::stream)
				.collect(Collectors.groupingByConcurrent(GenClass::getEcoreClass, Collectors.toSet()));
		return r.entrySet().parallelStream()
				.collect(Collectors.toConcurrentMap(x -> x.getKey(), x -> getTheOne(x.getValue())));
	}

	private Map<EStructuralFeature, GenFeature> collectEStructuralFeatureToGenFeature() {
		Map<EStructuralFeature, Set<GenFeature>> r = Stream
				.concat(map.keySet().stream().filter(x -> x instanceof GenModel).map(x -> (GenModel) x),
						Stream.of(ecoreGenModel))
				.map(GenModel::getAllGenAndUsedGenPackagesWithClassifiers).flatMap(List::stream)
				.map(GenPackage::getGenClasses).flatMap(List::stream).map(GenClass::getGenFeatures)
				.flatMap(List::stream)
				.collect(Collectors.groupingByConcurrent(GenFeature::getEcoreFeature, Collectors.toSet()));
		return r.entrySet().parallelStream()
				.collect(Collectors.toConcurrentMap(x -> x.getKey(), x -> getTheOne(x.getValue())));
	}

	private Map<EPackage, GenPackage> collectEPackageToGenPackage() {
		Map<EPackage, Set<GenPackage>> r = Stream
				.concat(map.keySet().stream().filter(x -> x instanceof GenModel).map(x -> (GenModel) x),
						Stream.of(ecoreGenModel))
				.map(GenModel::getAllGenAndUsedGenPackagesWithClassifiers).flatMap(List::stream)
				.collect(Collectors.groupingByConcurrent(GenPackage::getEcorePackage, Collectors.toSet()));
		return r.entrySet().parallelStream()
				.collect(Collectors.toConcurrentMap(x -> x.getKey(), x -> getTheOne(x.getValue())));
	}

	public List<? extends EObject> transform(List<EObject> source) {
		System.out.println("create");
		String name = NameFixer.getName(source);
		term = createClassFormalParam("T");
		def = createClassFormalParam("D");
		GeneratedClass c = createClass(Visibility.PUBLIC, true, getQualified(source, name), term, def);
		source.forEach(i -> hookMapping(i, c));

		AbstractMethod require = createAbstractMethod(Visibility.PROTECTED, createPrimitiveType(JavaPrimitiveType.VOID),
				"require", createFormalParameter(createClassType(getExternalClass(String.class)), "lib"));
		getConstructor = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "constructor",
				createFormalParameter(createClassType(getExternalClass(String.class)), "name"));
		apply = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "apply",
				createFormalParameter(createClassParameterType(term), "l"),
				createFormalParameter(createClassParameterType(term), "r"));
		reference = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "reference",
				createFormalParameter(createClassParameterType(def), "def"));
		createFresh = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(def), "fresh",
				createFormalParameter(createClassType(getExternalClass(EObject.class)), "obj"),
				createFormalParameter(createClassType(getExternalClass(EClass.class)), "stype"),
				createFormalParameter(createClassParameterType(term), "val"));
		createUri = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "uri",
				createFormalParameter(createClassParameterType(def), "def"));
		get = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(def), "get",
				createFormalParameter(createClassType(getExternalClass(EObject.class)), "obj"),
				createFormalParameter(createClassType(getExternalClass(EClass.class)), "staticClass"));
		wrapBoolean = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrap",
				createFormalParameter(createPrimitiveType(JavaPrimitiveType.BOOLEAN), "value"));
		wrapByte = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrap",
				createFormalParameter(createPrimitiveType(JavaPrimitiveType.BYTE), "value"));
		wrapShort = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrap",
				createFormalParameter(createPrimitiveType(JavaPrimitiveType.SHORT), "value"));
		wrapInt = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrap",
				createFormalParameter(createPrimitiveType(JavaPrimitiveType.INT), "value"));
		wrapLong = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrap",
				createFormalParameter(createPrimitiveType(JavaPrimitiveType.LONG), "value"));
		wrapBigInteger = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrap",
				createFormalParameter(createClassType(getExternalClass(BigInteger.class)), "value"));
		wrapChar = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrap",
				createFormalParameter(createPrimitiveType(JavaPrimitiveType.CHAR), "value"));
		wrapString = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrap",
				createFormalParameter(createClassType(getExternalClass(String.class)), "value"));
		wrapList = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrapList",
				createFormalParameter(createClassType(getExternalClass(Stream.class), createClassParameterType(term)),
						"value"));
		wrapNoDupList = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrapNoDupList",
				createFormalParameter(createClassType(getExternalClass(Stream.class), createClassParameterType(term)),
						"value"));
		wrapSet = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrapSet",
				createFormalParameter(createClassType(getExternalClass(Stream.class), createClassParameterType(term)),
						"value"));
		wrapBag = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrapBag",
				createFormalParameter(createClassType(getExternalClass(Stream.class), createClassParameterType(term)),
						"value"));
		wrapMapEntry = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "wrapMapEntry",
				createFormalParameter(createClassType(getExternalClass(Map.Entry.class)), "value"));
		none = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "none");
		some = createAbstractMethod(Visibility.PROTECTED, createClassParameterType(term), "some",
				createFormalParameter(createClassParameterType(term), "value"));
		ConcreteMethod init = createConcreteMethod(Visibility.PUBLIC, true, createPrimitiveType(JavaPrimitiveType.VOID),
				"prologue");
		Stream.of(require, getConstructor, apply, reference, createFresh, createUri, get, wrapBoolean, wrapByte,
				wrapShort, wrapInt, wrapLong, wrapBigInteger, wrapChar, wrapString, wrapList, wrapNoDupList, wrapSet,
				wrapBag, wrapMapEntry, none, some, init).forEach(c.getMembers()::add);

		init.getBody().add(createEval(createCall(createThis(), require, createStringLiteral(name))));

		Map<SIInductive, ConcreteMethod> indToM = source.stream().map(x -> (SIPackage) x).map(SIPackage::getDefinitions)
				.flatMap(List::stream).flatMap(SI2MyJava::streamInductiveTypes)
				.collect(Collectors.toConcurrentMap(Function.identity(), this::generateMethod));

		indToM.values().forEach(c.getMembers()::add);
		Map<EClass, ConcreteMethod> cToM = indToM.entrySet().stream()
				.collect(Collectors.toConcurrentMap(e -> getEClass(e.getKey()), Map.Entry::getValue));
		indToM.entrySet().forEach(e -> populateMethod(cToM, e.getKey(), e.getValue()));

		System.out.println("generation: ...");
		Unit u = MyjavaFactory.eINSTANCE.createUnit();
		u.getImports().add(c);
		u.getImports().addAll(externals.values());
		List<Unit> l = new LinkedList<>();
		l.add(u);
		return l;
	}

	private String getQualified(List<EObject> source, String name) {
		Optional<EPackage> x = source.stream().map(map::get).flatMap(Set::stream).filter(a -> a instanceof EPackage)
				.map(a -> (EPackage) a).findAny();
		return x.map(ePackageToGenPackage::get).map(GenPackage::getQualifiedPackageName).map(a -> a + ".gen." + name)
				.orElse(name);
	}

	private EClass getEClass(SIInductive name) {
		return siInductiveToEClass.get(name);
	}

	private EClass getEClass(NREClass name) {
		return nreClassToEClass.get(name);
	}

	private Clazz getExternalClass(Class<?> c) {
		return getExternalClass(c.getCanonicalName());
	}

	private Clazz getExternalClass(String name) {
		return (Clazz) externals.computeIfAbsent(name, n -> createExternalClassOfString((String) n));
	}

	private Clazz getExternalClass(SIInductive name) {
		return getExternalClass(getEClass(name));
	}

	private Clazz getExternalClass(SIConstructor name) {
		return getExternalClass(siConstructorToEClass.get(name));
	}

	private Clazz getExternalClass(EClass name) {
		return getExternalClass(eClassToGenClass.get(name));
	}

	private Clazz getExternalClass(GenClass name) {
		return (Clazz) externals.computeIfAbsent(name, n -> createExternalClassOfGenClass((GenClass) n));
	}

	private GetGenFeatureCallable getExternalGetter(GenFeature name) {
		return (GetGenFeatureCallable) externals.computeIfAbsent(name, n -> createGet((GenFeature) n));
	}

	private GenOperationCallable getExternalOperation(GenOperation op) {
		return (GenOperationCallable) externals.computeIfAbsent(op, n -> createGenOperationCallable((GenOperation) n));
	}

	private JavaMethodCallable getExternalOperation(Method op) {
		return (JavaMethodCallable) externals.computeIfAbsent(op,
				n -> createJavaMethodCallable(((Method) n).getName()));
	}

	private ConcreteMethod generateMethod(SIInductive i) {
		FormalParameter in = createFormalParameter(createClassType(getExternalClass(i)), "in");
		ConcreteMethod m = createConcreteMethod(Visibility.PUBLIC, true, createClassParameterType(def), i.getName(),
				new FormalParameter[] { in },
				new InstantiableClassType[] { createClassType(getExternalClass(IllegalArgumentException.class)) });
		hookMapping(i, m);
		return m;
	}

	private void populateMethod(Map<EClass, ConcreteMethod> cToM, SIInductive i, ConcreteMethod m) {
		FormalParameter in = m.getFormalParameters().get(0);
		Throw t = createThrow(createNewObject(createClassType(getExternalClass(IllegalArgumentException.class))));
		LocalVariable eClass = createLocalVariable(true, createClassType(getExternalClass(EClass.class)), "eClass",
				createReifiedEClass(eClassToGenClass.get(siInductiveToEClass.get(i))));
		m.getBody().add(eClass);
		LocalVariable cached = createLocalVariable(true, createClassParameterType(def), "cached",
				createCall(createThis(), get, createReference(in), createReference(eClass)));
		m.getBody().add(cached);
		Chained<Statement> c = i.getConstructors().stream().map(x -> constructorToFragment(cToM, in, x, eClass))
				.collect(Chained::id, Chained::combine, Chained::combine);
		m.getBody().add(createIfThenElse(createDifferent(createReference(cached), createNull()),
				new Statement[] { createReturn(createReference(cached)) }, new Statement[] { c.get(t) }));
	}

	private Chained<Statement> constructorToFragment(Map<EClass, ConcreteMethod> cToM, Binder b, SIConstructor c,
			Binder eClass) {
		RChained<Expression> e = c.getParameters().stream().map(p -> parameterToFragment(cToM, b, p, eClass, c))
				.collect(RChained::id, RChained::combine, RChained::combine);
		Expression ctor = createCall(createThis(), getConstructor, createStringLiteral(c.getName()));
		Expression x = e.get(ctor);
		IfThenElse ite = createIfThen(
				createCall(createReifiedEClass(eClassToGenClass.get(siConstructorToEClass.get(c))),
						getExternalOperation(object_equals),
						createCall(createReference(b), getExternalOperation(eObject_eClass))),
				createReturn(createCall(createThis(), createFresh, createReference(b), createReference(eClass), x)));
		hookMapping(c, ite);
		return new Chained<>(() -> ite, ite.getElse()::add);
	}

	private RChained<Expression> parameterToFragment(Map<EClass, ConcreteMethod> cToM, Binder b, SIParameter p,
			Binder eClass, SIConstructor c) {
		EStructuralFeature esf = siParameterToEStructuralFeature.get(p);
		GenFeature gf = eStructuralFeatureToGenFeature.get(esf);
		Expression exprGet = createCall(createCast(createClassType(getExternalClass(c)), createReference(b)),
				getExternalGetter(gf));
		NREStructuralFeature nsf = siParameterToNREStructuralFeature.get(p);
		Expression wrapped = addWrappers(cToM, b, gf, nsf.getEType(), exprGet);
		// TODO: list, option, non-container, ...
		Expression placeHolder = createStringLiteral("place holder for " + p.getName());
		Call r = createCall(createThis(), apply, placeHolder, wrapped);
		return new RChained<>(() -> r, x -> {
			if (r.getEffectiveParameters().get(0) == placeHolder) {
				r.getEffectiveParameters().set(0, x);
			} else {
				throw new IllegalStateException();
			}
		});
	}

	private Expression addWrappers(Map<EClass, ConcreteMethod> cToM, Binder b, GenFeature gf, NREType t, Expression e) {
		if (t instanceof NREClassifierType) {
			NREClassifierType ct = (NREClassifierType) t;
			NREClassifier c = ct.getEClassifier();
			if (c instanceof NREClass) {
				return createCall(createThis(), reference,
						createCall(createThis(), cToM.get(getEClass((NREClass) c)), e));
			} else if (c instanceof NREDataType) {
				if (c instanceof NREEnum) {
					// TODO
					return e;
					// throw new AssertionError();
				} else {
					NREDataType dt = (NREDataType) c;
					switch (dt.getInstanceClassName()) {
					case "java.lang.String":
						return createCall(createThis(), wrapString, e);
					case "java.lang.Boolean":
					case "boolean":
						return createCall(createThis(), wrapBoolean, e);
					case "java.lang.Byte":
					case "byte":
						return createCall(createThis(), wrapByte, e);
					case "java.lang.Short":
					case "short":
						return createCall(createThis(), wrapShort, e);
					case "java.lang.Integer":
					case "int":
						return createCall(createThis(), wrapInt, e);
					case "java.lang.Long":
					case "long":
						return createCall(createThis(), wrapLong, e);
					case "java.math.BigInteger":
						return createCall(createThis(), wrapBigInteger, e);
					case "java.util.Optional": {
						List<NREType> args = ct.getEArguments();
						if (args.size() == 1 && b != null && gf != null) {
							return createConditional(
									createCall(createReference(b), getExternalOperation(eObject_eIsSet),
											createReifiedEStructuralFeature(gf)),
									createCall(createThis(), some, addWrappers(cToM, null, null, args.get(0), e)),
									createCall(createThis(), none));
						} else {
							throw new AssertionError();
						}
					}
					case "org.eclipse.emf.common.util.URI":
					case "my.ecore2coq.utils.URI": {
						List<NREType> args = ct.getEArguments();
						if (args.size() == 1) {
							NREType p = args.get(0);
							if (p instanceof NREClassifierType) {
								NREClassifierType pct = (NREClassifierType) p;
								NREClassifier pc = pct.getEClassifier();
								if (pc instanceof NREClass) {
									return createCall(createThis(), createUri,
											createCall(createThis(), cToM.get(getEClass((NREClass) pc)), e));
								} else {
									throw new AssertionError();
								}
							} else {
								throw new AssertionError();
							}
						} else {
							throw new AssertionError();
						}
					}
					case "java.util.List":
					case "org.eclipse.emf.common.util.EList":
						return createStream(wrapList, cToM, ct, e);
					case "my.ecore2coq.utils.NoDupList":
						return createStream(wrapNoDupList, cToM, ct, e);
					case "my.ecore2coq.utils.Bag":
						return createStream(wrapBag, cToM, ct, e);
					case "java.util.Set":
						return createStream(wrapSet, cToM, ct, e);
					case "java.util.Map$Entry":
						return createCall(createThis(), wrapMapEntry, e);
					default:
						// TODO
						throw new AssertionError();
					}
				}
			} else if (c instanceof NRETypeAlias) {
				NRETypeAlias alias = (NRETypeAlias) c;
				return addWrappers(cToM, b, gf, alias.getType(), e);
			} else {
				throw new AssertionError();
			}
		} else {
			throw new AssertionError();
		}
	}

	private Expression createStream(AbstractMethod wrap, Map<EClass, ConcreteMethod> cToM, NREClassifierType ct,
			Expression e) {
		List<NREType> args = ct.getEArguments();
		if (args.size() == 1) {
			NREType p = args.get(0);
			FormalParameter fp = createFormalParameter(getType(p), "x");
			return createCall(createThis(), wrap,
					createCall(createCall(e, getExternalOperation(collection_stream)), getExternalOperation(stream_map),
							createLambda(addWrappers(cToM, null, null, p, createReference(fp)), fp)));
		} else {
			throw new AssertionError();
		}
	}

	private Type getType(NREType t) {
		if (t instanceof NREClassifierType) {
			NREClassifierType ct = (NREClassifierType) t;
			NREClassifier c = ct.getEClassifier();
			if (c instanceof NREClass) {
				return createClassType(getExternalClass(nreClassToEClass.get((NREClass) c)));
			} else if (c instanceof NREDataType) {
				if (c instanceof NREEnum) {
					// TODO
					throw new AssertionError();
				} else {
					NREDataType dt = (NREDataType) c;
					switch (dt.getInstanceClassName()) {
					case "java.lang.String":
						return createClassType(getExternalClass(String.class));
					case "java.lang.Boolean":
					case "boolean":
						return createClassType(getExternalClass(Boolean.class));
					case "java.lang.Byte":
					case "byte":
						return createClassType(getExternalClass(Byte.class));
					case "java.lang.Short":
					case "short":
						return createClassType(getExternalClass(Short.class));
					case "java.lang.Integer":
					case "int":
						return createClassType(getExternalClass(Integer.class));
					case "java.lang.Long":
					case "long":
						return createClassType(getExternalClass(Long.class));
					case "java.math.BigInteger":
						return createClassType(getExternalClass(BigInteger.class));
					case "org.eclipse.emf.common.util.URI":
					case "my.ecore2coq.utils.URI": {
						List<NREType> args = ct.getEArguments();
						if (args.size() == 1) {
							NREType p = args.get(0);
							if (p instanceof NREClassifierType) {
								NREClassifierType pct = (NREClassifierType) p;
								NREClassifier pc = pct.getEClassifier();
								if (pc instanceof NREClass) {
									return getType(p);
								} else {
									throw new AssertionError();
								}
							} else {
								throw new AssertionError();
							}
						} else {
							throw new AssertionError();
						}
					}
					case "java.util.Map$Entry":
						return createClassType(getExternalClass(Map.Entry.class));
					default:
						// TODO
						throw new AssertionError();
					}
				}
			} else if (c instanceof NRETypeAlias) {
				NRETypeAlias alias = (NRETypeAlias) c;
				return getType(alias.getType());
			} else {
				throw new AssertionError();
			}
		} else {
			throw new AssertionError();
		}
	}

	private static Stream<SIInductive> streamInductiveTypes(SIDefinition d) {
		if (d instanceof SIInductiveGroupDefinition) {
			return ((SIInductiveGroupDefinition) d).getInductives().stream();
		} else if (d instanceof SISimpleDefinition) {
			SIType t = ((SISimpleDefinition) d).getType();
			if (t instanceof SIInductive) {
				return Stream.of((SIInductive) t);
			} else {
				return Stream.empty();
			}
		} else {
			return Stream.empty();
		}
	}

	private static class Chained<T> {
		private Supplier<T> root;
		private Consumer<T> chain;
		private boolean consumed;

		public Chained(Supplier<T> root, Consumer<T> chain) {
			this.root = root;
			this.chain = chain;
		}

		public T get(T t) {
			if (consumed) {
				throw new IllegalStateException();
			} else {
				consumed = true;
				chain.accept(t);
				return root.get();
			}
		}

		public static <T> void combine(Chained<T> l, Chained<T> r) {
			if (r.consumed) {
				throw new IllegalStateException();
			} else {
				r.consumed = true;
				l.chain.accept(r.root.get());
				l.chain = r.chain;
			}
		}

		public static <T> Chained<T> id() {
			Holder<T> h = new Holder<>();
			return new Chained<>(h::get, h::accept);
		}
	}

	private static class RChained<T> {
		private boolean isId;
		private Supplier<T> root;
		private Consumer<T> chain;
		private boolean consumed;

		public RChained(boolean isId, Supplier<T> root, Consumer<T> chain) {
			this.isId = isId;
			this.root = root;
			this.chain = chain;
			this.consumed = false;
		}

		public RChained(Supplier<T> root, Consumer<T> chain) {
			this(false, root, chain);
		}

		public T get(T t) {
			if (consumed) {
				throw new IllegalStateException();
			} else {
				consumed = true;
				chain.accept(t);
				return root.get();
			}
		}

		public static <T> void combine(RChained<T> l, RChained<T> r) {
			if (l.consumed || r.consumed) {
				throw new IllegalStateException();
			} else {
				if (l.isId) {
					l.isId = r.isId;
					l.consumed = r.consumed;
					l.root = r.root;
					l.chain = r.chain;
					r.consumed = true;
				} else {
					r.consumed = true;
					r.chain.accept(l.root.get());
					l.root = r.root;
				}
			}
		}

		public static <T> RChained<T> id() {
			Holder<T> h = new Holder<>();
			return new RChained<>(true, h::get, h::accept);
		}
	}

	private static class Holder<T> {
		private T ref = null;

		public T get() {
			return ref;
		}

		public void accept(T r) {
			if (ref == null) {
				ref = r;
			} else {
				throw new IllegalStateException();
			}
		}
	}
}
