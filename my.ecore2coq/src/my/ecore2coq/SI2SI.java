package my.ecore2coq;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.resource.ResourceSet;

import mapping.MappingModel;
import my.transform.utils.Rule;
import scheduledinductive.SINamedElement;
import scheduledinductive.ScheduledinductivePackage;

public class SI2SI extends GenericRemapper {
	private Set<String> names;

	private static final Set<String> forbidden = new HashSet<>();

	private static final Pattern inIdent = Pattern.compile("\\W");

	static {
		forbidden.add("as");
		forbidden.add("at");
		forbidden.add("else");
		forbidden.add("end");
		forbidden.add("if");
		forbidden.add("in");
		forbidden.add("let");
		forbidden.add("match");
		forbidden.add("then");
		forbidden.add("with");
		forbidden.add("where");
	}

	public SI2SI(ResourceSet resourceSet, MappingModel mapping) {
		super(ScheduledinductivePackage.eINSTANCE, "SI", ScheduledinductivePackage.eINSTANCE, "SI", mapping);
		names = new TreeSet<>();
	}

	@Rule
	public void name(SINamedElement source, SINamedElement target) {
		String m = mangleName(source.getName());
		String n = clashless(m);
		target.setName(n);
	}

	private String clashless(String m) {
		if (names.add(m)) {
			return m;
		} else {
			for (int i = 0;; ++i) {
				String newName = m + "_" + i;
				if (names.add(newName)) {
					return newName;
				}
			}
		}
	}

	public static String mangleName(String i) {
		String n = specials(i);
		if (forbidden.contains(n)) {
			return mangle(n);
		} else {
			return n;
		}
	}

	private static String mangle(String name) {
		return "_" + name;
	}

	private static String specials(String n) {
		Matcher m = inIdent.matcher(n);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			m.appendReplacement(sb, "_" + encode(m.group().getBytes()) + "_");
		}
		m.appendTail(sb);
		String s = sb.toString();
		if (Character.isDigit(s.charAt(0))) {
			return "_" + s;
		} else {
			return s;
		}
	}

	private static String encode(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		for (byte i : bytes) {
			sb.append(Integer.toString(i, 16));
		}
		return sb.toString();
	}
}
