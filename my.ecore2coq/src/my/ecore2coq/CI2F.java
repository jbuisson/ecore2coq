package my.ecore2coq;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

import cleanimplecore.CIEAnyType;
import cleanimplecore.CIEClass;
import cleanimplecore.CIEClassifierType;
import cleanimplecore.CIEImplementation;
import cleanimplecore.CIELowerBoundType;
import cleanimplecore.CIEPackage;
import cleanimplecore.CIEType;
import cleanimplecore.CIETypeParameter;
import cleanimplecore.CIEUpperBoundType;
import cleanimplecore.CIEVariableType;
import cleanimplecore.CleanimplecoreFactory;
import cleanimplecore.CleanimplecorePackage;
import flattenedecore.FEClass;
import flattenedecore.FEImplementation;
import flattenedecore.FEType;
import flattenedecore.FlattenedecorePackage;
import mapping.MappingModel;
import my.ecore2coq.utils.Utils;
import my.transform.utils.Rule;

public class CI2F extends GenericRemapper {
	private final MappingModel mapping;

	public CI2F(ResourceSet resourceSet, MappingModel mapping) {
		super(CleanimplecorePackage.eINSTANCE, "CIE", FlattenedecorePackage.eINSTANCE, "FE", mapping);
		this.mapping = mapping;
	}

	@Rule
	public void eImplementations(CIEClass source, FEClass target) {
		EList<FEImplementation> implementations = target.getEImplementations();
		EObject root = EcoreUtil.getRootContainer(source);
		Stream.concat(Stream.of(root),
				StreamSupport.stream(Spliterators.spliteratorUnknownSize(root.eAllContents(), Spliterator.IMMUTABLE),
						false))
				.flatMap(Utils.filter(CIEPackage.class)).map(CIEPackage::getEImplementations).flatMap(EList::stream)
				.map(this::copy).flatMap((i) -> makeImplementation(source, i)).forEach(implementations::add);
	}

	private Stream<FEImplementation> makeImplementation(CIEClass sourceClass, CIEImplementation sourceImpl) {
		Optional<EList<CIEType>> o = collect(sourceClass, sourceImpl.getEClass(),
				makeArgs(sourceImpl.getETypeParameters())).reduce((l, r) -> {
					if (same(l, r)) {
						r.forEach(mapping.getTemporaryObjects()::add);
						return l;
					} else {
						throw new AssertionError(
								"in " + sourceClass.toString() + " : " + l.toString() + " != " + r.toString());
					}
				});
		Function<EObject, EObject> typeTransformer = maybeTransform(
				CleanimplecorePackage.Literals.CIE_IMPLEMENTATION__ETYPE_PARAMETERS,
				FlattenedecorePackage.Literals.FE_IMPLEMENTATION__RESULT_ARGUMENTS);
		Function<EObject, EObject> implTransformer = maybeTransform(
				CleanimplecorePackage.Literals.CIE_PACKAGE__EIMPLEMENTATIONS,
				FlattenedecorePackage.Literals.FE_CLASS__EIMPLEMENTATIONS);
		return o.map((x) -> {
			FEImplementation i = (FEImplementation) implTransformer.apply(sourceImpl);
			x.stream().map(typeTransformer).map(Utils.cast(FEType.class)).forEachOrdered(i.getResultArguments()::add);
			return Stream.of(i);
		}).orElse(Stream.empty());
	}

	private static boolean same(EList<CIEType> l, EList<CIEType> r) {
		if (l.size() == r.size()) {
			for (int i = 0; i < l.size(); ++i) {
				if (!same(l.get(i), r.get(i))) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	private static boolean same(CIEType l, CIEType r) {
		if (l == r) {
			return true;
		} else if (l.eClass().equals(r.eClass())) {
			if (l instanceof CIEClassifierType && r instanceof CIEClassifierType) {
				CIEClassifierType ll = (CIEClassifierType) l;
				CIEClassifierType rr = (CIEClassifierType) r;
				return ll.getEClassifier().equals(rr.getEClassifier()) && same(ll.getEArguments(), rr.getEArguments());
			} else if (l instanceof CIEVariableType && r instanceof CIEVariableType) {
				CIEVariableType ll = (CIEVariableType) l;
				CIEVariableType rr = (CIEVariableType) r;
				return ll.getETypeParameter().equals(rr.getETypeParameter());
			} else if (l instanceof CIEAnyType && r instanceof CIEAnyType) {
				return true;
			} else if (l instanceof CIELowerBoundType && r instanceof CIELowerBoundType) {
				CIELowerBoundType ll = (CIELowerBoundType) l;
				CIELowerBoundType rr = (CIELowerBoundType) r;
				return same(ll.getELowerBound(), rr.getELowerBound());
			} else if (l instanceof CIEUpperBoundType && r instanceof CIEUpperBoundType) {
				CIEUpperBoundType ll = (CIEUpperBoundType) l;
				CIEUpperBoundType rr = (CIEUpperBoundType) r;
				return same(ll.getEUpperBound(), rr.getEUpperBound());
			} else {
				throw new AssertionError(l.toString() + " / " + r.toString());
			}
		} else {
			return false;
		}
	}

	private Stream<EList<CIEType>> collect(CIEClass target, CIEClass current, EList<CIEType> params) {
		if (target.equals(current)) {
			return Stream.of(params);
		} else {
			if (current.getETypeParameters().size() != params.size()) {
				throw new AssertionError();
			}
			Map<CIETypeParameter, CIEType> subst = new HashMap<>();
			for (int i = 0; i < current.getETypeParameters().size(); ++i) {
				subst.put(current.getETypeParameters().get(i), copy(params.get(i)));
			}
			return current.getESuperTypes().stream().flatMap(Utils.filter(CIEClassifierType.class))
					.flatMap(
							(CIEClassifierType c) -> collect(target, (CIEClass) c.getEClassifier(),
									ECollections.asEList(c.getEArguments().stream()
											.map((x) -> new TypeSubstitution(subst, mapping).subst(x))
											.collect(Collectors.toList()))));
		}
	}

	private EList<CIEType> makeArgs(EList<CIETypeParameter> p) {
		return ECollections.asEList(p.stream().map(this::makeType).collect(Collectors.toList()));
	}

	private CIEType makeType(CIETypeParameter p) {
		CIEVariableType t = CleanimplecoreFactory.eINSTANCE.createCIEVariableType();
		mapping.getTemporaryObjects().add(t);
		t.setETypeParameter(p);
		return t;
	}
}
