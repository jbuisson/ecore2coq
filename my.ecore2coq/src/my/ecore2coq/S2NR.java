package my.ecore2coq;

import java.lang.reflect.TypeVariable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;

import mapping.MappingModel;
import my.ecore2coq.utils.Bag;
import my.ecore2coq.utils.NoDupList;
import my.ecore2coq.utils.URI;
import my.transform.utils.Rule;
import norefecore.NREAttribute;
import norefecore.NREClass;
import norefecore.NREClassifier;
import norefecore.NREClassifierType;
import norefecore.NREDataType;
import norefecore.NREPackage;
import norefecore.NREStructuralFeature;
import norefecore.NREType;
import norefecore.NRETypeParameter;
import norefecore.NRETypedElement;
import norefecore.NorefecoreFactory;
import norefecore.NorefecorePackage;
import structuralecore.SEClass;
import structuralecore.SEClassifier;
import structuralecore.SEPackage;
import structuralecore.SEReference;
import structuralecore.SEStructuralFeature;
import structuralecore.SETypedElement;
import structuralecore.StructuralecorePackage;

public class S2NR extends GenericRemapper {
	private final NREDataType nreURI;
	private final NREDataType nreList;
	private final NREDataType nreNoDupList;
	private final NREDataType nreSet;
	private final NREDataType nreBag;
	private final NREDataType nreMap;
	private final NREDataType nreMapEntry;
	private final NREDataType nreOptional;
	private final Map<String, NREClassifier> additions = new TreeMap<>();

	public S2NR(ResourceSet resourceSet, MappingModel mapping) {
		super(StructuralecorePackage.eINSTANCE, "SE", NorefecorePackage.eINSTANCE, "NRE", mapping);
		synchronized (resourceSet) {
			nreURI = makeDataType("_URI", URI.class);
			nreList = makeDataType("_List", List.class);
			nreNoDupList = makeDataType("_NoDupList", NoDupList.class);
			nreBag = makeDataType("_Bag", Bag.class);
			nreSet = makeDataType("_Set", Set.class);
			nreMap = makeDataType("_Map", Map.class);
			nreMapEntry = makeDataType("_MapEntry", Map.Entry.class);
			nreOptional = makeDataType("_Option", Optional.class);
		}
		Stream.of(nreURI, nreList, nreNoDupList, nreBag, nreSet, nreMap, nreMapEntry, nreOptional)
				.forEachOrdered((x) -> additions.put(x.getInstanceClassName(), x));
	}

	private static <X> NREDataType makeDataType(String name, Class<X> clazz) {
		TypeVariable<Class<X>>[] v = clazz.getTypeParameters();
		String[] w = new String[v.length];
		for (int i = 0; i < v.length; ++i) {
			w[i] = v[i].getName();
		}
		return makeDataType(name, clazz.getName(), w);
	}

	private static NREDataType makeDataType(String name, String instanceClassName, String... typeParameters) {
		NREDataType ret = NorefecoreFactory.eINSTANCE.createNREDataType();
		ret.setName(name);
		ret.setInstanceClassName(instanceClassName);
		Arrays.stream(typeParameters).map(S2NR::makeTypeParameter).forEachOrdered(ret.getETypeParameters()::add);
		return ret;
	}

	private static NRETypeParameter makeTypeParameter(String name) {
		NRETypeParameter ret = NorefecoreFactory.eINSTANCE.createNRETypeParameter();
		ret.setName(name);
		return ret;
	}

	@Rule
	public void eClassifiers(SEPackage source, NREPackage target) {
		source.getEClassifiers().stream().map(this::doTransform).forEachOrdered(target.getEClassifiers()::add);
		additions.values().stream().forEachOrdered(target.getEClassifiers()::add);
	}

	private NREClassifier doTransform(SEClassifier source) {
		Function<EObject, EObject> transformer = maybeTransform(
				StructuralecorePackage.Literals.SE_PACKAGE__ECLASSIFIERS,
				NorefecorePackage.Literals.NRE_PACKAGE__ECLASSIFIERS);
		return (NREClassifier) transformer.apply(source);
	}

	@Rule
	public void eStructuralFeatures(SEClass source, NREClass target) {
		Function<EObject, EObject> transformer = maybeTransform(
				StructuralecorePackage.Literals.SE_CLASS__ESTRUCTURAL_FEATURES,
				NorefecorePackage.Literals.NRE_CLASS__ESTRUCTURAL_FEATURES);
		EList<NREStructuralFeature> targetList = target.getEStructuralFeatures();
		Consumer<SEStructuralFeature> transformAndAdd = (sourceFeature) -> targetList
				.add((NREStructuralFeature) transformer.apply(sourceFeature));
		source.getEStructuralFeatures().forEach((sourceFeature) -> {
			if (sourceFeature instanceof SEReference) {
				SEReference sourceReference = (SEReference) sourceFeature;
				if (sourceReference.isContainment()) {
					transformAndAdd.accept(sourceFeature);
				} else {
					NREAttribute replacement = NorefecoreFactory.eINSTANCE.createNREAttribute();
					cloneInto(sourceFeature, replacement);
					replacement.setID(false);
					targetList.add(replacement);
				}
			} else {
				transformAndAdd.accept(sourceFeature);
			}
		});
	}

	@Rule
	public void eType(SEReference source, NREAttribute target) {
		Function<EObject, EObject> transformer = maybeTransform(StructuralecorePackage.Literals.SE_TYPED_ELEMENT__ETYPE,
				NorefecorePackage.Literals.NRE_TYPED_ELEMENT__ETYPE);
		if (source.eIsSet(StructuralecorePackage.Literals.SE_TYPED_ELEMENT__ETYPE)) {
			NREClassifierType type = NorefecoreFactory.eINSTANCE.createNREClassifierType();
			type.setEClassifier(nreURI);
			type.getEArguments().add((NREType) transformer.apply(source.getEType()));
			target.setEType(elaborateType(source, type));
		}
	}

	@Rule
	public void eType(SETypedElement source, NRETypedElement target) {
		if (source.eIsSet(StructuralecorePackage.Literals.SE_TYPED_ELEMENT__ETYPE)) {
			Function<EObject, EObject> transformer = maybeTransform(
					StructuralecorePackage.Literals.SE_TYPED_ELEMENT__ETYPE,
					NorefecorePackage.Literals.NRE_TYPED_ELEMENT__ETYPE);
			target.setEType(elaborateType(source, (NREType) transformer.apply(source.getEType())));
		}
	}

	private NREType elaborateType(SETypedElement source, NREType baseType) {
		if (source.getUpperBound() == 1) {
			if (source.getLowerBound() == 1) {
				return baseType;
			} else {
				return makeOptional(baseType);
			}
		} else {
			if (source.isOrdered()) {
				if (source.isUnique()) {
					return makeCollection(nreNoDupList, baseType);
				} else {
					return makeList(baseType);
				}
			} else {
				if (source.isUnique()) {
					return makeCollection(nreSet, baseType);
				} else {
					return makeCollection(nreBag, baseType);
				}
			}
		}
	}

	private NREType makeOptional(NREType baseType) {
		return makeCollection(nreOptional, baseType);
	}

	private NREType makeList(NREType baseType) {
		return makeCollection(nreList, baseType);
	}

	private NREType makeCollection(NREClassifier kind, NREType baseType) {
		NREClassifierType type = NorefecoreFactory.eINSTANCE.createNREClassifierType();
		type.setEClassifier(kind);
		type.getEArguments().add(baseType);
		return type;
	}
}
