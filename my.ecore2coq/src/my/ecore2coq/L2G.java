package my.ecore2coq;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;

import genericecore.GEClass;
import genericecore.GEClassifier;
import genericecore.GEClassifierType;
import genericecore.GELowerBoundType;
import genericecore.GEOperation;
import genericecore.GEType;
import genericecore.GETypeAlias;
import genericecore.GETypeParameter;
import genericecore.GETypedElement;
import genericecore.GEUpperBoundType;
import genericecore.GEVariableType;
import genericecore.GenericecoreFactory;
import genericecore.GenericecorePackage;
import linkedecore.LEClass;
import linkedecore.LEClassifier;
import linkedecore.LEGenericType;
import linkedecore.LEOperation;
import linkedecore.LETypeAlias;
import linkedecore.LETypeParameter;
import linkedecore.LETypedElement;
import linkedecore.LinkedecorePackage;
import mapping.MappingModel;
import my.transform.utils.Rule;

public class L2G extends GenericRemapper {
	public L2G(MappingModel mapping) {
		super(LinkedecorePackage.eINSTANCE, "LE", GenericecorePackage.eINSTANCE, "GE", mapping);
	}

	@Rule
	public void eType(LETypedElement source, GETypedElement target) {
		if (source.getEGenericType() == null) {
			LEClassifier c = source.getEType();
			if (c != null) {
				target.setEType(makeClassifierType(c));
			}
		} else {
			target.setEType(makeType(source.getEGenericType()));
		}
	}

	@Rule
	public void type(LETypeAlias source, GETypeAlias target) {
		target.setType(makeType(source.getType()));
	}

	@Rule
	public void eSuperTypes(LEClass source, GEClass target) {
		Set<LEClassifier> genericRaw = source.getEGenericSuperTypes().stream().map(LEGenericType::getEClassifier)
				.filter((k) -> k != null).collect(Collectors.toCollection(HashSet::new));
		EList<GEType> list = target.getESuperTypes();
		Stream.concat(source.getEGenericSuperTypes().stream().map(this::makeType),
				source.getESuperTypes().stream().filter((k) -> !genericRaw.contains(k)).map(this::makeClassifierType))
				.distinct().forEachOrdered(list::add);
	}

	@Rule
	public void eBounds(LETypeParameter source, GETypeParameter target) {
		EList<GEType> list = target.getEBounds();
		source.getEBounds().stream().map(this::makeType).forEachOrdered(list::add);
	}

	@Rule
	public void eExceptions(LEOperation source, GEOperation target) {
		EList<GEType> list = target.getEExceptions();
		source.getEGenericExceptions().stream().map(this::makeType).forEachOrdered(list::add);
	}

	private GEType makeType(LEGenericType gen) {
		if (gen.getEClassifier() != null) {
			assert gen.getETypeParameter() == null;
			assert gen.getELowerBound() == null;
			assert gen.getEUpperBound() == null;
			LEClassifier raw = gen.getEClassifier();
			GEClassifierType type = makeRawClassifierType(raw);
			EList<GEType> args = type.getEArguments();
			gen.getETypeArguments().stream().map(this::makeType).forEachOrdered(args::add);
			return type;
		} else if (gen.getETypeParameter() != null) {
			assert gen.getEClassifier() == null;
			assert gen.getELowerBound() == null;
			assert gen.getEUpperBound() == null;
			Function<EObject, EObject> transformPar = maybeTransform(
					EcorePackage.Literals.EGENERIC_TYPE__ETYPE_PARAMETER,
					GenericecorePackage.Literals.GE_VARIABLE_TYPE__ETYPE_PARAMETER);
			GEVariableType type = GenericecoreFactory.eINSTANCE.createGEVariableType();
			registerFinishingAction(() -> {
				type.setETypeParameter((GETypeParameter) transformPar.apply(gen.getETypeParameter()));
			});
			return type;
		} else if (gen.getELowerBound() != null) {
			assert gen.getEClassifier() == null;
			assert gen.getETypeParameter() == null;
			assert gen.getEUpperBound() == null;
			GELowerBoundType type = GenericecoreFactory.eINSTANCE.createGELowerBoundType();
			type.setELowerBound(makeType(gen.getELowerBound()));
			return type;
		} else if (gen.getEUpperBound() != null) {
			assert gen.getEClassifier() == null;
			assert gen.getETypeParameter() == null;
			assert gen.getELowerBound() == null;
			GEUpperBoundType type = GenericecoreFactory.eINSTANCE.createGEUpperBoundType();
			type.setEUpperBound(makeType(gen.getEUpperBound()));
			return type;
		} else {
			assert gen.getEClassifier() == null;
			assert gen.getETypeParameter() == null;
			assert gen.getELowerBound() == null;
			assert gen.getEUpperBound() == null;
			return GenericecoreFactory.eINSTANCE.createGEAnyType();
		}
	}

	private GEClassifierType makeClassifierType(LEClassifier raw) {
		GEClassifierType type = makeRawClassifierType(raw);
		for (int i = 0; i < raw.getETypeParameters().size(); ++i) {
			type.getEArguments().add(GenericecoreFactory.eINSTANCE.createGEAnyType());
		}
		return type;
	}

	private GEClassifierType makeRawClassifierType(LEClassifier raw) {
		GEClassifierType type = GenericecoreFactory.eINSTANCE.createGEClassifierType();
		Function<EObject, EObject> transformRaw = maybeTransform(EcorePackage.Literals.EGENERIC_TYPE__ECLASSIFIER,
				GenericecorePackage.Literals.GE_CLASSIFIER_TYPE__ECLASSIFIER);
		registerFinishingAction(() -> {
			type.setEClassifier((GEClassifier) transformRaw.apply(raw));
		});
		return type;
	}
}
