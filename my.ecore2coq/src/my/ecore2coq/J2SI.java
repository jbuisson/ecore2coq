package my.ecore2coq;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

import javainductive.JConstructor;
import javainductive.JDataType;
import javainductive.JGAnyType;
import javainductive.JGClassifierType;
import javainductive.JGLowerBoundType;
import javainductive.JGType;
import javainductive.JGUpperBoundType;
import javainductive.JGVariableType;
import javainductive.JInductive;
import javainductive.JPackage;
import javainductive.JParameter;
import javainductive.JType;
import javainductive.JTypeDef;
import javainductive.JavainductivePackage;
import mapping.MappingModel;
import my.ecore2coq.utils.Utils;
import my.transform.utils.Rule;
import scheduledinductive.SIConstructor;
import scheduledinductive.SIDataType;
import scheduledinductive.SIDefinition;
import scheduledinductive.SIGAnyType;
import scheduledinductive.SIGClassifierType;
import scheduledinductive.SIGLowerBoundType;
import scheduledinductive.SIGType;
import scheduledinductive.SIGUpperBoundType;
import scheduledinductive.SIGVariableType;
import scheduledinductive.SIInductive;
import scheduledinductive.SIInductiveGroupDefinition;
import scheduledinductive.SIPackage;
import scheduledinductive.SIParameter;
import scheduledinductive.SISimpleDefinition;
import scheduledinductive.SIType;
import scheduledinductive.SITypeDef;
import scheduledinductive.ScheduledinductiveFactory;
import scheduledinductive.ScheduledinductivePackage;

public class J2SI extends GenericRemapper {
	public J2SI(ResourceSet resourceSet, MappingModel mapping) {
		super(JavainductivePackage.eINSTANCE, "J", ScheduledinductivePackage.eINSTANCE, "SI", mapping);
	}

	@Rule
	public void definitions(JPackage source, SIPackage target) {
		Function<EObject, EObject> transSimple = maybeTransform(JavainductivePackage.Literals.JPACKAGE__TYPES,
				ScheduledinductivePackage.Literals.SI_SIMPLE_DEFINITION__TYPE);
		Function<EObject, EObject> transGroup = maybeTransform(JavainductivePackage.Literals.JPACKAGE__TYPES,
				ScheduledinductivePackage.Literals.SI_INDUCTIVE_GROUP_DEFINITION__INDUCTIVES);
		Stream<JType> types = StreamSupport
				.stream(Spliterators.spliteratorUnknownSize(EcoreUtil.getRootContainer(source).eAllContents(),
						Spliterator.IMMUTABLE), false)
				.flatMap(Utils.filter(JType.class));
		Collection<Set<JType>> components = Utils.stronglyConnectedComponents(types, (t) -> streamReferencedTypes(t));
		List<SIDefinition> unsortedDefinitions = components.stream()
				.map((c) -> makeDefinition(transSimple, transGroup, c))
				.sorted((l, r) -> l.getName().compareTo(r.getName())).collect(Collectors.toList());
		registerFinishingAction(() -> {
			List<SIDefinition> sortedDefinitions = Utils.topologicalSort(unsortedDefinitions.stream(),
					(c) -> successorsOf(c, unsortedDefinitions));
			target.getDefinitions().addAll(sortedDefinitions);
		});
	}

	private static Stream<SIDefinition> successorsOf(SIDefinition node, Collection<SIDefinition> all) {
		Set<? extends SIType> types = streamDefinedTypes(node).collect(Collectors.toSet());
		Stream<SIDefinition> succ = all.stream().filter((c) -> streamReferencedTypes(c).anyMatch(types::contains))
				.filter((c) -> !node.equals(c));
		return succ;
	}

	private static SIDefinition makeDefinition(Function<EObject, EObject> transSimple,
			Function<EObject, EObject> transGroup, Set<JType> c) throws AssertionError {
		List<JType> src = c.stream().sorted((l, r) -> l.getName().compareTo(r.getName())).collect(Collectors.toList());
		if (src.size() == 1) {
			JType t = src.get(0);
			SISimpleDefinition sd = ScheduledinductiveFactory.eINSTANCE.createSISimpleDefinition();
			sd.setName(t.getName());
			sd.setType((SIType) transSimple.apply(t));
			return sd;
		} else if (src.size() > 1) {
			SIInductiveGroupDefinition igd = ScheduledinductiveFactory.eINSTANCE.createSIInductiveGroupDefinition();
			String name = src.stream().map(JType::getName).sorted().collect(Collectors.joining("__"));
			igd.setName(name);
			EList<SIInductive> i = igd.getInductives();
			src.stream().map(transGroup).map(Utils.cast(SIInductive.class)).forEach(i::add);
			return igd;
		} else {
			throw new AssertionError();
		}
	}

	private static Stream<JType> streamReferencedTypes(JType t) {
		Function<JType, JType> checkNonNull = (x) -> {
			if (x == null) {
				throw new AssertionError(t.toString());
			}
			return x;
		};
		if (t instanceof JDataType) {
			return Stream.empty();
		} else if (t instanceof JInductive) {
			return ((JInductive) t).getConstructors().stream().flatMap(J2SI::streamReferencedTypes).map(checkNonNull);
		} else if (t instanceof JTypeDef) {
			return Stream.of(((JTypeDef) t).getType()).flatMap(J2SI::streamReferencedTypes).map(checkNonNull);
		} else {
			throw new AssertionError(t.toString());
		}
	}

	private static Stream<JType> streamReferencedTypes(JConstructor c) {
		Function<JType, JType> checkNonNull = (x) -> {
			if (x == null) {
				throw new AssertionError(c.toString());
			}
			return x;
		};
		return c.getParameters().stream().map(JParameter::getEType).flatMap(J2SI::streamReferencedTypes)
				.map(checkNonNull);
	}

	private static Stream<JType> streamReferencedTypes(JGType t) {
		if (t instanceof JGClassifierType) {
			JGClassifierType c = (JGClassifierType) t;
			return Stream.concat(Stream.of(c.getEClassifier()),
					c.getEArguments().stream().flatMap(J2SI::streamReferencedTypes));
		} else if (t instanceof JGUpperBoundType) {
			return streamReferencedTypes(((JGUpperBoundType) t).getEUpperBound());
		} else if (t instanceof JGLowerBoundType) {
			return Stream.empty();
		} else if (t instanceof JGAnyType) {
			return Stream.empty();
		} else if (t instanceof JGVariableType) {
			return Stream.empty();
		} else {
			throw new AssertionError(t.toString());
		}
	}

	private static Stream<SIType> streamReferencedTypes(SIDefinition d) {
		return streamDefinedTypes(d).flatMap(J2SI::streamReferencedTypes);
	}

	private static Stream<SIType> streamReferencedTypes(SIType type) {
		if (type instanceof SIDataType) {
			return Stream.empty();
		} else if (type instanceof SIInductive) {
			return ((SIInductive) type).getConstructors().stream().map(SIConstructor::getParameters)
					.flatMap(EList::stream).map(SIParameter::getEType).flatMap(J2SI::streamReferencedTypes);
		} else if (type instanceof SITypeDef) {
			return Stream.of(((SITypeDef) type).getType()).flatMap(J2SI::streamReferencedTypes);
		} else {
			throw new AssertionError();
		}
	}

	private static Stream<SIType> streamReferencedTypes(SIGType t) {
		if (t instanceof SIGClassifierType) {
			SIGClassifierType c = (SIGClassifierType) t;
			return Stream.concat(Stream.of(c.getEClassifier()),
					c.getEArguments().stream().flatMap(J2SI::streamReferencedTypes));
		} else if (t instanceof SIGUpperBoundType) {
			return streamReferencedTypes(((SIGUpperBoundType) t).getEUpperBound());
		} else if (t instanceof SIGLowerBoundType) {
			return Stream.empty();
		} else if (t instanceof SIGAnyType) {
			return Stream.empty();
		} else if (t instanceof SIGVariableType) {
			return Stream.empty();
		} else {
			throw new AssertionError(t.toString());
		}
	}

	private static Stream<? extends SIType> streamDefinedTypes(SIDefinition d) {
		if (d instanceof SISimpleDefinition) {
			return Stream.of(((SISimpleDefinition) d).getType());
		} else if (d instanceof SIInductiveGroupDefinition) {
			return ((SIInductiveGroupDefinition) d).getInductives().stream();
		} else {
			throw new AssertionError();
		}
	}
}
