package my.ecore2coq;

import java.util.List;
import java.util.function.Function;

import org.eclipse.emf.ecore.EObject;

import genericecore.GEAnnotation;
import genericecore.GEClass;
import genericecore.GenericecorePackage;
import mapping.MappingModel;
import my.ecore2coq.utils.Utils;
import my.transform.utils.Rule;
import structuralecore.SEAnnotation;
import structuralecore.SEClass;
import structuralecore.SEStructuralFeature;
import structuralecore.StructuralecorePackage;

public class G2S extends GenericRemapper {
	public G2S(MappingModel mapping) {
		super(GenericecorePackage.eINSTANCE, "GE", StructuralecorePackage.eINSTANCE, "SE", mapping);
	}

	@Rule
	public void eStructuralFeatures(GEClass sourceClass, SEClass targetClass) {
		Function<EObject, EObject> transformer = maybeTransform(
				GenericecorePackage.Literals.GE_CLASS__ESTRUCTURAL_FEATURES,
				StructuralecorePackage.Literals.SE_CLASS__ESTRUCTURAL_FEATURES);
		List<SEStructuralFeature> targetList = targetClass.getEStructuralFeatures();
		sourceClass.getEStructuralFeatures().stream()
				.filter((sourceFeature) -> !sourceFeature.isDerived() && !sourceFeature.isTransient()).map(transformer)
				.map(Utils.cast(SEStructuralFeature.class)).forEach(targetList::add);
	}

	@Rule
	public void contents(GEAnnotation source, SEAnnotation target) {
		Function<EObject, EObject> transformer = maybeTransform(GenericecorePackage.Literals.GE_ANNOTATION__CONTENTS,
				StructuralecorePackage.Literals.SE_ANNOTATION__CONTENTS);
		List<EObject> targetList = target.getContents();
		source.getContents().stream().filter(this::retains).map(transformer).forEach(targetList::add);
	}

	@Rule
	public void references(GEAnnotation source, SEAnnotation target) {
		Function<EObject, EObject> transformer = maybeTransform(GenericecorePackage.Literals.GE_ANNOTATION__REFERENCES,
				StructuralecorePackage.Literals.SE_ANNOTATION__REFERENCES);
		List<EObject> targetList = target.getContents();
		source.getContents().stream().filter(this::retains).map((x) -> {
			EObject r = transformer.apply(x);
			if (r == null) {
				throw new AssertionError(x.toString());
			}
			return r;
		}).forEach(targetList::add);
	}

	private boolean retains(EObject o) {
		return optionallyRemap(o.eClass()).isPresent();
	}
}
