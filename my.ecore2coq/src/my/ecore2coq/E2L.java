package my.ecore2coq;

import java.lang.reflect.TypeVariable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.ExtendedMetaData;

import linkedecore.LEAttribute;
import linkedecore.LEClass;
import linkedecore.LEClassifier;
import linkedecore.LEGenericType;
import linkedecore.LEPackage;
import linkedecore.LEStructuralFeature;
import linkedecore.LETypeAlias;
import linkedecore.LinkedecoreFactory;
import linkedecore.LinkedecorePackage;
import mapping.MappingModel;
import my.ecore2coq.utils.Singleton;
import my.ecore2coq.utils.Utils;
import my.transform.utils.Rule;
import my.utils.MapSet;
import my.utils.OrderPreservingMap;
import pseudo.PseudoPackage;
import pseudo.PseudoRoot;

public class E2L extends GenericRemapper {
	private final Map<Class<?>, EDataType> additions = new OrderPreservingMap<>();
	private final Map<EClassifier, EGenericType> substitutes = new HashMap<>();
	private final Singleton<ExtendedMetaData> emd = new Singleton<>();
	private final MappingModel mapping;

	public E2L(MappingModel mapping) {
		super(EcorePackage.eINSTANCE, "E", LinkedecorePackage.eINSTANCE, "LE", mapping);
		this.mapping = mapping;
	}

	@Override
	protected EClass remapEClass(EClass sourceClass) {
		if (PseudoPackage.Literals.PSEUDO_ROOT.equals(sourceClass)) {
			return LinkedecorePackage.Literals.LE_PACKAGE;
		} else {
			return super.remapEClass(sourceClass);
		}
	}

	private Registry getPackageRegistry(EObject o) {
		Resource r = o.eResource();
		if (r != null) {
			ResourceSet s = r.getResourceSet();
			if (s != null) {
				return s.getPackageRegistry();
			} else {
				return Registry.INSTANCE;
			}
		} else {
			return Registry.INSTANCE;
		}
	}

	@Override
	public void prevalidate(EObject o) {
		if (o instanceof EPackage) {
			validateEPackage((EPackage) o);
			this.emd.get(() -> new BasicExtendedMetaData(getPackageRegistry(o)));
		} else if (o instanceof PseudoRoot) {
			PseudoRoot r = (PseudoRoot) o;
			r.getRoots().forEach(this::validateEPackage);
			List<Registry> l = r.getRoots().stream().map(this::getPackageRegistry).collect(Collectors.toList());
			if (l.isEmpty()) {
				throw new AssertionError("empty pseudo root");
			} else {
				Registry reg = l.get(0);
				if (l.size() > 1) {
					if (!l.stream().allMatch((x) -> reg == x)) {
						throw new AssertionError("several package registries");
					}
				}
				this.emd.get(() -> new BasicExtendedMetaData(reg));
			}
		} else {
			throw new AssertionError("not a package");
		}
	}

	private void validateEPackage(EPackage o) throws AssertionError {
		Diagnostic d = Diagnostician.INSTANCE.validate(o);
		if (d.getSeverity() != Diagnostic.OK) {
			throw new AssertionError(new DiagnosticException(d));
		}
	}

	@Rule
	public void name(EClassifier source, LEClassifier target) {
		target.setName(fdqn(source).collect(Collectors.joining("_")));
	}

	private static Stream<String> fdqn(EObject i) {
		if (i == null) {
			return Stream.empty();
		} else if (i instanceof ENamedElement) {
			return Stream.concat(fdqn(i.eContainer()), Stream.of(((ENamedElement) i).getName()));
		} else {
			return Stream.empty();
		}
	}

	@Rule
	public void eSuperTypes(EClass source, LEClass target) {
		Function<EObject, EObject> transformer = maybeTransform(EcorePackage.Literals.ECLASS__ESUPER_TYPES,
				LinkedecorePackage.Literals.LE_CLASS__ESUPER_TYPES);
		EList<LEClass> list = target.getESuperTypes();
		if (!EcorePackage.Literals.EOBJECT.equals(source)) {
			list.add((LEClass) transformer.apply(EcorePackage.Literals.EOBJECT));
		}
		source.getESuperTypes().stream().map(transformer).map(Utils.cast(LEClass.class)).forEachOrdered(list::add);
	}

	@Rule
	public void eClassifiers(PseudoRoot source, LEPackage target) {
		doEClassifiers(source.getRoots(), target);
	}

	@Rule
	public void eClassifiers(EPackage source, LEPackage target) {
		List<EPackage> l = new LinkedList<>();
		l.add(source);
		doEClassifiers(l, target);
	}

	@Rule
	public void eStructuralFeatures(EClass source, LEClass target) {
		Function<EObject, EObject> transformer = maybeTransform(EcorePackage.Literals.ECLASS__ESTRUCTURAL_FEATURES,
				LinkedecorePackage.Literals.LE_CLASS__ESTRUCTURAL_FEATURES);
		source.getEStructuralFeatures().stream().map((sourceFeature) -> {
			if (sourceFeature instanceof EReference && substitutes.containsKey(sourceFeature.getEType())) {
				LEAttribute targetFeature = LinkedecoreFactory.eINSTANCE.createLEAttribute();
				cloneInto(sourceFeature, targetFeature);
				return targetFeature;
			} else {
				return (LEStructuralFeature) transformer.apply(sourceFeature);
			}
		}).forEach(target.getEStructuralFeatures()::add);
	}

	private void doEClassifiers(List<EPackage> source, LEPackage target) {
		Set<EClassifier> allClassifiers = new MapSet<>(OrderPreservingMap::new);
		allClassifiers.addAll(Utils.transitiveClosure(source.stream().flatMap((p) -> streamPackages(p, target))
				.map(EPackage::getEClassifiers).flatMap(Collection::stream), this::referencedClassifiers));
		allClassifiers.addAll(additions.values());
		allClassifiers.stream().filter((k) -> k != null).map(this::transformClassifier)
				.map(Utils.cast(LEClassifier.class)).sorted((l, r) -> l.getName().compareTo(r.getName()))
				.forEachOrdered(target.getEClassifiers()::add);
	}

	private LEClassifier transformClassifier(EClassifier source) {
		if (substitutes.containsKey(source)) {
			LETypeAlias alias = LinkedecoreFactory.eINSTANCE.createLETypeAlias();
			alias.setName(source.getName());
			Function<EObject, EObject> transformer = maybeTransform(EcorePackage.Literals.ETYPED_ELEMENT__EGENERIC_TYPE,
					LinkedecorePackage.Literals.LE_TYPE_ALIAS__TYPE);
			alias.setType((LEGenericType) transformer.apply(substitutes.get(source)));
			registerTransformedOf(source, alias);
			return alias;
		} else {
			Function<EObject, EObject> transformer = maybeTransform(EcorePackage.Literals.EPACKAGE__ECLASSIFIERS,
					LinkedecorePackage.Literals.LE_PACKAGE__ECLASSIFIERS);
			return (LEClassifier) transformer.apply(source);
		}
	}

	private Stream<EPackage> streamPackages(EPackage pkg, LEPackage target) {
		registerTransformedOf(pkg, target);
		return Stream.concat(Stream.of(pkg), pkg.getESubpackages().stream().flatMap((p) -> streamPackages(p, target)));
	}

	private Stream<EClassifier> referencedClassifiers(EClassifier c) {
		Stream<EClassifier> fromAnnotations = streamAnnotations(c).flatMap(E2L::referencedClassifiers);
		return Stream.concat(fromAnnotations, referencedClassifiers_(c));
	}

	private Stream<EClassifier> referencedClassifiers_(EClassifier c) {
		if (c instanceof EClass) {
			EClass cl = (EClass) c;
			EGenericType t = getSubstituteOf(cl);
			if (t == null) {
				Stream<EClassifier> r = Stream.of(
						cl.getEGenericSuperTypes().stream().flatMap(E2L::referencedClassifiers),
						cl.getESuperTypes().stream(), cl.getEOperations().stream().flatMap(E2L::referencedClassifiers),
						cl.getEStructuralFeatures().stream().flatMap(E2L::referencedClassifiers),
						cl.getETypeParameters().stream().flatMap(E2L::referencedClassifiers)).flatMap((i) -> i);
				if (EcorePackage.Literals.EOBJECT.equals(c)) {
					return r;
				} else {
					return Stream.concat(Stream.of(EcorePackage.Literals.EOBJECT), r);
				}
			} else {
				return referencedClassifiers(t);
			}
		} else if (c instanceof EDataType) {
			EDataType cl = (EDataType) c;
			EGenericType t = getSubstituteOf(cl);
			if (t == null) {
				return cl.getETypeParameters().stream().flatMap(E2L::referencedClassifiers);
			} else {
				return referencedClassifiers(t);
			}
		} else {
			return Stream.empty();
		}
	}

	private EGenericType getSubstituteOf(EClass cl) {
		EGenericType r = substitutes.get(cl);
		if (r == null) {
			EGenericType c = createSubstitute(cl);
			if (c != null) {
				EGenericType r2 = substitutes.get(cl);
				if (r2 == null) {
					substitutes.put(cl, c);
					return c;
				} else if (r2 == c) {
					return c;
				} else {
					throw new AssertionError();
				}
			} else {
				return c;
			}
		} else {
			return r;
		}
	}

	private EGenericType getSubstituteOf(EDataType cl) {
		EGenericType r = substitutes.get(cl);
		if (r == null) {
			EGenericType c = createSubstitute(cl);
			if (c != null) {
				EGenericType r2 = substitutes.get(cl);
				if (r2 == null) {
					substitutes.put(cl, c);
					return c;
				} else if (r2 == c) {
					return c;
				} else {
					throw new AssertionError();
				}
			} else {
				return c;
			}
		} else {
			return r;
		}
	}

	private EGenericType createSubstitute(EClass cl) {
		if (cl.eIsSet(EcorePackage.Literals.ECLASSIFIER__INSTANCE_CLASS_NAME)) {
			if (cl.getETypeParameters().size() != 0) {
				throw new AssertionError();
			} else {
				if ("java.util.Map$Entry".equals(cl.getInstanceClassName())) {
					if (cl.getEStructuralFeatures().size() == 2) {
						EStructuralFeature key = cl.getEStructuralFeature("key");
						EStructuralFeature value = cl.getEStructuralFeature("value");
						if (key instanceof EAttribute && value instanceof EAttribute) {
							EClassifier keyType = key.getEType();
							EClassifier valueType = value.getEType();
							if (keyType instanceof EDataType && valueType instanceof EDataType) {
								return makeMapEntryOf((EDataType) keyType, (EDataType) valueType);
							} else {
								throw new AssertionError();
							}
						} else {
							throw new AssertionError();
						}
					} else {
						throw new AssertionError();
					}
				} else {
					return parseType(cl.getInstanceClassName());
				}
			}
		} else {
			return null;
		}
	}

	private EGenericType createSubstitute(EDataType cl) {
		ExtendedMetaData emd = this.emd
				.get(() -> new BasicExtendedMetaData(cl.eResource().getResourceSet().getPackageRegistry()));
		EDataType baseType = emd.getBaseType(cl);
		if (baseType != null) {
			return getSubstituteOf(baseType);
		} else {
			if (cl.eIsSet(EcorePackage.Literals.ECLASSIFIER__INSTANCE_TYPE_NAME)) {
				if (cl.getETypeParameters().size() != 0) {
					throw new AssertionError();
				} else {
					return parseType(cl.getInstanceTypeName());
				}
			} else if (cl.eIsSet(EcorePackage.Literals.ECLASSIFIER__INSTANCE_CLASS_NAME)) {
				if (cl.getETypeParameters().size() == 0) {
					if ("java.util.List".contentEquals(cl.getInstanceClassName())) {
						EDataType itemType = emd.getItemType(cl);
						if (itemType != null) {
							return makeListOf(itemType);
						} else {
							throw new AssertionError();
						}
					} else {
						return null;
					}
				} else {
					return null;
				}
			} else {
				return null;
			}
		}
	}

	private EGenericType eGenericTypeOfEDataType(EDataType t) {
		EGenericType g = getSubstituteOf(t);
		if (g == null) {
			EGenericType p = EcoreFactory.eINSTANCE.createEGenericType();
			mapping.getTemporaryObjects().add(p);
			p.setEClassifier(t);
			return p;
		} else {
			return copy(g);
		}
	}

	private EGenericType makeListOf(EDataType itemType) {
		EGenericType type = EcoreFactory.eINSTANCE.createEGenericType();
		mapping.getTemporaryObjects().add(type);
		type.setEClassifier(getAddedDataType(List.class));
		type.getETypeArguments().add(eGenericTypeOfEDataType(itemType));
		return type;
	}

	private EGenericType makeMapEntryOf(EDataType keyType, EDataType valueType) {
		EGenericType type = EcoreFactory.eINSTANCE.createEGenericType();
		mapping.getTemporaryObjects().add(type);
		type.setEClassifier(getAddedDataType(Map.Entry.class));
		type.getETypeArguments().add(eGenericTypeOfEDataType(keyType));
		type.getETypeArguments().add(eGenericTypeOfEDataType(valueType));
		return type;
	}

	private static Stream<EClassifier> referencedClassifiers(ETypeParameter eTypeParameter) {
		if (eTypeParameter == null) {
			return Stream.empty();
		} else {
			return eTypeParameter.getEBounds().stream().flatMap(E2L::referencedClassifiers);
		}
	}

	private static Stream<EClassifier> referencedClassifiers(EStructuralFeature eStructuralFeature) {
		return Stream.concat(referencedClassifiers(eStructuralFeature.getEGenericType()),
				Stream.of(eStructuralFeature.getEType()));
	}

	private static Stream<EClassifier> referencedClassifiers(EOperation eOperation) {
		return Stream.concat(
				Stream.of(Stream.of(eOperation.getEGenericType()), eOperation.getEGenericExceptions().stream(),
						eOperation.getEParameters().stream().map(EParameter::getEGenericType)).flatMap((i) -> i)
						.flatMap(E2L::referencedClassifiers),
				Stream.of(Stream.of(eOperation.getEType()), eOperation.getEExceptions().parallelStream(),
						eOperation.getEParameters().stream().map(EParameter::getEType)).flatMap((i) -> i));
	}

	private static Stream<EClassifier> referencedClassifiers(EGenericType eGenericType) {
		return Utils.transitiveClosure(Stream.of(eGenericType), E2L::referencedGenericTypes).stream()
				.flatMap(E2L::classifiersOfGenericType);
	}

	private static Stream<EClassifier> classifiersOfGenericType(EGenericType eGenericType) {
		if (eGenericType == null) {
			return Stream.empty();
		} else {
			return Stream.of(eGenericType.getEClassifier(), eGenericType.getERawType()).filter((i) -> i != null);
		}
	}

	private static Stream<EGenericType> referencedGenericTypes(EGenericType eGenericType) {
		if (eGenericType == null) {
			return Stream.empty();
		} else {
			return Stream.of(Stream.of(eGenericType.getELowerBound(), eGenericType.getEUpperBound()),
					referencedGenericTypes(eGenericType.getETypeParameter()), eGenericType.getETypeArguments().stream())
					.flatMap((i) -> i).filter((i) -> i != null);
		}
	}

	private static Stream<EGenericType> referencedGenericTypes(ETypeParameter eTypeParameter) {
		if (eTypeParameter == null) {
			return Stream.empty();
		} else {
			return eTypeParameter.getEBounds().stream();
		}
	}

	private static List<String> typeTokenizer(String s) {
		List<String> r = new LinkedList<>();
		for (int i = 0; i < s.length();) {
			char x = s.charAt(i);
			if (x == '<' || x == '>' || x == '[' || x == ']' || x == '.' || x == ',') {
				r.add("" + x);
				++i;
			} else if (Character.isJavaIdentifierStart(x)) {
				StringBuilder sb = new StringBuilder();
				sb.append(x);
				++i;
				x = s.charAt(i);
				while (Character.isJavaIdentifierPart(x)) {
					sb.append(x);
					++i;
					x = s.charAt(i);
				}
				r.add(sb.toString());
			} else if (Character.isWhitespace(x)) {
				++i;
			} else {
				throw new AssertionError(s);
			}
		}
		return r;
	}

	private EGenericType parseType(String s) {
		List<String> tokens = typeTokenizer(s);
		Iterator<String> stream = tokens.iterator();
		if (stream.hasNext()) {
			String t = stream.next();
			EGenericType[] r = new EGenericType[1];
			Optional<String> next = expectType(stream, t, r);
			if (next.isPresent()) {
				throw new AssertionError();
			} else {
				return r[0];
			}
		} else {
			throw new AssertionError();
		}
	}

	private Optional<String> expectType(Iterator<String> stream, String first, EGenericType[] holder) {
		StringBuilder fqn = new StringBuilder();
		Optional<String> next = expectFqn(stream, first, fqn);
		EDataType raw = getAddedDataType(fqn.toString());
		EGenericType type = EcoreFactory.eINSTANCE.createEGenericType();
		mapping.getTemporaryObjects().add(type);
		type.setEClassifier(raw);
		holder[0] = type;
		if (next.isPresent()) {
			String n = next.get();
			if ("<".contentEquals(n)) {
				if (stream.hasNext()) {
					String second = stream.next();
					if (">".contentEquals(second)) {
						if (stream.hasNext()) {
							return Optional.of(stream.next());
						} else {
							return Optional.empty();
						}
					} else {
						Optional<String> last = expectTypeList(stream, second, type.getETypeArguments());
						if (last.isPresent()) {
							String l = last.get();
							if (">".contentEquals(l)) {
								if (stream.hasNext()) {
									return Optional.of(stream.next());
								} else {
									return Optional.empty();
								}
							} else {
								throw new AssertionError();
							}
						} else {
							throw new AssertionError();
						}
					}
				} else {
					throw new AssertionError();
				}

			} else {
				return next;
			}
		} else {
			return next;
		}
	}

	private EDataType getAddedDataType(String name) {
		try {
			return getAddedDataType(Class.forName(name));
		} catch (ClassNotFoundException e) {
			throw new AssertionError(name, e);
		}
	}

	private EDataType getAddedDataType(Class<?> clazz) {
		return additions.computeIfAbsent(clazz, this::createEDataType);
	}

	private <X> EDataType createEDataType(Class<X> clazz) {
		EDataType t = EcoreFactory.eINSTANCE.createEDataType();
		mapping.getTemporaryObjects().add(t);
		t.setName(mangle(clazz.getName()));
		t.setInstanceClassName(clazz.getName());
		TypeVariable<Class<X>>[] v = clazz.getTypeParameters();
		Arrays.stream(v).map(TypeVariable::getName).map(this::makeTypeParameter)
				.forEachOrdered(t.getETypeParameters()::add);
		return t;
	}

	private ETypeParameter makeTypeParameter(String name) {
		ETypeParameter ret = EcoreFactory.eINSTANCE.createETypeParameter();
		mapping.getTemporaryObjects().add(ret);
		ret.setName(name);
		return ret;
	}

	private Optional<String> expectTypeList(Iterator<String> stream, String first, List<EGenericType> types) {
		for (;;) {
			EGenericType[] type = new EGenericType[1];
			Optional<String> next = expectType(stream, first, type);
			types.add(type[0]);
			if (next.isPresent()) {
				String n = next.get();
				if (",".contentEquals(n)) {
					if (stream.hasNext()) {
						first = stream.next();
						continue;
					} else {
						throw new AssertionError();
					}
				} else {
					return next;
				}
			} else {
				return next;
			}
		}
	}

	private Optional<String> expectFqn(Iterator<String> stream, String first, StringBuilder fqn) {
		for (;;) {
			if (isIdentifier(first)) {
				fqn.append(first);
				if (stream.hasNext()) {
					String second = stream.next();
					if (".".contentEquals(second)) {
						fqn.append(second);
						if (stream.hasNext()) {
							String third = stream.next();
							first = third;
							continue;
						} else {
							throw new AssertionError();
						}
					} else {
						return Optional.of(second);
					}
				} else {
					return Optional.empty();
				}
			} else {
				throw new AssertionError(first);
			}
		}
	}

	private static boolean isIdentifier(String t) {
		return !("<".contentEquals(t) || ">".contentEquals(t) || "[".contentEquals(t) || "]".contentEquals(t)
				|| ".".contentEquals(t) || ",".contentEquals(t));
	}

	private static String mangle(String n) {
		StringBuilder sb = new StringBuilder("__");
		for (char i : n.toCharArray()) {
			if (i == '_') {
				sb.append("__");
			} else if (i == '$') {
				sb.append("_s");
			} else if (i == '.') {
				sb.append("_");
			} else if (i == '/') {
				sb.append("_");
			} else {
				sb.append(i);
			}
		}
		return sb.toString();
	}

	private static Stream<EAnnotation> streamAnnotations(EObject root) {
		if (root == null) {
			return Stream.empty();
		} else if (root instanceof EAnnotation) {
			return Stream.of((EAnnotation) root);
		} else {
			return root.eContents().stream().flatMap(E2L::streamAnnotations);
		}
	}

	private static Stream<EClassifier> referencedClassifiers(EAnnotation root) {
		return Stream.concat(root.getReferences().stream().flatMap(Utils.filter(EClassifier.class)),
				root.getReferences().stream().flatMap(E2L::containerClassifiers));
	}

	private static Stream<EClassifier> containerClassifiers(EObject root) {
		if (root == null) {
			return Stream.empty();
		} else if (root instanceof EClassifier) {
			return Stream.of((EClassifier) root);
		} else {
			return containerClassifiers(root.eContainer());
		}
	}
}
