package my.ecore2coq;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingFactory;
import mapping.MappingModel;
import mapping.ObjectMapping;
import my.transform.utils.ReflectiveHookableTransformer;

public abstract class TransformerWithMapping extends ReflectiveHookableTransformer {
	private final MappingModel mapping;

	protected TransformerWithMapping(MappingModel mapping) {
		this.mapping = mapping;
	}

	@Override
	protected void hookMapping(EObject source, EObject target) {
		ObjectMapping m = MappingFactory.eINSTANCE.createObjectMapping();
		m.setSource(source);
		m.getTargets().add(target);
		this.mapping.getObjectMappings().add(m);
	}

	protected final <T extends EObject> T copy(T source) {
		return Cloner.copy(this.mapping, source);
	}
}
