package my.ecore2coq;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;

import flattenedecore.FEClass;
import flattenedecore.FEEnum;
import flattenedecore.FEEnumLiteral;
import flattenedecore.FEImplementation;
import flattenedecore.FEPackage;
import flattenedecore.FlattenedecorePackage;
import javainductive.JConstructor;
import javainductive.JInductive;
import javainductive.JPackage;
import javainductive.JParameter;
import javainductive.JType;
import javainductive.JavainductivePackage;
import mapping.MappingModel;
import my.ecore2coq.utils.Utils;
import my.transform.utils.Rule;

public class F2J extends TransformerWithMapping {
	public F2J(ResourceSet resourceSet, MappingModel mapping) {
		super(mapping);
	}

	private static final Map<EClass, EClass> eClassMap;
	private static final Set<EClass> forbiden;

	static {
		Map<EClass, EClass> map = new HashMap<>();
		Set<EClass> set = new HashSet<>();
		map.put(FlattenedecorePackage.Literals.FE_ANNOTATION, JavainductivePackage.Literals.JANNOTATION);
		map.put(FlattenedecorePackage.Literals.FE_ANY_TYPE, JavainductivePackage.Literals.JG_ANY_TYPE);
		map.put(FlattenedecorePackage.Literals.FE_ATTRIBUTE, JavainductivePackage.Literals.JPARAMETER);
		map.put(FlattenedecorePackage.Literals.FE_CLASS, JavainductivePackage.Literals.JINDUCTIVE);
		set.add(FlattenedecorePackage.Literals.FE_CLASSIFIER);
		map.put(FlattenedecorePackage.Literals.FE_CLASSIFIER_TYPE, JavainductivePackage.Literals.JG_CLASSIFIER_TYPE);
		map.put(FlattenedecorePackage.Literals.FE_DATA_TYPE, JavainductivePackage.Literals.JDATA_TYPE);
		map.put(FlattenedecorePackage.Literals.FE_ENUM, JavainductivePackage.Literals.JINDUCTIVE);
		map.put(FlattenedecorePackage.Literals.FE_ENUM_LITERAL, JavainductivePackage.Literals.JCONSTRUCTOR);
		map.put(FlattenedecorePackage.Literals.FE_IMPLEMENTATION, JavainductivePackage.Literals.JCONSTRUCTOR);
		map.put(FlattenedecorePackage.Literals.FE_LOWER_BOUND_TYPE, JavainductivePackage.Literals.JG_LOWER_BOUND_TYPE);
		set.add(FlattenedecorePackage.Literals.FE_MODEL_ELEMENT);
		set.add(FlattenedecorePackage.Literals.FE_NAMED_ELEMENT);
		set.add(FlattenedecorePackage.Literals.FE_OBJECT);
		map.put(FlattenedecorePackage.Literals.FE_PACKAGE, JavainductivePackage.Literals.JPACKAGE);
		map.put(FlattenedecorePackage.Literals.FE_REFERENCE, JavainductivePackage.Literals.JPARAMETER);
		map.put(FlattenedecorePackage.Literals.FE_STRING_TO_STRING_MAP_ENTRY,
				JavainductivePackage.Literals.JSTRING_TO_STRING_MAP_ENTRY);
		set.add(FlattenedecorePackage.Literals.FE_STRUCTURAL_FEATURE);
		set.add(FlattenedecorePackage.Literals.FE_TYPE);
		map.put(FlattenedecorePackage.Literals.FE_TYPE_ALIAS, JavainductivePackage.Literals.JTYPE_DEF);
		map.put(FlattenedecorePackage.Literals.FE_TYPE_PARAMETER, JavainductivePackage.Literals.JTYPE_PARAMETER);
		set.add(FlattenedecorePackage.Literals.FE_TYPED_ELEMENT);
		map.put(FlattenedecorePackage.Literals.FE_UPPER_BOUND_TYPE, JavainductivePackage.Literals.JG_UPPER_BOUND_TYPE);
		map.put(FlattenedecorePackage.Literals.FE_VARIABLE_TYPE, JavainductivePackage.Literals.JG_VARIABLE_TYPE);
		eClassMap = Collections.unmodifiableMap(map);
		forbiden = Collections.unmodifiableSet(set);
	}

	@Override
	protected EClass remapEClass(EClass sourceClass) {
		return remapEClass(sourceClass, eClassMap, forbiden);
	}

	@Rule
	public void types(FEPackage source, JPackage target) {
		Function<EObject, EObject> transformer = maybeTransform(FlattenedecorePackage.Literals.FE_PACKAGE__ECLASSIFIERS,
				JavainductivePackage.Literals.JPACKAGE__TYPES);
		EList<JType> list = target.getTypes();
		source.getEClassifiers().stream().map(transformer).map(Utils.cast(JType.class)).forEach(list::add);
	}

	@Rule
	public void constructors(FEClass source, JInductive target) {
		Function<EObject, EObject> transformer = maybeTransform(
				FlattenedecorePackage.Literals.FE_CLASS__EIMPLEMENTATIONS,
				JavainductivePackage.Literals.JINDUCTIVE__CONSTRUCTORS);
		EList<JConstructor> list = target.getConstructors();
		source.getEImplementations().stream().map(transformer).map(Utils.cast(JConstructor.class))
				.sorted((l, r) -> l.getName().compareTo(r.getName())).forEachOrdered(list::add);
	}

	@Rule
	public void constructors(FEEnum source, JInductive target) {
		Function<EObject, EObject> transformer = maybeTransform(FlattenedecorePackage.Literals.FE_ENUM__ELITERALS,
				JavainductivePackage.Literals.JINDUCTIVE__CONSTRUCTORS);
		EList<JConstructor> list = target.getConstructors();
		source.getELiterals().stream().map(transformer).map(Utils.cast(JConstructor.class))
				.sorted((l, r) -> l.getName().compareTo(r.getName())).forEachOrdered(list::add);
	}

	@Rule
	public void name(FEImplementation source, JConstructor target) {
		target.setName(((FEClass) source.eContainer()).getName() + "_" + source.getEClass().getName());
	}

	@Rule
	public void name(FEEnumLiteral source, JConstructor target) {
		target.setName(((FEEnum) source.eContainer()).getName() + "_" + source.getName());
	}

	@Rule
	public void parameters(FEImplementation source, JConstructor target) {
		Function<EObject, EObject> transformer = maybeTransform(
				FlattenedecorePackage.Literals.FE_IMPLEMENTATION__ESTRUCTURAL_FEATURES,
				JavainductivePackage.Literals.JCONSTRUCTOR__PARAMETERS);
		EList<JParameter> list = target.getParameters();
		source.getEStructuralFeatures().stream().map(transformer).map(Utils.cast(JParameter.class))
				.sorted((l, r) -> l.getName().compareTo(r.getName())).forEachOrdered(list::add);
	}
}
