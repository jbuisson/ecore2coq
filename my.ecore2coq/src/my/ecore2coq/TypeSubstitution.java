package my.ecore2coq;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import cleanimplecore.CIEClassifierType;
import cleanimplecore.CIELowerBoundType;
import cleanimplecore.CIEType;
import cleanimplecore.CIETypeParameter;
import cleanimplecore.CIEUpperBoundType;
import cleanimplecore.CIEVariableType;
import cleanimplecore.CleanimplecorePackage;
import mapping.MappingModel;
import my.transform.utils.Rule;

public class TypeSubstitution extends TransformerWithMapping {
	private Map<CIETypeParameter, CIEType> map;
	private final MappingModel mapping;

	public TypeSubstitution(Map<CIETypeParameter, CIEType> map, MappingModel mapping) {
		super(mapping);
		this.map = map;
		this.mapping = mapping;
	}

	private CIEType subst(CIEType t, Supplier<CIEType> other) {
		if (t instanceof CIEVariableType) {
			CIEVariableType v = (CIEVariableType) t;
			CIEType r = map.get(v.getETypeParameter());
			if (r != null) {
				return new TypeSubstitution(map, mapping).subst(r);
			}
		}
		CIEType r = other.get();
		mapping.getTemporaryObjects().add(r);
		return r;
	}

	public CIEType subst(CIEType t) {
		return subst(t, () -> (CIEType) transform(t));
	}

	@Rule
	public void eClassifier(CIEClassifierType source, CIEClassifierType target) {
		target.setEClassifier(source.getEClassifier());
	}

	@Rule
	public void eArguments(CIEClassifierType source, CIEClassifierType target) {
		Function<EObject, EObject> transformer = maybeTransform(
				CleanimplecorePackage.Literals.CIE_CLASSIFIER_TYPE__EARGUMENTS,
				CleanimplecorePackage.Literals.CIE_CLASSIFIER_TYPE__EARGUMENTS);
		EList<CIEType> list = target.getEArguments();
		source.getEArguments().stream().map((t) -> subst(t, () -> (CIEType) transformer.apply(t)))
				.forEachOrdered(list::add);
	}

	@Rule
	public void eTypeParameter(CIEVariableType source, CIEVariableType target) {
		target.setETypeParameter(source.getETypeParameter());
	}

	@Rule
	public void eUpperBound(CIEUpperBoundType source, CIEUpperBoundType target) {
		Function<EObject, EObject> transformer = maybeTransform(
				CleanimplecorePackage.Literals.CIE_UPPER_BOUND_TYPE__EUPPER_BOUND,
				CleanimplecorePackage.Literals.CIE_UPPER_BOUND_TYPE__EUPPER_BOUND);
		target.setEUpperBound(
				subst(source.getEUpperBound(), () -> (CIEType) transformer.apply(source.getEUpperBound())));
	}

	@Rule
	public void eLowerBound(CIELowerBoundType source, CIELowerBoundType target) {
		Function<EObject, EObject> transformer = maybeTransform(
				CleanimplecorePackage.Literals.CIE_LOWER_BOUND_TYPE__ELOWER_BOUND,
				CleanimplecorePackage.Literals.CIE_LOWER_BOUND_TYPE__ELOWER_BOUND);
		target.setELowerBound(
				subst(source.getELowerBound(), () -> (CIEType) transformer.apply(source.getELowerBound())));
	}
}
