package my.ecore2coq;

import java.util.Optional;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;

import mapping.MappingModel;

public abstract class GenericRemapper extends TransformerWithMapping {
	private final EPackage source;
	private final String sourcePrefix;
	private final EPackage target;
	private final String targetPrefix;

	public GenericRemapper(EPackage source, String sourcePrefix, EPackage target, String targetPrefix,
			MappingModel mapping) {
		super(mapping);
		this.source = source;
		this.sourcePrefix = sourcePrefix;
		this.target = target;
		this.targetPrefix = targetPrefix;
	}

	private static String rename(String name, String sourcePrefix, String targetPrefix) {
		if (name.startsWith(sourcePrefix)) {
			return targetPrefix + name.substring(sourcePrefix.length());
		} else {
			throw new AssertionError(name);
		}
	}

	protected Optional<EClass> optionallyRemap(EClass sourceClass) {
		try {
			return Optional.of(remapEClass(sourceClass));
		} catch (AssertionError e) {
			return Optional.empty();
		}
	}

	@Override
	protected EClass remapEClass(EClass sourceClass) {
		if (source.equals(sourceClass.getEPackage())) {
			String sourceName = sourceClass.getName();
			String targetName = rename(sourceName, sourcePrefix, targetPrefix);
			EClassifier targetClassifier = target.getEClassifier(targetName);
			if (targetClassifier == null) {
				throw new AssertionError(sourceClass.toString());
			} else if (targetClassifier instanceof EClass) {
				return (EClass) targetClassifier;
			} else {
				throw new AssertionError(sourceClass.toString());
			}
		} else {
			throw new AssertionError(sourceClass.toString());
		}
	}
}
