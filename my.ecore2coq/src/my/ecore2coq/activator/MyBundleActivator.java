package my.ecore2coq.activator;

import java.util.Map;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class MyBundleActivator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		Map<String, Object> extensionToFactoryMap = Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap();
		Stream.of("linked", "generic", "structural", "noref", "impl", "cleanimpl", "flattened", "javainductive",
				"scheduled", "mapping").forEach((s) -> {
					extensionToFactoryMap.put(s, new XMIResourceFactoryImpl());
				});
	}

	@Override
	public void stop(BundleContext context) throws Exception {
	}

}
