package my.ecore2coq;

import java.util.Set;
import java.util.function.Function;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;

import implecore.IEClass;
import implecore.IEClassifier;
import implecore.IEImplementation;
import implecore.IEPackage;
import implecore.IEStructuralFeature;
import implecore.ImplecoreFactory;
import implecore.ImplecorePackage;
import mapping.MappingModel;
import my.ecore2coq.utils.Utils;
import my.transform.utils.Rule;
import norefecore.NREPackage;
import norefecore.NorefecorePackage;

public class NR2I extends GenericRemapper {

	public NR2I(ResourceSet resourceSet, MappingModel mapping) {
		super(NorefecorePackage.eINSTANCE, "NRE", ImplecorePackage.eINSTANCE, "IE", mapping);
	}

	@Rule
	public void eClassifiers(NREPackage source, IEPackage target) {
		Function<EObject, EObject> transformer = maybeTransform(NorefecorePackage.Literals.NRE_PACKAGE__ECLASSIFIERS,
				ImplecorePackage.Literals.IE_PACKAGE__ECLASSIFIERS);
		EList<IEClassifier> targetClassifiers = target.getEClassifiers();
		source.getEClassifiers().stream().map(transformer).map(Utils.cast(IEClassifier.class))
				.forEach(targetClassifiers::add);
		EList<IEImplementation> targetImplementations = target.getEImplementations();
		registerFinishingAction(() -> target.getEClassifiers().stream().flatMap(Utils.filter(IEClass.class))
				.filter((c) -> !c.isAbstract() && !c.isInterface()).forEach((targetClass) -> {
					IEImplementation targetImplementation = ImplecoreFactory.eINSTANCE.createIEImplementation();
					hookMapping(targetClass, targetImplementation);
					targetImplementation.setEClass(targetClass);
					Set<IEClass> classes = Utils.selfAndSuperTypesOf(targetClass);
					EList<IEStructuralFeature> featureList = targetImplementation.getEStructuralFeatures();
					classes.stream().map(IEClass::getEStructuralFeatures).flatMap(EList::stream)
							.sorted((l, r) -> l.getName().compareTo(r.getName())).forEachOrdered(featureList::add);
					targetImplementations.add(targetImplementation);
				}));
	}
}
