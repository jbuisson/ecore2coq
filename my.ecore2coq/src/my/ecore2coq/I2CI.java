package my.ecore2coq;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import cleanimplecore.CIEClass;
import cleanimplecore.CIEClassifier;
import cleanimplecore.CIEClassifierType;
import cleanimplecore.CIEImplementation;
import cleanimplecore.CIEStructuralFeature;
import cleanimplecore.CIEType;
import cleanimplecore.CIETypeParameter;
import cleanimplecore.CIEVariableType;
import cleanimplecore.CleanimplecoreFactory;
import cleanimplecore.CleanimplecorePackage;
import implecore.IEImplementation;
import implecore.ImplecorePackage;
import mapping.MappingModel;
import my.ecore2coq.utils.Utils;
import my.transform.utils.Rule;

public class I2CI extends GenericRemapper {
	private final MappingModel mapping;

	public I2CI(ResourceSet resourceSet, MappingModel mapping) {
		super(ImplecorePackage.eINSTANCE, "IE", CleanimplecorePackage.eINSTANCE, "CIE", mapping);
		this.mapping = mapping;
	}

	@Rule
	public void eTypeParameters(IEImplementation source, CIEImplementation target) {
		Function<EObject, EObject> transformer = maybeTransform(
				ImplecorePackage.Literals.IE_CLASSIFIER__ETYPE_PARAMETERS,
				CleanimplecorePackage.Literals.CIE_IMPLEMENTATION__ETYPE_PARAMETERS);
		EList<CIETypeParameter> params = target.getETypeParameters();
		source.getEClass().getETypeParameters().stream().map(this::copy).map(transformer)
				.map(Utils.cast(CIETypeParameter.class)).forEachOrdered(params::add);
		registerFinishingAction(() -> fixFeatures(target));
	}

	@Rule
	public void eStructuralFeatures(IEImplementation source, CIEImplementation target) {
		Function<EObject, EObject> transformer = maybeTransform(
				ImplecorePackage.Literals.IE_IMPLEMENTATION__ESTRUCTURAL_FEATURES,
				CleanimplecorePackage.Literals.CIE_IMPLEMENTATION__ESTRUCTURAL_FEATURES);
		EList<CIEStructuralFeature> list = target.getEStructuralFeatures();
		source.getEStructuralFeatures().stream().map(this::copy).map(transformer)
				.map(Utils.cast(CIEStructuralFeature.class)).forEachOrdered(list::add);
	}

	private void fixFeatures(CIEImplementation target) {
		Map<CIETypeParameter, CIEType> map = new HashMap<>();
		populateDirectMap(map, target.getEClass().getETypeParameters(), target.getETypeParameters());
		populateSuperTypes(map, target.getEClass());
		target.getEStructuralFeatures().stream().forEach((f) -> {
			try {
				CIEType original = f.getEType();
				CIEType substituted = substitute(map, original);
				f.setEType(substituted);
				if (original.eContainer() == null) {
					mapping.getTemporaryObjects().add(original);
				} else {
					throw new AssertionError("should not be in any contained anymore");
				}
			} catch (Throwable e) {
				throw new AssertionError(target.getEClass().getName() + " / " + f.getName(), e);
			}
		});
	}

	private void populateSuperTypes(Map<CIETypeParameter, CIEType> map, CIEClass eClass) {
		eClass.getESuperTypes().stream().flatMap(Utils.filter(CIEClassifierType.class))
				.forEach((t) -> populate(map, t));
	}

	private void populate(Map<CIETypeParameter, CIEType> map, CIEClassifierType t) {
		CIEClassifier c = t.getEClassifier();
		if (c instanceof CIEClass) {
			CIEClass cl = (CIEClass) c;
			populateSubstituted(map, cl.getName(), cl.getETypeParameters(), t.getEArguments());
			populateSuperTypes(map, cl);
		} else {
			throw new AssertionError(t.toString());
		}
	}

	private void populateSubstituted(Map<CIETypeParameter, CIEType> map, String name, EList<CIETypeParameter> source,
			EList<CIEType> target) {
		if (source.size() == target.size()) {
			for (int i = 0; i < source.size(); ++i) {
				map.put(source.get(i), substitute(map, target.get(i)));
			}
		} else {
			throw new AssertionError(name + ": " + source.size() + " != " + target.size());
		}
	}

	private void populateDirectMap(Map<CIETypeParameter, CIEType> map, EList<CIETypeParameter> source,
			EList<CIETypeParameter> target) {
		if (source.size() == target.size()) {
			for (int i = 0; i < source.size(); ++i) {
				map.put(source.get(i), makeType(target.get(i)));
			}
		} else {
			throw new AssertionError();
		}
	}

	private CIEType makeType(CIETypeParameter p) {
		CIEVariableType t = CleanimplecoreFactory.eINSTANCE.createCIEVariableType();
		mapping.getTemporaryObjects().add(t);
		t.setETypeParameter(p);
		return t;
	}

	private CIEType substitute(Map<CIETypeParameter, CIEType> map, CIEType t) {
		return (CIEType) new TypeSubstitution(map, mapping).subst(t);
	}
}
