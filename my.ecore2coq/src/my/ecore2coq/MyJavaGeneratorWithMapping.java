package my.ecore2coq;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingFactory;
import mapping.MappingModel;
import mapping.ObjectMapping;
import my.java.MyJavaGenerator;

public class MyJavaGeneratorWithMapping extends MyJavaGenerator {
	private final MappingModel mapping;

	protected MyJavaGeneratorWithMapping(MappingModel mapping) {
		this.mapping = mapping;
	}

	protected void hookMapping(EObject source, EObject target) {
		ObjectMapping m = MappingFactory.eINSTANCE.createObjectMapping();
		m.setSource(source);
		m.getTargets().add(target);
		this.mapping.getObjectMappings().add(m);
	}
}
