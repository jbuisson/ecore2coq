package my.ecore2coq.ui.popup.actions;

import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import my.ecore2coq.ui.UiActivator;
import my.ecore2coq.workflow.MyAbstractWorkflow;

public abstract class MyAbstractTransformerHandler extends AbstractHandler {

	protected MyAbstractTransformerHandler() {
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Job job = Job.create(this.getClass().getSimpleName(), (ICoreRunnable) (progressMonitor) -> doExecute(event, progressMonitor));
		job.schedule();
		return null;
	}

	private void doExecute(ExecutionEvent event, IProgressMonitor progressMonitor) {
		SubMonitor sub = SubMonitor.convert(progressMonitor, 1);
		try {
			ISelection selection = HandlerUtil.getActiveMenuSelection(event);
			if (!selection.isEmpty() && selection instanceof IStructuredSelection) {
				IStructuredSelection structuredSelection = (IStructuredSelection) selection;
				Object[] resources = structuredSelection.toArray();
				doExecute(sub, resources);
			}
		} catch (IOException | CoreException e) {
			UiActivator.error(e);
		} catch (OperationCanceledException e) {
		} finally {
			sub.done();
		}
	}

	private void doExecute(SubMonitor monitor, Object[] resources) throws CoreException, IOException {
		createWorkflow().execute(monitor, resources);
	}

	protected abstract MyAbstractWorkflow createWorkflow();
}