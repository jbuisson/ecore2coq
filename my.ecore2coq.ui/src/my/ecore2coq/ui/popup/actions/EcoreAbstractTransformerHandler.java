package my.ecore2coq.ui.popup.actions;

import my.ecore2coq.workflow.Ecore2CoqWorkflow;
import my.ecore2coq.workflow.MyAbstractWorkflow;

public abstract class EcoreAbstractTransformerHandler extends MyAbstractTransformerHandler {
	@Override
	protected MyAbstractWorkflow createWorkflow() {
		return new Ecore2CoqWorkflow();
	}
}