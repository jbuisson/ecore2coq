package my.ecore2coq.ui.popup.actions;

import my.ecore2coq.workflow.Genmodel2CoqWorkflow;
import my.ecore2coq.workflow.MyAbstractWorkflow;

public class GenmodelTransformerHandler extends MyAbstractTransformerHandler {
	@Override
	protected MyAbstractWorkflow createWorkflow() {
		return new Genmodel2CoqWorkflow();
	}
}