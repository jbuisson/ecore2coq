package my.ecore2coq.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class UiActivator extends AbstractUIPlugin {
	private static UiActivator instance = null;

	public static UiActivator getInstance() {
		return instance;
	}

	public UiActivator() {
		instance = this;
	}

	public static void error(Throwable t) {
		String pluginID = getInstance().getBundle().getSymbolicName();
		getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, t.getMessage(), t));
	}
}
