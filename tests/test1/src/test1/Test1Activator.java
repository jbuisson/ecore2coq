package test1;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;

public class Test1Activator extends Plugin {
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		DoTest.doTest();
	}
}
