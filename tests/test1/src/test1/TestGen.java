package test1;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import test1.gen.test1_2e_genmodel;

public class TestGen extends test1_2e_genmodel<TestGen.Term, TestGen.Definition> {
	public static abstract class Term {
	}

	public static class Definition {
		public final EObject object;
		public final EClass type;
		public final String name;
		public final Term term;

		public Definition(EObject object, EClass type, String name, Term term) {
			this.object = object;
			this.type = type;
			this.name = name;
			this.term = term;
		}
	}

	public static class Atomic extends Term {
		public final String term;

		public Atomic(String t) {
			this.term = t;
		}

		public String toString() {
			return term;
		}
	}

	public static class Apply extends Term {
		public final Term left;
		public final Term right;

		public Apply(Term l, Term r) {
			this.left = l;
			this.right = r;
		}

		public String toString() {
			return "(" + left.toString() + " " + right.toString() + ")";
		}
	}

	private int counter = 0;
	private final List<String> libraries = new LinkedList<>();
	private final List<Definition> definitions = new LinkedList<>();
	private final Map<EObject, Map<EClass, Definition>> index = new HashMap<>();
	private final Map<EClass, List<Definition>> uris = new HashMap<>();

	public TestGen() {
		require("ZArith");
	}

	private synchronized int next() {
		return counter++;
	}

	@Override
	protected void require(String lib) {
		libraries.add(lib);
	}

	@Override
	protected Term constructor(String name) {
		return new Atomic(name);
	}

	@Override
	protected Term apply(Term l, Term r) {
		return new Apply(l, r);
	}

	@Override
	protected Term reference(Definition def) {
		return new Atomic(def.name);
	}

	@Override
	protected synchronized Definition fresh(EObject obj, EClass stype, Term val) {
		Definition r = new Definition(obj, stype, "_" + next(), val);
		definitions.add(r);
		index.computeIfAbsent(obj, (x) -> new HashMap<>()).put(stype, r);
		return r;
	}

	@Override
	protected synchronized Term uri(Definition def) {
		List<Definition> l = uris.computeIfAbsent(def.type, (x) -> new LinkedList<>());
		int v = l.indexOf(def);
		if (v < 0) {
			v = l.size();
			l.add(def);
		}
		return new Atomic(Integer.toString(v));
	}

	@Override
	protected synchronized Definition get(EObject obj, EClass staticClass) {
		Map<EClass, Definition> m = index.getOrDefault(obj, null);
		if (m == null) {
			return null;
		} else {
			return m.getOrDefault(staticClass, null);
		}
	}

	@Override
	protected Term wrap(boolean value) {
		return new Atomic(value ? "true" : "false");
	}

	@Override
	protected Term wrap(byte value) {
		return new Atomic("(" + Byte.toString(value) + ")%Z");
	}

	@Override
	protected Term wrap(short value) {
		return new Atomic("(" + Short.toString(value) + ")%Z");
	}

	@Override
	protected Term wrap(int value) {
		return new Atomic("(" + Integer.toString(value) + ")%Z");
	}

	@Override
	protected Term wrap(long value) {
		return new Atomic("(" + Long.toString(value) + ")%Z");
	}

	@Override
	protected Term wrap(BigInteger value) {
		return new Atomic("(" + value.toString() + ")%Z");
	}

	@Override
	protected Term wrap(char value) {
		throw new AssertionError();
	}

	@Override
	protected Term wrap(String value) {
		return new Atomic("\"" + value + "\"");
	}

	@Override
	protected Term wrapList(Stream<Term> value) {
		List<Term> v = value.collect(Collectors.toUnmodifiableList());
		Term acc = constructor("nil");
		ListIterator<Term> i = v.listIterator();
		if (i.hasNext()) {
			while (i.hasNext()) {
				i.next();
			}
			while (i.hasPrevious()) {
				acc = apply(apply(constructor("cons"), i.previous()), acc);
			}
		}
		return acc;
	}

	@Override
	protected Term wrapNoDupList(Stream<Term> value) {
		return wrapList(value);
	}

	@Override
	protected Term wrapSet(Stream<Term> value) {
		return wrapList(value);
	}

	@Override
	protected Term wrapBag(Stream<Term> value) {
		return wrapList(value);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Term wrapMapEntry(Entry value) {
		throw new AssertionError();
	}

	@Override
	protected Term none() {
		return constructor("None");
	}

	@Override
	protected Term some(Term value) {
		return apply(constructor("Some"), value);
	}

	public void dump(PrintWriter w) throws IOException {
		for (String l : libraries) {
			w.print("Require Import ");
			w.print(l);
			w.println('.');
		}
		w.println();
		for (Definition d : definitions) {
			w.print("Definition ");
			w.print(d.name);
			w.print(" := ");
			w.print(d.term.toString());
			w.println(".");
		}
		for (Map.Entry<EClass, List<Definition>> e : uris.entrySet()) {
			w.println();
			w.print("(* URI Table for ");
			w.println(e.getKey().getName());
			List<Definition> l = e.getValue();
			int sp = (int) Math.ceil(Math.log10(l.size()));
			for (int i = 0; i < l.size(); ++i) {
				w.print(" - ");
				String x = Integer.toString(i);
				for (int j = x.length(); j < sp; ++j) {
					w.print(' ');
				}
				w.print(x);
				w.print(" => ");
				w.println(l.get(i).name);
			}
			w.println("*)");
		}
	}
}
