package test1;

import java.io.IOException;
import java.io.PrintWriter;

public class DoTest {
	public static void doTest() {
		TestGen t = new TestGen();
		t.prologue();
		C3 c3_0 = Test1Factory.eINSTANCE.createC3();
		c3_0.getToto().add(0);
		c3_0.getToto().add(1);
		c3_0.getToto().add(2);
		t.test1_C3_0(c3_0);
		C3 c3_1 = Test1Factory.eINSTANCE.createC3();
		t.test1_C3_0(c3_1);
		C2 c2_0 = Test1Factory.eINSTANCE.createC2();
		c2_0.setNewAttribute(42);
		t.test1_C2_0(c2_0);
		C2 c2_1 = Test1Factory.eINSTANCE.createC2();
		c2_1.setNewAttribute(37);
		t.test1_C2_0(c2_1);
		B1 b1_0 = Test1Factory.eINSTANCE.createB1();
		t.test1_C1_0(b1_0);
		B1 b1_1 = Test1Factory.eINSTANCE.createB1();
		b1_1.setNewAttribute(12);
		t.test1_C1_0(b1_1);
		B1 b1_2 = Test1Factory.eINSTANCE.createB1();
		b1_2.setNewAttribute(17);
		b1_2.setC2(c2_0);
		t.test1_C1_0(b1_2);
		PrintWriter w = new PrintWriter(System.out);
		try {
			t.dump(w);
			w.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
