package my.ecore2coq.workflow;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;

import gallina.GallinaPackage;
import mapping.MappingFactory;
import mapping.MappingFragment;
import mapping.MappingModel;
import mapping.MappingRoot;
import mapping.ObjectMapping;
import my.ecore2coq.workflow.activator.WorkflowActivator;
import my.ecore2coq.workflow.internal.Pair;
import my.ecore2coq.workflow.internal.StuckException;
import my.ecore2coq.workflow.tasks.TransformationTask;
import my.transform.utils.MyResourceSetImpl;
import my.utils.MapSet;
import my.utils.OrderPreservingMap;
import timing.Profile;
import timing.Task;
import timing.TimingFactory;

public abstract class MyAbstractWorkflow {
	private static final String BENCHMARK_ = "--benchmark=";
	private static final String MAP_ = "--map=";

	private static final long THRESHOLD_MAP;
	private static final int WARMUP;

	static {
		long map = 80000;
		int warmup = 0;
		String[] args = Platform.getCommandLineArgs();
		for (int i = 0; i < args.length; ++i) {
			if (args[i].startsWith(MAP_)) {
				map = Long.valueOf(args[i].substring(MAP_.length()));
			} else if (args[i].startsWith(BENCHMARK_)) {
				warmup = Integer.valueOf(args[i].substring(BENCHMARK_.length()));
			}
		}
		info("parameters: thresholds for mappings: " + map);
		THRESHOLD_MAP = map;
		info("parameters: warmup: " + warmup);
		WARMUP = warmup;
	}

	private static Map<?, ?> saveOptions() {
		Map<String, Object> r = new HashMap<>();
		r.put(XMLResource.OPTION_URI_HANDLER, new URIHandlerImpl.PlatformSchemeAware());
		return r;
	}

	public void execute(SubMonitor monitor, List<?> resources) throws CoreException, IOException {
		execute(monitor, resources.toArray());
	}

	public void execute(SubMonitor monitor, Stream<?> resources) throws CoreException, IOException {
		execute(monitor, resources.toArray());
	}

	public void execute(SubMonitor monitor, Object... resources) throws CoreException, IOException {
		Profile profile = TimingFactory.eINSTANCE.createProfile();
		ResourceSet resourceSet = new MyResourceSetImpl();
		Set<IContainer> containers = new HashSet<>();
		SubMonitor first = monitor.split(1).setWorkRemaining(resources.length + 1);
		IWorkspace base = null;
		for (Object i : resources) {
			if (first.isCanceled()) {
				throw new OperationCanceledException();
			}
			SubMonitor j = first.split(1);
			if (i instanceof IFile) {
				IFile file = (IFile) i;
				base = file.getWorkspace();
				containers.add(file.getParent());
				SubMonitor k = SubMonitor.convert(j, 1 + numberOfWarmups());
				for (int x = 0; x < numberOfWarmups(); ++x) {
					SubMonitor l = k.split(1);
					l.setTaskName(file.getName() + " (warmup " + (x + 1) + ")");
					executeFor(file, l, Optional.empty());
					System.gc();
				}
				SubMonitor l = k.split(1);
				l.setTaskName(file.getName());
				executeFor(file, l, Optional.of(profile));
			}
			j.done();
		}
		SubMonitor second = first.split(1).setWorkRemaining(containers.size());
		for (IContainer i : containers) {
			if (first.isCanceled()) {
				throw new OperationCanceledException();
			}
			SubMonitor j = second.split(1);
			j.setTaskName(i.getName());
			i.refreshLocal(IResource.DEPTH_ONE, j);
		}
		second.done();
		first.done();
		if (base != null) {
			IProject project = base.getRoot().getProject("timing_data");
			if (!project.exists()) {
				project.create(monitor);
			}
			if (!project.isOpen()) {
				project.open(monitor);
			}
			IFile f = project.getFile("profile.timing");
			Resource report = resourceSet.createResource(URI.createURI(f.getLocationURI().toString()));
			report.getContents().add(profile);
			report.save(saveOptions());
			new TimingToCSV().generateCsv(resourceSet, f);
		}
	}

	protected abstract Stream<TransformationTask> getTasks();

	private void executeFor(IFile file, SubMonitor monitor, Optional<Profile> profile) {
		String location = file.getLocationURI().toString();
		WorkflowActivator.info("processing: " + location);
		URI rootURI = URI.createURI(location);
		executeFor(file, new ResourceBasedContainer(rootURI), rootURI.trimFileExtension().lastSegment(), monitor,
				profile);
	}

	private void executeFor(IFile file, Container container, String name, SubMonitor monitor,
			Optional<Profile> profile) {
		Collection<TransformationTask> pendingTasks = getTasks().collect(Collectors.toCollection(LinkedList::new));
		MappingModel mapping = MappingFactory.eINSTANCE.createMappingModel();
		monitor.setWorkRemaining(pendingTasks.size() + 1);
		try {
			while (!pendingTasks.isEmpty()) {
				if (monitor.isCanceled()) {
					throw new OperationCanceledException();
				}
				Optional<Pair<TransformationTask, Optional<String>>> selected = pendingTasks.stream()
						.map((t) -> new Pair<>(t, t.isEligible(container.keys()))).filter((p) -> p.second.isPresent())
						.findAny();
				Pair<TransformationTask, Optional<String>> p = selected.orElseThrow(() -> new StuckException());
				TransformationTask selectedTask = p.first;
				String source = p.second.orElseThrow(() -> new AssertionError());
				monitor.setTaskName(name + ": " + selectedTask.getName());
				pendingTasks.remove(selectedTask);
				SubMonitor sub = monitor.split(1);
				System.gc();
				Map<String, List<EObject>> produced = selectedTask.executeFor(source, container.get(source).get(), sub,
						profile, mapping, container, file);
				produced.entrySet().forEach((i) -> {
					WorkflowActivator.info("saving artifact " + i.getKey());
					container.put(i.getKey(), i.getValue());
				});
				sub.done();
			}
		} catch (StuckException e) {
			String pending = collectionToString(pendingTasks);
			String sources = collectionToString(container.keys());
			error("in " + this.getClass().getName() + ": no candidate in tasks " + pending + " given the sources "
					+ sources, e);
		}
		SubMonitor last = monitor.split(1);
		System.gc();
		saveMapping(container, name, profile, mapping, monitor);
		last.done();
		monitor.done();
	}

	private void smallTask(Container container, String n, Optional<Profile> profile, String name,
			Supplier<String> uriSupplier, Supplier<EObject> contentSupplier, SubMonitor monitor) {
		monitor.setTaskName(n + ": " + name);
		long before = System.nanoTime();
		String uri = uriSupplier.get();
		List<EObject> contents = Stream.of(contentSupplier.get()).collect(Collectors.toList());
		container.put(uri, contents);
		long after = System.nanoTime();
		profile.ifPresent((p) -> {
			long timespan = after - before;
			Task t = TimingFactory.eINSTANCE.createTask();
			t.setFileName(n);
			t.setTaskName(name);
			t.setTime(BigInteger.valueOf(timespan));
			p.getTasks().add(t);
		});
	}

	private void saveMapping(Container container, String name, Optional<Profile> profile, MappingModel mapping,
			SubMonitor monitor) {
		smallTask(container, name, profile, "SaveObjects", this::createObjectsURI, () -> {
			MappingModel objects = MappingFactory.eINSTANCE.createMappingModel();
			objects.getTemporaryObjects().addAll(mapping.getTemporaryObjects());
			mapping.getTemporaryObjects().clear();
			WorkflowActivator.info(name + " saving " + objects.getTemporaryObjects().size() + " temporary objects");
			return objects;
		}, monitor);

		smallTask(container, name, profile, "SaveMapping", this::createMappingURI, () -> {
			Map<EObject, Set<EObject>> map = simplify(mapping);
			Set<EPackage> pkgs = Stream.of(EcorePackage.eINSTANCE, GallinaPackage.eINSTANCE)
					.collect(Collectors.toSet());
			MappingModel s = simplify(map, pkgs);
			WorkflowActivator.info(name + " saving " + s.getObjectMappings().size() + " mappings");
			return s;
		}, monitor);

		smallTask(container, name, profile, "SaveFullMapping", this::createFullMappingURI, () -> {
			Map<EObject, Set<EObject>> simpl = simplify(mapping);
			MappingRoot split = split(name, simpl);
			if (split instanceof MappingFragment) {
				saveFragments(container, name, (MappingFragment) split, "");
			}
			return split;
		}, monitor);
	}

	private String concat(String l, String r) {
		if (l.isEmpty()) {
			return r;
		} else if (r.isEmpty()) {
			return l;
		} else {
			return l + "." + r;
		}
	}

	private void saveFragments(Container container, String name, MappingFragment simplified, String prefix) {
		int counter = 0;
		for (MappingRoot i : simplified.getSubMappings()) {
			String key = concat(prefix, String.format("fragment.%04d", counter));
			if (i instanceof MappingFragment) {
				saveFragments(container, name, (MappingFragment) i, key);
			} else {
				String x = concat(key, "mapping");
				WorkflowActivator.info("saving " + x);
				container.put(x, Stream.of(i).collect(Collectors.toList()));
			}
			counter = counter + 1;
		}
	}

	private Map<EObject, Set<EObject>> simplify(MappingModel original) {
		Map<EObject, Set<EObject>> map = new OrderPreservingMap<>();
		for (ObjectMapping m : original.getObjectMappings()) {
			map.computeIfAbsent(m.getSource(), (x) -> new MapSet<>(OrderPreservingMap::new)).addAll(m.getTargets());
		}
		return map;
	}

	private MappingModel simplify(Map<EObject, Set<EObject>> original, Set<EPackage> retained) {
		MappingModel r = MappingFactory.eINSTANCE.createMappingModel();
		List<ObjectMapping> l = r.getObjectMappings();
		original.keySet().stream().filter((x) -> retained.contains(x.eClass().getEPackage()))
				.map((x) -> getObjectMapping(x, original, retained)).forEach((x) -> x.ifPresent(l::add));
		return r;
	}

	private Optional<ObjectMapping> getObjectMapping(EObject source, Map<EObject, Set<EObject>> deps,
			Set<EPackage> retained) {
		ObjectMapping r = MappingFactory.eINSTANCE.createObjectMapping();
		r.setSource(source);
		streamDeps(source, deps).filter((x) -> retained.contains(x.eClass().getEPackage()))
				.forEach(r.getTargets()::add);
		return Optional.of(r);
	}

	private Stream<EObject> streamDeps(EObject source, Map<EObject, Set<EObject>> deps) {
		return deps.getOrDefault(source, new MapSet<>(OrderPreservingMap::new)).stream()
				.flatMap((x) -> Stream.concat(Stream.of(x), streamDeps(x, deps)));
	}

	private MappingRoot split(String name, Map<EObject, Set<EObject>> m) {
		Map<EObject, Set<EObject>> map = new OrderPreservingMap<>();
		map.putAll(m);
		long mapCount = map.values().stream().flatMap(Set::stream).count() + map.size();
		WorkflowActivator.info(name + ": " + mapCount + " mappings");
		if (mapCount < THRESHOLD_MAP) {
			MappingModel simplified = MappingFactory.eINSTANCE.createMappingModel();
			for (Map.Entry<EObject, Set<EObject>> i : map.entrySet()) {
				ObjectMapping x = MappingFactory.eINSTANCE.createObjectMapping();
				x.setSource(i.getKey());
				x.getTargets().addAll(i.getValue());
				simplified.getObjectMappings().add(x);
			}
			return simplified;
		} else {
			MappingFragment simplified = MappingFactory.eINSTANCE.createMappingFragment();
			long gathered = 0;
			long mapCurrent = 0;
			MappingModel current = MappingFactory.eINSTANCE.createMappingModel();
			for (Map.Entry<EObject, Set<EObject>> e : map.entrySet()) {
				ObjectMapping x = MappingFactory.eINSTANCE.createObjectMapping();
				current.getObjectMappings().add(x);
				x.setSource(e.getKey());
				x.getTargets().addAll(e.getValue());
				mapCurrent += x.getTargets().size() + 1;
				if (mapCurrent >= THRESHOLD_MAP) {
					gathered += mapCurrent;
					simplified.getSubMappings().add(current);
					WorkflowActivator.info(name + ": " + gathered + " mappings gathered over " + mapCount);
					mapCurrent = 0;
					current = MappingFactory.eINSTANCE.createMappingModel();
				}
			}
			if (mapCurrent > 0) {
				gathered += mapCurrent;
				simplified.getSubMappings().add(current);
				WorkflowActivator.info(name + ": " + gathered + " mappings gathered over " + mapCount);
			}
			WorkflowActivator.info("sub mapping created: " + simplified.getSubMappings().size());
			return simplified;
		}
	}

	public String createObjectsURI() {
		return "objects.mapping";
	}

	public String createMappingURI() {
		return "mapping";
	}

	public String createFullMappingURI() {
		return "full.mapping";
	}

	private static String collectionToString(Collection<?> collection) {
		return collection.stream().map(Object::toString).collect(Collectors.joining(", ", "[", "]"));
	}

	private static void error(String message, Throwable t) {
		WorkflowActivator.error(message, t);
	}

	private static void info(String message) {
		WorkflowActivator.info(message);
	}

	private int numberOfWarmups() {
		return WARMUP;
	}
}