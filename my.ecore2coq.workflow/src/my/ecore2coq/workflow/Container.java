package my.ecore2coq.workflow;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

public interface Container {
	Set<String> keys();
	
	Optional<List<EObject>> get(String key);
	
	void put(String key, List<EObject> contents);
}
