package my.ecore2coq.workflow;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;

import my.ecore2coq.workflow.internal.Pair;
import my.transform.utils.MyResourceSetImpl;

public class ResourceBasedContainer implements Container {
	private final URI baseURI;
	private final ResourceSet resourceSet;
	private final Map<String, Pair<URI, Resource>> contents;

	public ResourceBasedContainer(URI sourceURI) {
		this.baseURI = sourceURI.trimFileExtension();
		this.resourceSet = new MyResourceSetImpl();
		this.contents = new TreeMap<>();
		this.contents.put(sourceURI.fileExtension(), new Pair<>(sourceURI, load(this.resourceSet, sourceURI)));
	}

	private static Resource load(ResourceSet resourceSet, URI uri) {
		Resource r = resourceSet.getResource(uri, true);
		EcoreUtil.resolveAll(resourceSet);
		return r;
	}

	@Override
	public Set<String> keys() {
		return this.contents.keySet();
	}

	@Override
	public Optional<List<EObject>> get(String key) {
		Pair<URI, Resource> p = this.contents.get(key);
		if (p == null) {
			return Optional.empty();
		} else {
			return Optional.of(p.second.getContents());
		}
	}

	@Override
	public void put(String key, List<EObject> contents) {
		URI uri = baseURI.appendFileExtension(key);
		Resource r = getOrCreate(uri);
		r.getContents().clear();
		r.getContents().addAll(contents);
		try {
			r.save(saveOptions());
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.contents.put(key, new Pair<>(uri, r));
	}

	private static Map<?, ?> saveOptions() {
		Map<String, Object> r = new HashMap<>();
		r.put(XMLResource.OPTION_URI_HANDLER, new URIHandlerImpl.PlatformSchemeAware());
		return r;
	}

	private Resource getOrCreate(URI uri) {
		if (this.resourceSet.getURIConverter().exists(uri, null)) {
			return this.resourceSet.getResource(uri, true);
		} else {
			return this.resourceSet.createResource(uri);
		}
	}
}
