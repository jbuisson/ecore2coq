package my.ecore2coq.workflow.tasks;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.L2G;

public class LToGTask extends ObjectByObjectExtensionBasedTransformationTask {
	public LToGTask() {
	}

	@Override
	protected EObject transform(MappingModel mapping, EObject root) {
		L2G transformer = new L2G(mapping);
		return transformer.transform(root);
	}

	@Override
	protected String getSourceExtension() {
		return "linked";
	}

	@Override
	protected String getTargetExtension() {
		return "generic";
	}
}
