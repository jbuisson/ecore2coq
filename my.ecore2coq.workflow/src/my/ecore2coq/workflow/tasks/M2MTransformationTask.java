package my.ecore2coq.workflow.tasks;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.workflow.Container;
import my.ecore2coq.workflow.activator.WorkflowActivator;
import timing.Profile;
import timing.Task;
import timing.TimingFactory;

public abstract class M2MTransformationTask implements TransformationTask {
	public M2MTransformationTask() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.ecore2coq.ui.tasks.TransformationTask#executeFor(org.eclipse.emf.ecore
	 * .resource.ResourceSet, org.eclipse.emf.common.util.URI)
	 */
	@Override
	public Map<String, List<EObject>> executeFor(String from, List<EObject> source, IProgressMonitor monitor,
			Optional<Profile> profile, MappingModel mapping, Container container, IFile file) {
		long before = System.nanoTime();
		List<? extends EObject> res = transform(file, container, mapping, source);
		long after = System.nanoTime();
		if (res != null) {
			Map<String, List<EObject>> r = new HashMap<>();
			String target = map(from);
			r.put(target, new LinkedList<>(res));
			profile.ifPresent((p) -> {
				long timespan = after - before;
				Task t = TimingFactory.eINSTANCE.createTask();
				t.setFileName(from);
				t.setTaskName(this.getName());
				t.setTime(BigInteger.valueOf(timespan));
				p.getTasks().add(t);
			});
			return r;
		} else {
			error("unable to transform");
			return new HashMap<>();
		}
	}

	protected abstract List<? extends EObject> transform(IFile file, Container container, MappingModel mapping,
			List<EObject> source);

	protected abstract String map(String source);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.ecore2coq.ui.tasks.TransformationTask#isEligible(java.util.Collection)
	 */
	@Override
	public abstract Optional<String> isEligible(Collection<String> uri);

	private void error(String message) {
		WorkflowActivator.error(message);
	}
}
