package my.ecore2coq.workflow.tasks;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.CI2F;

public class CIToFTask extends ObjectByObjectExtensionBasedTransformationTask {
	public CIToFTask() {
		super();
	}

	@Override
	protected EObject transform(MappingModel mapping, EObject root) {
		CI2F transformer = new CI2F(root.eResource().getResourceSet(), mapping);
		return transformer.transform(root);
	}

	@Override
	protected String getSourceExtension() {
		return "cleanimpl";
	}

	@Override
	protected String getTargetExtension() {
		return "flattened";
	}
}
