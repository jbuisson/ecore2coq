package my.ecore2coq.workflow.tasks;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.J2SI;

public class JIToSITask extends ObjectByObjectExtensionBasedTransformationTask {
	public JIToSITask() {
	}

	@Override
	protected EObject transform(MappingModel mapping, EObject root) {
		J2SI transformer = new J2SI(root.eResource().getResourceSet(), mapping);
		return transformer.transform(root);
	}

	@Override
	protected String getSourceExtension() {
		return "javainductive";
	}

	@Override
	protected String getTargetExtension() {
		return "scheduled";
	}
}
