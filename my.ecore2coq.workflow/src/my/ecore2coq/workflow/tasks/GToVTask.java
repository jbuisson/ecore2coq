package my.ecore2coq.workflow.tasks;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.acceleo.common.preference.AcceleoPreferences;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.workflow.Container;
import my.ecore2coq.workflow.activator.WorkflowActivator;
import my.gallina.Coqgen;
import timing.Profile;
import timing.Task;
import timing.TimingFactory;

public class GToVTask implements TransformationTask {
	public GToVTask() {
	}

	protected String getSourceExtension() {
		return "gallina";
	}

	protected String getTargetExtension() {
		return "v";
	}

	private static void processOneFile(EObject model, IProgressMonitor monitor) throws IOException {
		new Coqgen(model, getFolder(model), new LinkedList<>()).doGenerate(BasicMonitor.toMonitor(monitor));
	}

	private static File getFolder(EObject s) {
		File sourceFile = new File(s.eResource().getURI().toFileString());
		return sourceFile.getParentFile();
	}

	@Override
	public Map<String, List<EObject>> executeFor(String from, List<EObject> source, IProgressMonitor monitor,
			Optional<Profile> profile, MappingModel mapping, Container container, IFile file) {
		try {
			boolean notifications = AcceleoPreferences.areSuccessNotificationsEnabled();
			AcceleoPreferences.switchSuccessNotifications(false);
			long before = System.nanoTime();
			for (EObject i : source) {
				processOneFile(i, monitor);
			}
			long after = System.nanoTime();
			AcceleoPreferences.switchSuccessNotifications(notifications);
			profile.ifPresent((p) -> {
				long timespan = after - before;
				Task t = TimingFactory.eINSTANCE.createTask();
				t.setFileName(from);
				t.setTaskName(this.getName());
				t.setTime(BigInteger.valueOf(timespan));
				p.getTasks().add(t);
			});
		} catch (IOException e) {
			WorkflowActivator.error(e);
		}
		return new HashMap<>();
	}

	@Override
	public Optional<String> isEligible(Collection<String> sources) {
		String s = getSourceExtension();
		return sources.stream().filter(s::contentEquals).findAny();
	}

	protected String map(String sourceURI) {
		return getTargetExtension();
	}
}
