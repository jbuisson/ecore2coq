package my.ecore2coq.workflow.tasks;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.S2NR;

public class SToNRTask extends ObjectByObjectExtensionBasedTransformationTask {
	public SToNRTask() {
	}

	@Override
	protected EObject transform(MappingModel mapping, EObject root) {
		S2NR transformer = new S2NR(root.eResource().getResourceSet(), mapping);
		return transformer.transform(root);
	}

	@Override
	protected String getSourceExtension() {
		return "structural";
	}

	@Override
	protected String getTargetExtension() {
		return "noref";
	}
}
