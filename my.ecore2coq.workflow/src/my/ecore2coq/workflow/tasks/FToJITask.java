package my.ecore2coq.workflow.tasks;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.F2J;

public class FToJITask extends ObjectByObjectExtensionBasedTransformationTask {
	public FToJITask() {
	}

	@Override
	protected EObject transform(MappingModel mapping, EObject root) {
		F2J transformer = new F2J(root.eResource().getResourceSet(), mapping);
		return transformer.transform(root);
	}

	@Override
	protected String getSourceExtension() {
		return "flattened";
	}

	@Override
	protected String getTargetExtension() {
		return "javainductive";
	}
}
