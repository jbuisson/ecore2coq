package my.ecore2coq.workflow.tasks;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import mapping.MappingModel;
import my.ecore2coq.workflow.Container;
import timing.Profile;

public interface TransformationTask {

	Map<String, List<EObject>> executeFor(String from, List<EObject> source, IProgressMonitor monitor,
			Optional<Profile> profile, MappingModel mapping, Container container, IFile file);

	Optional<String> isEligible(Collection<String> uri);

	default String getName() {
		return this.getClass().getSimpleName();
	}

}