package my.ecore2coq.workflow.tasks;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import pseudo.PseudoRoot;

public class EcoreToPseudoTask extends ToPseudoTask {
	public EcoreToPseudoTask() {
	}

	@Override
	protected String getSourceExtension() {
		return "ecore";
	}

	@Override
	protected void populate(PseudoRoot r, List<EObject> source) {
		source.stream().map((x) -> (EPackage) x).forEach(r.getRoots()::add);
	}
}
