package my.ecore2coq.workflow.tasks;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import mapping.MappingFactory;
import mapping.MappingModel;
import mapping.ObjectMapping;
import my.ecore2coq.workflow.Container;
import my.ecore2coq.workflow.activator.WorkflowActivator;
import pseudo.PseudoFactory;
import pseudo.PseudoRoot;

public abstract class ToPseudoTask extends ExtensionBasedTransformationTask {
	private static final String SCAN_ = "--scan=";
	private static final String IMPORT_ = "--import=";
	private static final List<URI> BASES;

	static {
		BASES = Stream.concat(streamCmdLineArgs(SCAN_), streamCmdLineArgs(IMPORT_)).sequential().map(URI::createFileURI)
				.collect(Collectors.toList());
	}

	private static Stream<String> streamCmdLineArgs(String i) {
		return Stream.of(Platform.getCommandLineArgs()).filter((x) -> x.startsWith(i))
				.map((x) -> x.substring(i.length()));
	}

	public ToPseudoTask() {
	}

	private EObject transform(MappingModel mapping, EObject root) {
		return root;
	}

	@Override
	protected String getTargetExtension() {
		return "pseudo";
	}

	@Override
	protected List<? extends EObject> transform(IFile file, Container container, MappingModel mapping,
			List<EObject> source) {
		PseudoRoot r = PseudoFactory.eINSTANCE.createPseudoRoot();
		String name = source.stream().map(EObject::eResource).map(Resource::getURI).map(ToPseudoTask::shortest)
				.filter((p) -> !p.isEmpty()).findAny().orElse("unnamed");
		WorkflowActivator.info("name -> " + name);
		r.setName(name);
		populate(r, source);
		source.stream().map((x) -> createMapping(r, x)).forEach(mapping.getObjectMappings()::add);
		List<EObject> l = new LinkedList<>();
		l.add(transform(mapping, r));
		return l;
	}

	protected abstract void populate(PseudoRoot r, List<EObject> source);

	private static ObjectMapping createMapping(PseudoRoot r, EObject o) {
		ObjectMapping m = MappingFactory.eINSTANCE.createObjectMapping();
		m.setSource(o);
		m.getTargets().add(r);
		return m;
	}

	private static String shortest(URI i) {
		return BASES.stream().sequential().map((b) -> i.deresolve(b)).map(URI::toString)
				.reduce(ToPseudoTask::shortestString).orElseGet(i::toString);
	}

	private static String shortestString(String l, String r) {
		if (l.length() < r.length()) {
			return l;
		} else {
			return r;
		}
	}
}
