package my.ecore2coq.workflow.tasks;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.SI2SI;

public class SIToSITask extends ObjectByObjectExtensionBasedTransformationTask {
	public SIToSITask() {
	}

	@Override
	protected EObject transform(MappingModel mapping, EObject root) {
		SI2SI transformer = new SI2SI(root.eResource().getResourceSet(), mapping);
		return transformer.transform(root);
	}

	@Override
	protected String getSourceExtension() {
		return "scheduled";
	}

	@Override
	protected String getTargetExtension() {
		return "scheduled2";
	}
}
