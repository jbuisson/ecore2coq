package my.ecore2coq.workflow.tasks;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.gallina.SI2Gallina;
import my.ecore2coq.workflow.Container;

public class SIToGTask extends ExtensionBasedTransformationTask {
	public SIToGTask() {
	}

	@Override
	protected List<? extends EObject> transform(IFile file, Container container, MappingModel mapping,
			List<EObject> root) {
		SI2Gallina transformer = new SI2Gallina();
		return transformer.transform(root, mapping);
	}

	@Override
	protected String getSourceExtension() {
		return "scheduled2";
	}

	@Override
	protected String getTargetExtension() {
		return "gallina";
	}
}
