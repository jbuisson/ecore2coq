package my.ecore2coq.workflow.tasks;

import java.util.List;

import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.ecore.EObject;

import pseudo.PseudoRoot;

public class GenmodelToPseudoTask extends ToPseudoTask {
	public GenmodelToPseudoTask() {
	}

	@Override
	protected String getSourceExtension() {
		return "genmodel";
	}

	@Override
	protected void populate(PseudoRoot r, List<EObject> source) {
		source.stream().map((x) -> (GenModel) x).map(GenModel::getGenPackages).flatMap(List::stream)
				.map(GenPackage::getEcorePackage).forEach(r.getRoots()::add);
	}
}
