package my.ecore2coq.workflow.tasks;

import java.util.Collection;
import java.util.Optional;

public abstract class ExtensionBasedTransformationTask extends M2MTransformationTask {
	public ExtensionBasedTransformationTask() {
		super();
	}

	protected abstract String getSourceExtension();

	protected abstract String getTargetExtension();

	@Override
	protected String map(String source) {
		return getTargetExtension();
	}

	@Override
	public Optional<String> isEligible(Collection<String> sources) {
		String s = getSourceExtension();
		return sources.stream().filter(s::contentEquals).findAny();
	}
}
