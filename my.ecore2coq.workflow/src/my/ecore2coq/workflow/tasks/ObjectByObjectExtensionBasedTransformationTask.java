package my.ecore2coq.workflow.tasks;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.workflow.Container;

public abstract class ObjectByObjectExtensionBasedTransformationTask extends ExtensionBasedTransformationTask {

	@Override
	protected List<EObject> transform(IFile file, Container container, MappingModel mapping, List<EObject> source) {
		return source.stream().map((x) -> transform(mapping, x)).collect(Collectors.toList());
	}

	protected abstract EObject transform(MappingModel mapping, EObject sourceRoot);

}
