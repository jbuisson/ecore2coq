package my.ecore2coq.workflow.tasks;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.E2L;

public class EcoreToLTask extends ObjectByObjectExtensionBasedTransformationTask {
	public EcoreToLTask() {
	}

	@Override
	protected EObject transform(MappingModel mapping, EObject root) {
		E2L transformer = new E2L(mapping);
		return transformer.transform(root);
	}

	@Override
	protected String getSourceExtension() {
		return "pseudo";
	}

	@Override
	protected String getTargetExtension() {
		return "linked";
	}
}
