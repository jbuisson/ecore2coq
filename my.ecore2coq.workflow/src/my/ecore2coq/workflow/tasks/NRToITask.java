package my.ecore2coq.workflow.tasks;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.NR2I;

public class NRToITask extends ObjectByObjectExtensionBasedTransformationTask {
	public NRToITask() {
	}

	@Override
	protected EObject transform(MappingModel mapping, EObject root) {
		NR2I transformer = new NR2I(root.eResource().getResourceSet(), mapping);
		return transformer.transform(root);
	}

	@Override
	protected String getSourceExtension() {
		return "noref";
	}

	@Override
	protected String getTargetExtension() {
		return "impl";
	}
}
