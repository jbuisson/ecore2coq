package my.ecore2coq.workflow.tasks;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.G2S;

public class GToSTask extends ObjectByObjectExtensionBasedTransformationTask {
	public GToSTask() {
	}

	@Override
	protected EObject transform(MappingModel mapping, EObject root) {
		G2S transformer = new G2S(mapping);
		return transformer.transform(root);
	}

	@Override
	protected String getSourceExtension() {
		return "generic";
	}

	@Override
	protected String getTargetExtension() {
		return "structural";
	}
}
