package my.ecore2coq.workflow.tasks;

import java.util.List;
import java.util.Optional;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import mapping.MappingModel;
import my.ecore2coq.SI2MyJava;
import my.ecore2coq.workflow.Container;
import my.java.TextGenerator;
import my.transform.utils.MyResourceSetImpl;
import my.utils.WorkspaceHelper;
import myjava.Unit;

public class SIToJTask extends ExtensionBasedTransformationTask {
	private final GenModel ecoreGenModel;

	public SIToJTask() {
		ResourceSet rs = new MyResourceSetImpl();
		Resource r = rs.getResource(URI.createPlatformPluginURI("org.eclipse.emf.ecore/model/Ecore.genmodel", true),
				true);
		ecoreGenModel = (GenModel) r.getContents().get(0);
	}

	@Override
	protected String getSourceExtension() {
		return "scheduled2";
	}

	@Override
	protected String getTargetExtension() {
		return "myjava";
	}

	@Override
	protected List<? extends EObject> transform(IFile file, Container container, MappingModel mapping,
			List<EObject> source) {
		Optional<GenModel> gm = container.get("genmodel")
				.flatMap(x -> x.stream().filter(e -> e instanceof GenModel).map(e -> (GenModel) e).findAny());
		IPath path = Path.fromPortableString(gm.get().getModelDirectory());
		IContainer root = file.getWorkspace().getRoot();
		IFolder base = root.getFolder(path);
		WorkspaceHelper.ensureExists(base);
		List<? extends EObject> r = new SI2MyJava(ecoreGenModel, mapping).transform(source);
		r.stream().filter(x -> x instanceof Unit).forEach(x -> TextGenerator.writeToFile(base, (Unit) x));
		return r;
	}
}
