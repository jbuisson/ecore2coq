package my.ecore2coq.workflow.tasks;

import org.eclipse.emf.ecore.EObject;

import mapping.MappingModel;
import my.ecore2coq.I2CI;

public class IToCITask extends ObjectByObjectExtensionBasedTransformationTask {
	public IToCITask() {
	}

	@Override
	protected EObject transform(MappingModel mapping, EObject root) {
		I2CI transformer = new I2CI(root.eResource().getResourceSet(), mapping);
		return transformer.transform(root);
	}

	@Override
	protected String getSourceExtension() {
		return "impl";
	}

	@Override
	protected String getTargetExtension() {
		return "cleanimpl";
	}
}
