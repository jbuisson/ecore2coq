package my.ecore2coq.workflow.internal;

public class StuckException extends Exception {

	private static final long serialVersionUID = -3679909044296974635L;

	public StuckException() {
		super();
	}

	public StuckException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public StuckException(String message, Throwable cause) {
		super(message, cause);
	}

	public StuckException(String message) {
		super(message);
	}

	public StuckException(Throwable cause) {
		super(cause);
	}

}
