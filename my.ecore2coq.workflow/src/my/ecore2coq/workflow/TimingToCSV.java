package my.ecore2coq.workflow;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

import my.transform.utils.MyResourceSetImpl;
import timing.Profile;
import timing.Task;

public class TimingToCSV {

	public void execute(SubMonitor monitor, Object[] resources) throws IOException, CoreException {
		Set<IContainer> containers = new HashSet<>();
		SubMonitor first = monitor.split(1).setWorkRemaining(resources.length + 1);
		for (Object i : resources) {
			if (first.isCanceled()) {
				throw new OperationCanceledException();
			}
			SubMonitor j = first.split(1);
			if (i instanceof IFile) {
				IFile file = (IFile) i;
				j.setTaskName(file.getName());
				containers.add(file.getParent());
				ResourceSet localResourceSet = new MyResourceSetImpl();
				executeFor(localResourceSet, file, j);
			}
			j.done();
		}
		SubMonitor second = first.split(1).setWorkRemaining(containers.size());
		for (IContainer i : containers) {
			if (first.isCanceled()) {
				throw new OperationCanceledException();
			}
			SubMonitor j = second.split(1);
			j.setTaskName(i.getName());
			i.refreshLocal(IResource.DEPTH_ONE, j);
		}
		second.done();
		first.done();
	}

	private void executeFor(ResourceSet resourceSet, IFile file, SubMonitor j) throws IOException {
		generateCsv(resourceSet, file);
		j.done();
	}

	public void generateCsv(ResourceSet resourceSet, IFile file) throws IOException {
		String location = file.getLocationURI().toString();
		URI rootURI = URI.createURI(location);
		Resource r = resourceSet.getResource(rootURI, true);
		EcoreUtil.resolveAll(resourceSet);
		Map<String, Integer> indices = new TreeMap<>();
		streamTasks(r).map(Task::getTaskName).forEachOrdered((n) -> {
			indices.putIfAbsent(n, indices.size());
		});
		File f = file.getLocation().addFileExtension("csv").toFile();
		try (PrintWriter w = new PrintWriter(new FileWriter(f))) {
			String[] head = new String[indices.size() + 1];
			head[0] = "\"File\"";
			indices.forEach((n, i) -> {
				head[i + 1] = "\"" + n + "\"";
			});
			w.println(Arrays.stream(head).collect(Collectors.joining(",")));
			streamTasks(r).collect(Collectors.groupingBy(TimingToCSV::nameOf)).forEach((n, t) -> {
				String[] line = new String[indices.size() + 1];
				Arrays.fill(line, "");
				line[0] = "\"" + fixUrl(n) + "\"";
				t.forEach((x) -> {
					line[indices.get(x.getTaskName()) + 1] = x.getTime().toString();
				});
				w.println(Arrays.stream(line).collect(Collectors.joining(",")));
			});
		}
	}

	private static String nameOf(Task t) {
		return URI.createURI(t.getFileName()).trimFileExtension().toString();
	}

	private static Stream<Task> streamTasks(Resource r) {
		return r.getContents().stream().map((x) -> (Profile) x).map(Profile::getTasks).flatMap(Collection::stream);
	}

	private static String fixUrl(String n) {
		return n.replace("-Java", "").replace("-QVTo", "");
	}
}
