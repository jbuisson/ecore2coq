package my.ecore2coq.workflow.activator;

import java.util.Map;
import java.util.stream.Stream;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.osgi.framework.BundleContext;

public class WorkflowActivator extends Plugin {
	private static WorkflowActivator instance = null;

	public static WorkflowActivator getInstance() {
		return instance;
	}

	public WorkflowActivator() {
		instance = this;
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		Map<String, Object> extensionToFactoryMap = Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap();
		Stream.of("linked", "generic", "structural", "noref", "impl", "cleanimpl", "flattened", "javainductive",
				"scheduled", "scheduled2", "gallina", "timing").forEach((s) -> {
					extensionToFactoryMap.put(s, new XMIResourceFactoryImpl());
					extensionToFactoryMap.put(s + "-backward", new XMIResourceFactoryImpl());
				});
	}

	public static void info(String s) {
		String pluginID = getInstance().getBundle().getSymbolicName();
		getInstance().getLog().log(new Status(IStatus.INFO, pluginID, s));
	}

	public static void error(Throwable t) {
		String pluginID = getInstance().getBundle().getSymbolicName();
		getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, t.getMessage(), t));
	}

	public static void error(String message, Throwable t) {
		String pluginID = getInstance().getBundle().getSymbolicName();
		getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, message, t));
	}

	public static void error(String message) {
		String pluginID = getInstance().getBundle().getSymbolicName();
		getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, message));
	}

	public static void warning(String message, Throwable t) {
		String pluginID = getInstance().getBundle().getSymbolicName();
		getInstance().getLog().log(new Status(IStatus.WARNING, pluginID, message, t));
	}
}
