package my.ecore2coq.workflow;

import java.util.stream.Stream;

import my.ecore2coq.workflow.tasks.CIToFTask;
import my.ecore2coq.workflow.tasks.EcoreToLTask;
import my.ecore2coq.workflow.tasks.EcoreToPseudoTask;
import my.ecore2coq.workflow.tasks.FToJITask;
import my.ecore2coq.workflow.tasks.GToSTask;
import my.ecore2coq.workflow.tasks.GToVTask;
import my.ecore2coq.workflow.tasks.IToCITask;
import my.ecore2coq.workflow.tasks.JIToSITask;
import my.ecore2coq.workflow.tasks.LToGTask;
import my.ecore2coq.workflow.tasks.NRToITask;
import my.ecore2coq.workflow.tasks.SIToGTask;
import my.ecore2coq.workflow.tasks.SIToSITask;
import my.ecore2coq.workflow.tasks.SToNRTask;
import my.ecore2coq.workflow.tasks.TransformationTask;

public class Ecore2CoqWorkflow extends MyAbstractWorkflow {
	@Override
	protected Stream<TransformationTask> getTasks() {
		return Stream.of(new EcoreToPseudoTask(), new EcoreToLTask(), new LToGTask(), new GToSTask(), new SToNRTask(),
				new NRToITask(), new IToCITask(), new CIToFTask(), new FToJITask(), new JIToSITask(), new SIToSITask(),
				new SIToGTask(), new GToVTask());
	}
}
