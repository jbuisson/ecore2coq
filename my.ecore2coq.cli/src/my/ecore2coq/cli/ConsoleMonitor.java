package my.ecore2coq.cli;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import my.ecore2coq.cli.activator.CliActivator;

public class ConsoleMonitor implements IProgressMonitor {
	private boolean canceled = false;

	@Override
	public void beginTask(String name, int totalWork) {
		info(name);
	}

	@Override
	public void done() {
	}

	@Override
	public void internalWorked(double work) {
	}

	@Override
	public boolean isCanceled() {
		return canceled;
	}

	@Override
	public void setCanceled(boolean value) {
		this.canceled = value;
	}

	@Override
	public void setTaskName(String name) {
		System.out.println(name);
		info(name);
	}

	@Override
	public void subTask(String name) {
		info(name);
	}

	@Override
	public void worked(int work) {
	}

	private static void info(String s) {
		String pluginID = CliActivator.getInstance().getBundle().getSymbolicName();
		CliActivator.getInstance().getLog().log(new Status(IStatus.INFO, pluginID, s));
	}
}
