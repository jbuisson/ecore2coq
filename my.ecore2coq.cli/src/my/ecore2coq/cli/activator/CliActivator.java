package my.ecore2coq.cli.activator;

import org.eclipse.core.runtime.Plugin;

public class CliActivator extends Plugin {
	private static CliActivator instance = null;

	public static CliActivator getInstance() {
		return instance;
	}

	public CliActivator() {
		instance = this;
	}
}
