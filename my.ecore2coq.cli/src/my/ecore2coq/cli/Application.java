package my.ecore2coq.cli;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

import my.ecore2coq.cli.activator.CliActivator;
import my.ecore2coq.workflow.Ecore2CoqWorkflow;
import my.utils.WorkspaceHelper;

public class Application implements IApplication {
	private static final String IMPORT_ = "--import=";
	private static final String SCAN_ = "--scan=";

	@Override
	public Object start(IApplicationContext context) throws Exception {
		info("Workspace location: " + ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString());
		importProjects(context);

		refreshAll();
		waitAutoBuild();

		List<IResource> files = getFiles();
		processFiles(files);
		return IApplication.EXIT_OK;
	}

	private void waitAutoBuild() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		IJobManager jobManager = Job.getJobManager();
		for (;;) {
			Job[] jobs = jobManager.find(ResourcesPlugin.FAMILY_AUTO_BUILD);
			if (jobs.length == 0) {
				break;
			}
			info("Waiting for " + jobs.length + " auto-build jobs");
			for (Job i : jobs) {
				try {
					i.join();
				} catch (InterruptedException e) {
				}
			}
		}
	}

	public void processFiles(List<IResource> files) throws CoreException, IOException {
		SubMonitor monitor = SubMonitor.convert(new ConsoleMonitor());
		new Ecore2CoqWorkflow().execute(monitor, files.stream().filter(Application::doValidate));
	}

	private static boolean doValidate(IResource r) {
		if (WorkspaceHelper.validate(r)) {
			return true;
		} else {
			info("skip  " + r.getLocation().toOSString());
			return false;
		}
	}

	public List<IResource> getFiles() throws CoreException {
		info("Looking for Ecore files in workspace");
		IProgressMonitor first = SubMonitor.convert(new NullProgressMonitor());
		List<IResource> files = WorkspaceHelper.listFiles(WorkspaceHelper.fileFilter(".ecore"), first);
		info("Found " + files.size() + " files");
		return files;
	}

	private void refreshAll() throws CoreException {
		info("Refreshing workspace");
		IProgressMonitor monitor = SubMonitor.convert(new ConsoleMonitor());
		ResourcesPlugin.getWorkspace().getRoot().refreshLocal(IResource.DEPTH_INFINITE, monitor);
	}

	private void importProjects(IApplicationContext context) {
		Object x = context.getArguments().get(IApplicationContext.APPLICATION_ARGS);
		if (x != null && x instanceof String[]) {
			String[] args = (String[]) x;
			for (int i = 0; i < args.length; ++i) {
				if (args[i].startsWith(IMPORT_)) {
					doImport(args[i].substring(IMPORT_.length()));
				} else if (args[i].startsWith(SCAN_)) {
					doScan(args[i].substring(SCAN_.length()));
				}
			}
		}
	}

	private void doScan(String path) {
		info("scanning " + path);
		IPath root = new Path(path);
		File f = root.toFile();
		if (f.isDirectory()) {
			for (File i : f.listFiles()) {
				if (i.isDirectory()) {
					if (new File(i, ".project").isFile()) {
						doImport(i.getPath());
					}
				}
			}
		} else {
			error("not a directory: " + path);
		}
	}

	private void doImport(String path) {
		info("importing " + path);
		IPath root = new Path(path);
		if (root.toFile().isDirectory()) {
			IPath projectFile = root.append(".project");
			if (projectFile.toFile().isFile()) {
				try {
					IWorkspace workspace = ResourcesPlugin.getWorkspace();
					IProjectDescription desc = workspace.loadProjectDescription(projectFile);
					IProject project = workspace.getRoot().getProject(desc.getName());
					if (project.exists()) {
						if (project.isOpen()) {
							project.close(new NullProgressMonitor());
							project.delete(false, true, new NullProgressMonitor());
						}
					}
					desc.setLocation(root);
					project.create(desc, new NullProgressMonitor());
					project.open(new NullProgressMonitor());
				} catch (CoreException e) {
					error("failed to import project: " + path, e);
				}
			} else {
				error("does not contain the .project file: " + path);
			}
		} else {
			error("not a directory: " + path);
		}
	}

	@Override
	public void stop() {
	}

	private static void info(String s) {
		String pluginID = CliActivator.getInstance().getBundle().getSymbolicName();
		CliActivator.getInstance().getLog().log(new Status(IStatus.INFO, pluginID, s));
		System.out.println(s);
	}

	private static void error(String message, Throwable t) {
		String pluginID = CliActivator.getInstance().getBundle().getSymbolicName();
		CliActivator.getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, message, t));
		System.err.println(message);
		t.printStackTrace(System.err);
	}

	private static void error(String message) {
		String pluginID = CliActivator.getInstance().getBundle().getSymbolicName();
		CliActivator.getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, message));
		System.err.println(message);
	}
}
