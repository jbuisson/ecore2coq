package my.ecore2coq.gallina;

import org.eclipse.m2m.qvt.oml.ModelExtent;

@FunctionalInterface
public interface TransformationMethod {
	boolean transform(ModelExtent source, ModelExtent lib, ModelExtent output, ModelExtent mapping);
}
