package my.ecore2coq.gallina;

import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.RandomAccess;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.m2m.qvt.oml.ModelExtent;

import gallina.Apply;
import gallina.Binder;
import gallina.Command;
import gallina.Constructor;
import gallina.Definition;
import gallina.File;
import gallina.Forall;
import gallina.Fun;
import gallina.GallinaFactory;
import gallina.Ident;
import gallina.Inductive;
import gallina.InductiveBody;
import gallina.Term;
import gallina.Type;
import mapping.MappingFactory;
import mapping.MappingModel;
import mapping.ObjectMapping;
import scheduledinductive.SIConstructor;
import scheduledinductive.SIDataType;
import scheduledinductive.SIDefinition;
import scheduledinductive.SIGAnyType;
import scheduledinductive.SIGClassifierType;
import scheduledinductive.SIGLowerBoundType;
import scheduledinductive.SIGType;
import scheduledinductive.SIGUpperBoundType;
import scheduledinductive.SIGVariableType;
import scheduledinductive.SIInductive;
import scheduledinductive.SIInductiveGroupDefinition;
import scheduledinductive.SIPackage;
import scheduledinductive.SIParameter;
import scheduledinductive.SISimpleDefinition;
import scheduledinductive.SITypeDef;
import scheduledinductive.SITypeParameter;

public class Handwritten {
	private final ModelExtent si;
	private final ModelExtent stdlib;
	private final ModelExtent g;
	private final ModelExtent m;

	private final Binder stdString;
	private final Binder stdBool;
	private final Binder stdZ;
	private final Binder stdNat;
	private final Binder stdOption;
	private final Binder stdList;
	private final Binder stdFalse;
	private final Binder stdProd;

	private final Map<EObject, Map<String, EObject>> mappings = new HashMap<>();
	private List<Runnable> late = new LinkedList<>();

	private final GallinaFactory f = GallinaFactory.eINSTANCE;

	public Handwritten(ModelExtent si, ModelExtent stdlib, ModelExtent g, ModelExtent m) {
		this.si = si;
		this.stdlib = stdlib;
		this.g = g;
		this.m = m;
		this.stdString = getStd("string");
		this.stdBool = getStd("bool");
		this.stdZ = getStd("Z");
		this.stdNat = getStd("nat");
		this.stdOption = getStd("option");
		this.stdList = getStd("list");
		this.stdFalse = getStd("False");
		this.stdProd = getStd("prod");
	}

	public void execute() {
		main();
		while (!late.isEmpty()) {
			List<Runnable> l = late;
			late = new LinkedList<>();
			l.forEach((r) -> {
				try {
					r.run();
				} catch (RetryException e) {
					late.add(r);
				}
			});
			if (same(late, l)) {
				throw new IllegalArgumentException("stuck...");
			}
		}
	}

	private void main() {
		List<EObject> g = new LinkedList<>();
		List<EObject> m = new LinkedList<>();
		si.getContents().stream().filter((x) -> x instanceof SIPackage).map((x) -> (SIPackage) x).map(this::pkg2file)
				.map(Handwritten::strict).forEach((x) -> {
					g.add(x.getF());
					m.add(x.getC());
				});
		this.g.setContents(g);
		this.m.setContents(m);
	}

	private Optional<FileAndMappingModel> pkg2file(SIPackage self) {
		return map(self, "SIPackage::pkg2file", (self_) -> {
			FileAndMappingModel fc = new FileAndMappingModel();
			MappingModel c = MappingFactory.eINSTANCE.createMappingModel();
			File result = f.createFile();
			result.setName(self.getName());
			self.getDefinitions().stream().map((x) -> def2cmd(x, c)).map(Handwritten::strict)
					.forEach(result.getCommands()::add);
			addObjectMapping(c, self, result);
			fc.setF(result);
			fc.setC(c);
			return fc;
		});
	}

	private Optional<? extends Command> def2cmd(SIDefinition self, MappingModel c) {
		if (self instanceof SIInductiveGroupDefinition) {
			SIInductiveGroupDefinition self_ = (SIInductiveGroupDefinition) self;
			return ind2ind(self_, c);
		} else if (self instanceof SISimpleDefinition) {
			SISimpleDefinition self_ = (SISimpleDefinition) self;
			Optional<? extends Command> r0 = simple2def(self_, c);
			return compute(r0, () -> {
				Optional<? extends Command> r1 = simple2ind(self_, c);
				Optional<? extends Command> r3 = compute(r1, () -> {
					Optional<? extends Command> r2 = typedef2def(self_, c);
					return r2;
				});
				return r3;
			});
		} else {
			return Optional.empty();
		}
	}

	private Optional<Inductive> ind2ind(SIInductiveGroupDefinition self, MappingModel c) {
		return map(self, "SIInductiveGroupDefinition::ind2ind", (self_) -> {
			Inductive result = f.createInductive();
			result.setName(self.getName());
			self.getInductives().stream().map((x) -> ind2body(x, c)).map(Handwritten::strict)
					.forEach(result.getBodies()::add);
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Optional<Definition> simple2def(SISimpleDefinition self, MappingModel c) {
		if (self.getType() instanceof SIDataType) {
			return map(self, "SISimpleDefinition::simple2def", (self_) -> {
				Definition result = f.createDefinition();
				result.setName(self.getName());
				SIDataType t = (SIDataType) self.getType();
				result.setBinder(strict(makeBinder(t, c)));
				result.setTerm(makeStdType(t.getInstanceClassName(), t.getETypeParameters()));
				addObjectMapping(c, self, result);
				return result;
			});
		} else {
			return Optional.empty();
		}
	}

	private Optional<Definition> typedef2def(SISimpleDefinition self, MappingModel c) {
		if (self.getType() instanceof SITypeDef) {
			return map(self, "SISimpleDefinition::typedef2def", (self_) -> {
				Definition result = f.createDefinition();
				result.setName(self.getName());
				SITypeDef t = (SITypeDef) self.getType();
				result.setBinder(strict(makeBinder(t, c)));
				result.setTerm(strict(typ2term(t.getType(), c)));
				addObjectMapping(c, self, result);
				return result;
			});
		} else {
			return Optional.empty();
		}
	}

	private Optional<Inductive> simple2ind(SISimpleDefinition self, MappingModel c) {
		if (self.getType() instanceof SIInductive) {
			return map(self, "SISimpleDefinition::simple2ind", (self_) -> {
				Inductive result = f.createInductive();
				result.setName(self.getName());
				result.getBodies().add(strict(ind2body((SIInductive) self.getType(), c)));
				addObjectMapping(c, self, result);
				return result;
			});
		} else {
			return Optional.empty();
		}
	}

	private Optional<InductiveBody> ind2body(SIInductive self, MappingModel c) {
		return map(self, "SIInductive::ind2body", (self_) -> {
			InductiveBody result = f.createInductiveBody();
			result.setName(self.getName());
			result.setBinder(strict(makeBinder(self, c)));
			self.getConstructors().stream().map((x) -> ctor2ctor(x, result.getBinder(), c)).map(Handwritten::strict)
					.forEach(result.getConstructors()::add);
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Optional<Constructor> ctor2ctor(SIConstructor self, Binder b, MappingModel c) {
		return map(self, "SIConstructor::ctor2ctor", (self_) -> {
			Constructor result = f.createConstructor();
			result.setName(self.getName());
			Binder o = f.createBinder();
			result.setBinder(o);
			o.setName(self.getName());
			List<Binder> p = self.getETypeParameters().stream().map((x) -> makeBinder(x, c)).map(Handwritten::strict)
					.collect(Collectors.toList());
			Ident i = f.createIdent();
			i.setBinder(b);
			Term concl = addParameters(i, self.getResultArguments().stream().map((x) -> typ2term(x, c))
					.map(Handwritten::strict).collect(Collectors.toList()));
			List<Binder> h = self.getParameters().stream().map((x) -> makeBinder(x, c)).map(Handwritten::strict)
					.collect(Collectors.toList());
			Term hyp = addHypotheses(concl, h);
			o.setType(bindParameters(hyp, p));
			addObjectMapping(c, self, result);
			addObjectMapping(c, self, result.getBinder());
			return result;
		});
	}

	private Optional<Binder> makeBinder(SIDataType self, MappingModel c) {
		return map(self, "SIDataType::makeBinder", (self_) -> {
			Binder result = f.createBinder();
			result.setName(self.getName());
			result.setType(makeType(self.getETypeParameters().size()));
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Optional<Binder> makeBinder(SIInductive self, MappingModel c) {
		return map(self, "SIInductive::makeBinder", (self_) -> {
			Binder result = f.createBinder();
			result.setName(self.getName());
			result.setType(makeType(self.getETypeParameters().size()));
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Optional<Binder> makeBinder(SITypeDef self, MappingModel c) {
		return map(self, "SITypeDef::makeBinder", (self_) -> {
			Binder result = f.createBinder();
			result.setName(self.getName());
			Type o = f.createType();
			result.setType(o);
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Optional<Binder> makeBinder(SITypeParameter self, MappingModel c) {
		return map(self, "SITypeParameter::makeBinder", (self_) -> {
			Binder result = f.createBinder();
			result.setName(self.getName());
			Type o = f.createType();
			result.setType(o);
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Optional<Binder> makeBinder(SIParameter self, MappingModel c) {
		return map(self, "SIParameter::makeBinder", (self_) -> {
			Binder result = f.createBinder();
			result.setName(self.getName());
			result.setType(strict(typ2term(self.getEType(), c)));
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Optional<? extends Term> typ2term(SIGType self, MappingModel c) {
		if (self instanceof SIGClassifierType) {
			SIGClassifierType self_ = (SIGClassifierType) self;
			return typ2term(self_, c);
		} else if (self instanceof SIGVariableType) {
			SIGVariableType self_ = (SIGVariableType) self;
			return typ2term(self_, c);
		} else if (self instanceof SIGAnyType) {
			SIGAnyType self_ = (SIGAnyType) self;
			return typ2term(self_, c);
		} else if (self instanceof SIGUpperBoundType) {
			SIGUpperBoundType self_ = (SIGUpperBoundType) self;
			return typ2term(self_, c);
		} else if (self instanceof SIGLowerBoundType) {
			SIGLowerBoundType self_ = (SIGLowerBoundType) self;
			return typ2term(self_, c);
		} else {
			return Optional.empty();
		}
	}

	private Optional<Term> typ2term(SIGClassifierType self, MappingModel c) {
		return map(self, "SIGClassifierType::typ2term", (self_) -> {
			Ident i = f.createIdent();
			lateResolveOne(self.getEClassifier(), Binder.class, (x) -> {
				i.setBinder(x);
			});
			Term result = addParams(i, self.getEArguments(), c);
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Optional<Ident> typ2term(SIGVariableType self, MappingModel c) {
		return map(self, "SIGVariableType::typ2term", (self_) -> {
			Ident result = f.createIdent();
			lateResolveOne(self.getETypeParameter(), Binder.class, (x) -> {
				result.setBinder(x);
			});
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Optional<Type> typ2term(SIGAnyType self, MappingModel c) {
		return map(self, "SIGAnyType::typ2term", (self_) -> {
			Type result = f.createType();
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Optional<Term> typ2term(SIGUpperBoundType self, MappingModel c) {
		return map(self, "SIGUpperBoundType::typ2term", (self_) -> {
			Term result = strict(typ2term(self.getEUpperBound(), c));
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Optional<Term> typ2term(SIGLowerBoundType self, MappingModel c) {
		return map(self, "SIGLowerBoundType::typ2term", (self_) -> {
			Type result = f.createType();
			addObjectMapping(c, self, result);
			return result;
		});
	}

	private Term addParams(Term self, Collection<SIGType> p, MappingModel c) {
		Term r = self;
		for (SIGType t : p) {
			Apply a = f.createApply();
			a.setLeft(r);
			a.setRight(strict(typ2term(t, c)));
			r = a;
		}
		return r;
	}

	private Term bindParameters(Term self, List<Binder> p) {
		Term r = self;
		for (Binder t : reverse(p)) {
			Forall a = f.createForall();
			a.setBinder(t);
			a.setTerm(r);
			r = a;
		}
		return r;
	}

	private Term addHypotheses(Term self, List<Binder> p) {
		Term r = self;
		for (Binder t : reverse(p)) {
			Forall a = f.createForall();
			a.setBinder(t);
			a.setTerm(r);
			r = a;
		}
		return r;
	}

	private Term addParameters(Term self, List<Term> p) {
		Term r = self;
		for (Term t : p) {
			Apply a = f.createApply();
			a.setLeft(r);
			a.setRight(t);
			r = a;
		}
		return r;
	}

	private Term makeType(int self) {
		if (self == 0) {
			Type r = f.createType();
			return r;
		} else {
			Forall a = f.createForall();
			Binder b = f.createBinder();
			Type c = f.createType();
			b.setType(c);
			a.setBinder(b);
			a.setTerm(makeType(self - 1));
			return a;
		}
	}

	private Term makeStdType(String self, Collection<SITypeParameter> p) {
		switch (self) {
		case "java.lang.String": {
			Ident a = f.createIdent();
			a.setBinder(stdString);
			return a;
		}
		case "java.util.List": {
			Ident a = f.createIdent();
			a.setBinder(stdList);
			return a;
		}
		case "java.util.Set": {
			Ident a = f.createIdent();
			a.setBinder(stdList);
			return a;
		}
		case "my.ecore2coq.utils.Bag": {
			Ident a = f.createIdent();
			a.setBinder(stdList);
			return a;
		}
		case "my.ecore2coq.utils.NoDupList": {
			Ident a = f.createIdent();
			a.setBinder(stdList);
			return a;
		}
		case "java.util.Map$Entry": {
			Ident a = f.createIdent();
			a.setBinder(stdProd);
			return a;
		}
		case "org.eclipse.emf.common.util.EList": {
			Ident a = f.createIdent();
			a.setBinder(stdList);
			return a;
		}
		case "org.eclipse.emf.common.util.URI": {
			Ident a = f.createIdent();
			a.setBinder(stdString);
			return a;
		}
		case "java.util.Optional": {
			Ident a = f.createIdent();
			a.setBinder(stdOption);
			return a;
		}
		case "java.lang.Byte": {
			Ident a = f.createIdent();
			a.setBinder(stdZ);
			return a;
		}
		case "byte": {
			Ident a = f.createIdent();
			a.setBinder(stdZ);
			return a;
		}
		case "java.lang.Short": {
			Ident a = f.createIdent();
			a.setBinder(stdZ);
			return a;
		}
		case "short": {
			Ident a = f.createIdent();
			a.setBinder(stdZ);
			return a;
		}
		case "java.lang.Integer": {
			Ident a = f.createIdent();
			a.setBinder(stdZ);
			return a;
		}
		case "int": {
			Ident a = f.createIdent();
			a.setBinder(stdZ);
			return a;
		}
		case "java.lang.Long": {
			Ident a = f.createIdent();
			a.setBinder(stdZ);
			return a;
		}
		case "long": {
			Ident a = f.createIdent();
			a.setBinder(stdZ);
			return a;
		}
		case "java.math.BigInteger": {
			Ident a = f.createIdent();
			a.setBinder(stdZ);
			return a;
		}
		case "java.lang.Boolean": {
			Ident a = f.createIdent();
			a.setBinder(stdBool);
			return a;
		}
		case "boolean": {
			Ident a = f.createIdent();
			a.setBinder(stdBool);
			return a;
		}
		case "coq.Nat": {
			return makeNat(p);
		}
		case "my.ecore2coq.utils.URI": {
			return makeNat(p);
		}
		default:
			return makeFalse(p);
		}
	}

	private Term makeWithParam(Collection<SITypeParameter> p, Binder b) {
		Ident i = f.createIdent();
		i.setBinder(b);
		Term r = i;
		for (@SuppressWarnings("unused")
		SITypeParameter t : reverse(p)) {
			Fun a = f.createFun();
			Binder x = f.createBinder();
			Type y = f.createType();
			x.setType(y);
			a.setBinder(x);
			a.setTerm(r);
			r = a;
		}
		return r;
	}

	private Term makeFalse(Collection<SITypeParameter> p) {
		return makeWithParam(p, stdFalse);
	}

	private Term makeNat(Collection<SITypeParameter> p) {
		return makeWithParam(p, stdNat);
	}

	private Binder getStd(String n) {
		return stdlib.getContents().stream().map(EObject::eAllContents).flatMap(Handwritten::stream)
				.filter((x) -> x instanceof Binder).map((x) -> (Binder) x)
				.filter((x) -> x.getName() != null && n.contentEquals(x.getName())).findAny()
				.orElseThrow(() -> new IllegalArgumentException(n));
	}

	private <T extends EObject, U extends EObject> void addObjectMapping(MappingModel c, T s, U t) {
		ObjectMapping x = MappingFactory.eINSTANCE.createObjectMapping();
		x.setSource(s);
		x.getTargets().add(t);
		c.getObjectMappings().add(x);
	}

	private static <T> Stream<T> stream(Iterator<T> i) {
		return StreamSupport.stream(Spliterators.spliterator(i, 0, Spliterator.IMMUTABLE), false);
	}

	private static <T> T strict(Optional<T> o) {
		return o.orElseThrow(IllegalArgumentException::new);
	}

	@SuppressWarnings("unused")
	private static <T> T nonStrict(Optional<T> o) {
		return o.orElse(null);
	}

	private static <T> Optional<? extends T> compute(Optional<? extends T> t, Supplier<Optional<? extends T>> e) {
		if (t.isPresent()) {
			return t;
		} else {
			return e.get();
		}
	}

	private <T extends EObject> void lateResolveOne(EObject x, Class<T> c, Consumer<T> f) {
		late.add(() -> {
			T y;
			try {
				y = resolveOne(x, c);
			} catch (IllegalArgumentException e) {
				throw new RetryException(e);
			}
			f.accept(y);
		});
	}

	private <T> T resolveOne(EObject x, Class<T> c) {
		Map<String, EObject> m = mappings.get(x);
		if (m == null) {
			throw new IllegalArgumentException("null");
		} else {
			List<EObject> l = m.values().stream().filter(c::isInstance).collect(Collectors.toList());
			if (l.size() == 1) {
				return c.cast(l.get(0));
			} else {
				throw new IllegalArgumentException("size = " + l.size());
			}
		}
	}

	private <F extends EObject, T extends EObject> Optional<T> map(F self, String n,
			Function<? super F, ? extends T> m) {
		Map<String, EObject> p = mappings.computeIfAbsent(self, (self_) -> new HashMap<>());
		@SuppressWarnings("unchecked")
		T t = (T) p.get(n);
		if (t == null) {
			t = m.apply(self);
			EObject prev = p.put(n, t);
			if (prev != null) {
				throw new IllegalArgumentException("spurious... ");
			}
		}
		return Optional.ofNullable(t);
	}

	private static <T> Iterable<T> reverse(Iterable<T> i) {
		if (i instanceof Deque) {
			Deque<T> i_ = (Deque<T>) i;
			return new Iterable<T>() {
				@Override
				public Iterator<T> iterator() {
					return i_.descendingIterator();
				}
			};
		} else if (i instanceof RandomAccess && i instanceof List) {
			List<T> i_ = (List<T>) i;
			return new Iterable<T>() {
				@Override
				public Iterator<T> iterator() {
					return new Iterator<T>() {
						private int current = i_.size() - 1;

						@Override
						public boolean hasNext() {
							return current >= 0;
						}

						@Override
						public T next() {
							return i_.get(current--);
						}
					};
				}
			};
		} else {
			Object[] i_ = StreamSupport.stream(i.spliterator(), false).toArray();
			return new Iterable<T>() {
				@Override
				public Iterator<T> iterator() {
					return new Iterator<T>() {
						private int current = i_.length - 1;

						@Override
						public boolean hasNext() {
							return current >= 0;
						}

						@SuppressWarnings("unchecked")
						@Override
						public T next() {
							return (T) i_[current--];
						}
					};
				}
			};
		}
	}

	private static <T> boolean same(List<T> left, List<T> right) {
		Iterator<T> l = left.iterator();
		Iterator<T> r = right.iterator();
		while (l.hasNext() && r.hasNext()) {
			T ll = l.next();
			T rr = r.next();
			if (ll != rr) {
				return false;
			}
		}
		return (!l.hasNext()) && (!r.hasNext());
	}
}
