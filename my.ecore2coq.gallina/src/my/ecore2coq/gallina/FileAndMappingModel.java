package my.ecore2coq.gallina;

import org.eclipse.emf.ecore.EObject;

import gallina.File;
import mapping.MappingModel;

public class FileAndMappingModel extends org.eclipse.emf.ecore.impl.MinimalEObjectImpl.Container implements EObject {
	private File f;
	private MappingModel c;

	public File getF() {
		return f;
	}

	public void setF(File f) {
		this.f = f;
	}

	public MappingModel getC() {
		return c;
	}

	public void setC(MappingModel c) {
		this.c = c;
	}
}
