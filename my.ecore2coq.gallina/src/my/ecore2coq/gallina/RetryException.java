package my.ecore2coq.gallina;

public class RetryException extends RuntimeException {
	private static final long serialVersionUID = 4371080503860029039L;

	public RetryException() {
	}

	public RetryException(Throwable e) {
		super(e);
	}
}
