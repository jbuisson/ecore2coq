package my.ecore2coq.gallina;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.m2m.internal.qvt.oml.tools.coverage.QVTOCoverageDecorator;
import org.eclipse.m2m.internal.qvt.oml.tools.coverage.common.CoverageData;
import org.eclipse.m2m.internal.qvt.oml.tools.coverage.common.CoverageDataPersistor;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.m2m.qvt.oml.util.Log;

import gallina.File;
import gallina.GallinaPackage;
import mapping.MappingModel;
import mapping.MappingPackage;
import my.ecore2coq.gallina.activator.GallinaActivator;
import my.ecore2coq.utils.NameFixer;
import my.si2gallina.files.Si2gallina;
import my.transform.utils.MyResourceSetImpl;
import scheduledinductive.ScheduledinductivePackage;

public class SI2Gallina {
	private final class MyLogger implements Log {
		@Override
		public void log(int level, String message, Object param) {
			String pluginID = GallinaActivator.getInstance().getBundle().getSymbolicName();
			GallinaActivator.getInstance().getLog()
					.log(new Status(IStatus.INFO, pluginID, message + " " + makeString(param)));
		}

		@Override
		public void log(int level, String message) {
			String pluginID = GallinaActivator.getInstance().getBundle().getSymbolicName();
			GallinaActivator.getInstance().getLog().log(new Status(IStatus.INFO, pluginID, message));
		}

		@Override
		public void log(String message, Object param) {
			String pluginID = GallinaActivator.getInstance().getBundle().getSymbolicName();
			GallinaActivator.getInstance().getLog()
					.log(new Status(IStatus.INFO, pluginID, message + " " + makeString(param)));
		}

		@Override
		public void log(String message) {
			String pluginID = GallinaActivator.getInstance().getBundle().getSymbolicName();
			GallinaActivator.getInstance().getLog().log(new Status(IStatus.INFO, pluginID, message));
		}

		private String makeString(Object param) {
			if (param == null) {
				return "(null)";
			} else {
				return "(" + param.getClass().getName() + ") " + param.toString();
			}
		}
	}

	private final URI transform = URI.createPlatformPluginURI("my.ecore2coq.gallina/transforms/si2gallina.qvto", true);

	private static enum Mode {
		JAVA, QVTO, ATL
	}

	private static final Mode mode = defaultMode(Mode.JAVA);

	private static EPackage.Registry registry;
	private static TransformationExecutor executor;
	private static boolean initialized = false;

	private static void verbose(String s) {
		String pluginID = GallinaActivator.getInstance().getBundle().getSymbolicName();
		GallinaActivator.getInstance().getLog().log(new Status(IStatus.INFO, pluginID, s));
	}

	private static void error(Throwable t) {
		String pluginID = GallinaActivator.getInstance().getBundle().getSymbolicName();
		GallinaActivator.getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, t.getMessage(), t));
	}

	private static void error(String s, Throwable t) {
		if (t == null) {
			error(s);
		} else {
			String pluginID = GallinaActivator.getInstance().getBundle().getSymbolicName();
			GallinaActivator.getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, s, t));
		}
	}

	private static void error(String s) {
		String pluginID = GallinaActivator.getInstance().getBundle().getSymbolicName();
		GallinaActivator.getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, s));
	}

	private static Mode defaultMode(Mode def) {
		String[] args = Platform.getCommandLineArgs();
		for (String i : args) {
			if ("--qvto".equals(i)) {
				Mode m = Mode.QVTO;
				verbose("SI2G mode: " + m.toString());
				return m;
			} else if ("--java".equals(i)) {
				Mode m = Mode.JAVA;
				verbose("SI2G mode: " + m.toString());
				return m;
			} else if ("--atl".equals(i)) {
				Mode m = Mode.ATL;
				verbose("SI2G mode: " + m.toString());
				return m;
			}
		}
		IWorkspaceRoot w = ResourcesPlugin.getWorkspace().getRoot();
		for (IProject p : w.getProjects()) {
			for (String n : p.getLocation().segments()) {
				String name = n.toUpperCase();
				for (Mode i : Mode.values()) {
					if (name.contains(i.name().toUpperCase())) {
						verbose("SI2G mode: " + i.toString());
						return i;
					}
				}
			}
		}
		verbose("SI2G mode: " + def.toString());
		return def;
	}

	public SI2Gallina() {
		if (!initialized) {
			registry = EPackage.Registry.INSTANCE;
			registry.putIfAbsent("http://my/ScheduledInductive", ScheduledinductivePackage.eINSTANCE);
			registry.putIfAbsent("http://my/gallina", GallinaPackage.eINSTANCE);
			registry.putIfAbsent("http://my/mapping", MappingPackage.eINSTANCE);
			registry.putIfAbsent("http://www.eclipse.org/emf/2002/Ecore", EcorePackage.eINSTANCE);
			executor = new TransformationExecutor(transform, registry);
			initialized = true;
		}
	}

	private TransformationMethod getMethod(Mode mode) {
		switch (mode) {
		case JAVA:
			return this::transformJava;
		case QVTO:
			return this::transformQVTo;
		case ATL:
			return this::transformATL;
		}
		throw new IllegalArgumentException("illegal mode " + mode);
	}

	public List<File> transform(List<EObject> in, MappingModel mapping) {
		return genericTransform(in, mapping, getMethod(mode));
	}

	private List<File> genericTransform(List<EObject> in, MappingModel mm, TransformationMethod m) {
		ResourceSet resourceSet = in.stream().map(EObject::eResource).filter((i) -> i != null)
				.map(Resource::getResourceSet).findAny().orElseGet(MyResourceSetImpl::new);
		ModelExtent source = new BasicModelExtent(in);
		ModelExtent output = new BasicModelExtent();
		ModelExtent mapping = new BasicModelExtent();
		Resource coqlib = my.gallina.Util.loadCoqLibResource(resourceSet);
		ModelExtent lib = new BasicModelExtent(coqlib.getContents());
		if (m.transform(source, lib, output, mapping)) {
			int s = in.size();
			if (output.getContents().size() != s) {
				error("not the right number of output Gallina models (" + output.getContents().size() + " -- " + s
						+ ")");
				return null;
			} else if (mapping.getContents().size() != s && mapping.getContents().size() != 0) {
				error("not the right number of output mapping models (" + mapping.getContents().size() + " -- " + s
						+ ")");
				return null;
			} else {
				return completion(in, output, mm, mapping);
			}
		} else {
			return null;
		}
	}

	private boolean transformATL(ModelExtent source, ModelExtent lib, ModelExtent target, ModelExtent mapping) {
		try {
			Si2gallina atl = new Si2gallina();
			Resource in = source.getContents().get(0).eResource();
			Resource coqlib = lib.getContents().get(0).eResource();
			atl.loadModels(in.getURI().toString(), coqlib.getURI().toString());
			atl.doSi2gallina(new NullProgressMonitor());
			URI uri = URI.createFileURI(java.io.File.createTempFile("out", ".xmi").getAbsolutePath());
			atl.saveModels(uri.toString());
			ResourceSet rs = new ResourceSetImpl();
			Resource out = rs.getResource(uri, true);
			target.setContents(out.getContents().stream().map(EcoreUtil::copy).collect(Collectors.toList()));
			return true;
		} catch (Throwable t) {
			error(t);
			return false;
		}
	}

	private boolean transformJava(ModelExtent source, ModelExtent lib, ModelExtent output, ModelExtent mapping) {
		Handwritten executor = new Handwritten(source, lib, output, mapping);
		try {
			executor.execute();
			return true;
		} catch (Throwable t) {
			error(t);
			return false;
		}
	}

	private void fixFileName(List<EObject> in, File file) {
		file.setName(NameFixer.getName(in));
	}

	private boolean transformQVTo(ModelExtent source, ModelExtent lib, ModelExtent output, ModelExtent mapping) {
		ExecutionContextImpl context = new ExecutionContextImpl();
		// CoverageData data = setupCoverage(context);
		context.setLog(new MyLogger());
		ExecutionDiagnostic result = executor.execute(context, source, lib, output, mapping);
		// CoverageDataPersistor.save(data);
		if (result.getSeverity() == Diagnostic.OK) {
			return true;
		} else {
			error(result.getMessage(), result.getException());
			for (Object i : result.getData()) {
				error(i.toString());
			}
			for (Diagnostic i : result.getChildren()) {
				error(i.getMessage(), i.getException());
			}
			return false;
		}
	}

	public List<File> completion(List<EObject> in, ModelExtent output, MappingModel targetMapping,
			ModelExtent mapping) {
		mapping.getContents().forEach((x) -> {
			MappingModel m = (MappingModel) x;
			targetMapping.getObjectMappings().addAll(m.getObjectMappings());
			targetMapping.getTemporaryObjects().addAll(m.getTemporaryObjects());
		});
		List<File> files = output.getContents().stream().map((x) -> (File) x).collect(Collectors.toList());
		if (files.isEmpty()) {
			error("no output Gallina model (" + files.size() + ")");
			return null;
		} else {
			fixFileName(in, files.get(0));
			return files;
		}
	}

	@SuppressWarnings({ "restriction", "unused" })
	private CoverageData setupCoverage(ExecutionContextImpl context) {
		List<Class<? extends org.eclipse.m2m.internal.qvt.oml.evaluator.QvtGenericVisitorDecorator>> decorators = new ArrayList<>();
		decorators.add(QVTOCoverageDecorator.class);
		context.getSessionData().setValue(
				org.eclipse.m2m.internal.qvt.oml.evaluator.QVTEvaluationOptions.VISITOR_DECORATORS, decorators);
		CoverageData data = CoverageDataPersistor.load();
		if (data == null) {
			data = new CoverageData();
		}
		context.getSessionData().setValue(QVTOCoverageDecorator.COVERAGE_DATA, data);
		return data;
	}
}
