package my.ecore2coq.gallina.activator;

import org.eclipse.core.runtime.Plugin;

public class GallinaActivator extends Plugin {
	private static GallinaActivator instance = null;

	public static GallinaActivator getInstance() {
		return instance;
	}

	public GallinaActivator() {
		instance = this;
	}
}
