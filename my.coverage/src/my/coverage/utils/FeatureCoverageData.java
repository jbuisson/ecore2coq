package my.coverage.utils;

import java.math.BigInteger;

public class FeatureCoverageData {
	private BigInteger hits = BigInteger.ZERO;

	public void hit() {
		this.hits = BigInteger.ONE.add(this.hits);
	}

	public BigInteger getHits() {
		return this.hits;
	}
}
