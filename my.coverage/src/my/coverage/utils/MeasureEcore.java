package my.coverage.utils;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import coverage.CoverageFactory;
import coverage.EcoreMetrics;
import coverage.EcoreSummary;
import coverage.MeasuredClass;

public class MeasureEcore {
	private final Map<EClass, MeasureEClass> classes = new HashMap<>();
	private BigInteger numberOfClasses = BigInteger.ZERO;
	private BigInteger numberOfDatatypes = BigInteger.ZERO;
	private BigInteger numberOfEnums = BigInteger.ZERO;

	public void collectFrom(Resource r) {
		r.getAllContents().forEachRemaining(this::collectFrom);
	}

	public void collectFrom(EObject o) {
		if (o instanceof EClass) {
			EClass c = (EClass) o;
			this.numberOfClasses = BigInteger.ONE.add(this.numberOfClasses);
			MeasureEClass m = new MeasureEClass(c);
			m.collectFrom();
			classes.put(c, m);
		} else if (o instanceof EDataType) {
			this.numberOfDatatypes = BigInteger.ONE.add(this.numberOfDatatypes);
			if (o instanceof EEnum) {
				this.numberOfEnums = BigInteger.ONE.add(this.numberOfEnums);
			}
		}
	}

	public void populate(EcoreMetrics m) {
		EcoreSummary s = CoverageFactory.eINSTANCE.createEcoreSummary();
		s.setNumberOfClasses(this.numberOfClasses);
		s.setNumberOfDataTypes(this.numberOfDatatypes);
		s.setNumberOfEnums(this.numberOfEnums);
		EList<MeasuredClass> l = m.getMeasuredClasses();
		classes.values().stream().map(MeasureEClass::toEcoreMetrics).forEach(l::add);
		s.setNumberOfAttributes(sum(MeasureEClass::getNumberOfAttributes));
		s.setNumberOfCompositionReferences(sum(MeasureEClass::getNumberOfCompositionReferences));
		s.setNumberOfReferences(sum(MeasureEClass::getNumberOfReferences));
		s.setNumberOfStructuralFeatures(sum(MeasureEClass::getNumberOfStructuralFeatures));
		s.setNumberOfSupertypes(sum(MeasureEClass::getNumberOfSupertypes));
		s.setMaximumDepthOfInheritance(max(MeasureEClass::getDepthOfInheritance));
		s.setMaximumWidthOfInheritance(max(MeasureEClass::getWidthOfInheritance));
		s.setMaximumMultipleInheritance(max(MeasureEClass::getMultipleInheritance));
		m.setSummary(s);
	}

	private BigInteger sum(Function<MeasureEClass, BigInteger> f) {
		return classes.values().stream().map(f).reduce(BigInteger.ZERO, BigInteger::add);
	}

	private BigInteger max(Function<MeasureEClass, BigInteger> f) {
		return classes.values().stream().map(f).reduce(BigInteger.ZERO, BigInteger::max);
	}
}
