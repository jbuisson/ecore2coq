package my.coverage.utils;

import java.math.BigInteger;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import coverage.CoverageFactory;
import coverage.MeasuredClass;

public class MeasureEClass {
	private final EClass target;
	private BigInteger numberOfStructuralFeatures = BigInteger.ZERO;
	private BigInteger numberOfReferences = BigInteger.ZERO;
	private BigInteger numberOfCompositionReferences = BigInteger.ZERO;
	private BigInteger numberOfAttributes = BigInteger.ZERO;
	private BigInteger numberOfSupertypes = BigInteger.ZERO;
	private BigInteger depthOfInheritance = BigInteger.ZERO;
	private BigInteger widthOfInheritance = BigInteger.ZERO;
	private BigInteger multipleInheritance = BigInteger.ZERO;

	public MeasureEClass(EClass target) {
		this.target = target;
	}

	public void collectFrom() {
		EList<EStructuralFeature> l = target.getEStructuralFeatures();
		this.numberOfStructuralFeatures = BigInteger.valueOf(l.size());
		this.numberOfReferences = streamEReferences(l).collect(counter());
		this.numberOfCompositionReferences = streamEReferences(l).filter(EReference::isContainment).collect(counter());
		this.numberOfAttributes = streamEAttributes(l).collect(counter());
		this.numberOfSupertypes = BigInteger
				.valueOf(Math.max(target.getESuperTypes().size(), target.getEGenericSuperTypes().size()));
		this.depthOfInheritance = computeDepthOfInheritance(target);
		this.widthOfInheritance = computeWidthOfInheritance(target);
		this.multipleInheritance = computeMultipleInheritance(target);
	}

	private static BigInteger computeDepthOfInheritance(EClass c) {
		if (EcorePackage.Literals.EOBJECT.equals(c)) {
			return BigInteger.ZERO;
		} else {
			return streamSuperClasses(c).map(MeasureEClass::computeDepthOfInheritance).map(BigInteger.ONE::add)
					.reduce(BigInteger.ZERO, BigInteger::max);
		}
	}

	private static Stream<EClass> streamSuperClasses(EClass c) {
		return Stream.concat(c.getESuperTypes().stream(),
				c.getEGenericSuperTypes().stream().map(EGenericType::getEClassifier).flatMap((x) -> {
					if (x == null) {
						return Stream.empty();
					} else {
						return Stream.of(x);
					}
				})).map((x) -> (EClass) x);
	}

	private static BigInteger computeWidthOfInheritance(EClass c) {
		Set<EClass> superClasses = streamSetOfSuperclasses(c).collect(Collectors.toSet());
		return BigInteger.valueOf(superClasses.size());
	}

	private static BigInteger computeMultipleInheritance(EClass c) {
		Set<EClass> superClasses = streamSuperClasses(c).collect(Collectors.toSet());
		return BigInteger.valueOf(superClasses.size());
	}

	private static Stream<EClass> streamSetOfSuperclasses(EClass c) {
		return streamSuperClasses(c).flatMap((x) -> Stream.concat(Stream.of(x), streamSetOfSuperclasses(x)));
	}

	public BigInteger getNumberOfStructuralFeatures() {
		return numberOfStructuralFeatures;
	}

	public BigInteger getNumberOfReferences() {
		return numberOfReferences;
	}

	public BigInteger getNumberOfCompositionReferences() {
		return numberOfCompositionReferences;
	}

	public BigInteger getNumberOfAttributes() {
		return numberOfAttributes;
	}

	public BigInteger getNumberOfSupertypes() {
		return numberOfSupertypes;
	}

	public BigInteger getDepthOfInheritance() {
		return depthOfInheritance;
	}

	public BigInteger getWidthOfInheritance() {
		return widthOfInheritance;
	}

	public BigInteger getMultipleInheritance() {
		return multipleInheritance;
	}

	private Stream<EReference> streamEReferences(EList<EStructuralFeature> l) {
		return l.stream().filter((x) -> x instanceof EReference).map((x) -> (EReference) x);
	}

	private Stream<EAttribute> streamEAttributes(EList<EStructuralFeature> l) {
		return l.stream().filter((x) -> x instanceof EAttribute).map((x) -> (EAttribute) x);
	}

	public MeasuredClass toEcoreMetrics() {
		MeasuredClass m = CoverageFactory.eINSTANCE.createMeasuredClass();
		m.setMeasuredClass(target);
		m.setNumberOfStructuralFeatures(this.numberOfStructuralFeatures);
		m.setNumberOfReferences(this.numberOfReferences);
		m.setNumberOfCompositionReferences(this.numberOfCompositionReferences);
		m.setNumberOfAttributes(this.numberOfAttributes);
		m.setNumberOfSupertypes(this.numberOfSupertypes);
		m.setDepthOfInheritance(this.depthOfInheritance);
		m.setMultipleInheritance(this.multipleInheritance);
		return m;
	}

	private static <T> Collector<T, ?, BigInteger> counter() {
		return Collectors.reducing(BigInteger.ZERO, (x) -> BigInteger.ONE, BigInteger::add);
	}

}
