package my.coverage.utils;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;

import coverage.Coverage;
import coverage.CoverageFactory;
import coverage.CoveredClass;
import coverage.EcoreMetrics;
import coverage.Summary;

public class CoverageData {
	private final Map<EClassifier, ClassifierCoverageData> data = new HashMap<>();
	private final Set<EPackage> packages = new HashSet<>();
	private final Set<EDataType> datatypes = new HashSet<>();
	private MeasureEcore metrics = null;

	public void populate(Coverage c) {
		EList<CoveredClass> l = c.getCoveredClasses();
		streamEntries().map(this::createCoveredClass).forEach(l::add);
		if (metrics != null) {
			EcoreMetrics m = CoverageFactory.eINSTANCE.createEcoreMetrics();
			metrics.populate(m);
			c.setEcoreMetrics(m);
		}
	}

	public void populate(Summary s) {
		s.setTotalNumberOfClasses(streamAllEClasses().collect(counter()));
		s.setNumberOfHitClasses(
				streamEClassifierCoverageData().filter((c) -> !BigInteger.ZERO.equals(c.getHits())).collect(counter()));
		s.setNumberOfHitInheritedClasses(streamEClassifierCoverageData()
				.filter((c) -> !BigInteger.ZERO.equals(c.getInheritedHits())).collect(counter()));
		Predicate<EClassifier> p = data::containsKey;
		s.setNumberOfMissedClasses(streamAllEClasses().filter(p.negate()).collect(counter()));
		s.setTotalNumberOfStructuralFeatures(streamCoveredEClasses().map(EClass::getEStructuralFeatures)
				.map(EList::size).map(BigInteger::valueOf).reduce(BigInteger.ZERO, BigInteger::add));
		s.setNumberOfHitStructuralFeatures(
				streamEClassEntries()
						.map((x) -> ((EClass) x.getKey()).getEStructuralFeatures().stream()
								.filter(x.getValue()::contains).collect(counter()))
						.reduce(BigInteger.ZERO, BigInteger::add));
		s.setNumberOfMissedStructuralFeatures(streamEClassEntries()
				.map((x) -> ((EClass) x.getKey()).getEStructuralFeatures().stream()
						.filter((f) -> !x.getValue().contains(f)).collect(counter()))
				.reduce(BigInteger.ZERO, BigInteger::add));
		s.setTotalNumberOfDataTypes(streamEDataTypes().collect(counter()));
		Predicate<EDataType> q = datatypes::contains;
		s.setNumberOfHitDataTypes(streamEDataTypes().filter(q).collect(counter()));
		s.setNumberOfMissedDataTypes(streamEDataTypes().filter(q.negate()).collect(counter()));
	}

	private Stream<EDataType> streamEDataTypes() {
		return streamAllEClassifiers().filter((c) -> c instanceof EDataType).map((x) -> (EDataType) x);
	}

	private Stream<Entry<EClassifier, ClassifierCoverageData>> streamEClassEntries() {
		return streamEntries().filter((c) -> c.getKey() instanceof EClass);
	}

	private Stream<Entry<EClassifier, ClassifierCoverageData>> streamEntries() {
		return data.entrySet().stream();
	}

	private Stream<EClass> streamCoveredEClasses() {
		return streamCoveredEClassifiers().filter((c) -> c instanceof EClass).map((c) -> (EClass) c);
	}

	private Stream<EClassifier> streamCoveredEClassifiers() {
		return data.keySet().stream();
	}

	private Stream<ClassifierCoverageData> streamEClassifierCoverageData() {
		return data.values().stream();
	}

	private static <T> Collector<T, ?, BigInteger> counter() {
		return Collectors.reducing(BigInteger.ZERO, (x) -> BigInteger.ONE, BigInteger::add);
	}

	private Stream<EClassifier> streamAllEClasses() {
		return streamAllEClassifiers().filter((c) -> c instanceof EClass);
	}

	private Stream<EClassifier> streamAllEClassifiers() {
		return packages.stream().map(EPackage::getEClassifiers).flatMap(EList::stream);
	}

	private CoveredClass createCoveredClass(Map.Entry<EClassifier, ClassifierCoverageData> d) {
		CoveredClass c = CoverageFactory.eINSTANCE.createCoveredClass();
		EClassifier cl = d.getKey();
		packages.add(cl.getEPackage());
		c.setCoveredClass(cl);
		ClassifierCoverageData e = d.getValue();
		c.setHits(e.getHits());
		c.setInheritedHits(e.getInheritedHits());
		e.populate(c.getCoveredFeatures());
		if (cl instanceof EClass) {
			((EClass) cl).getEStructuralFeatures().stream().filter((i) -> !e.contains(i))
					.forEach(c.getUncoveredFeatures()::add);
		}
		return c;
	}

	public void hit(EClassifier c) {
		this.get(c).hit();
	}

	private ClassifierCoverageData get(EClassifier c) {
		return data.computeIfAbsent(c, (x) -> new ClassifierCoverageData());
	}

	public void hitAsSuper(EClassifier c) {
		this.get(c).hitAsSuper();
	}

	public void hit(EStructuralFeature f) {
		this.get(f.getEContainingClass()).hit(f);
	}

	public void collectFrom(Resource r) {
		r.getAllContents().forEachRemaining(this::collectFrom);
		if (r.getContents().size() == 1) {
			EObject root = r.getContents().get(0);
			if (root instanceof EPackage) {
				if (this.metrics == null) {
					this.metrics = new MeasureEcore();
				}
				this.metrics.collectFrom(r);
			}
		}
	}

	public void collectFrom(EObject o) {
		EClass c = o.eClass();
		this.hit(c);
		Set<EClassifier> supers = new HashSet<>();
		addSupersOf(supers, c);
		supers.stream().forEach(this::hitAsSuper);
		c.getEAllStructuralFeatures().stream().filter((f) -> o.eIsSet(f)).forEach(this::collectFrom);
	}

	public void collectFrom(EStructuralFeature f) {
		this.hit(f);
		Stream.concat(Stream.of(f.getEType()), typesOfGeneric(f.getEGenericType()))
				.filter((x) -> x instanceof EDataType).map((x) -> (EDataType) x).forEach(datatypes::add);
	}

	private static void addSupersOf(Set<EClassifier> supers, EClass c) {
		c.getESuperTypes().stream().filter(supers::add).forEach((d) -> addSupersOf(supers, d));
		c.getEGenericSuperTypes().stream().flatMap(CoverageData::typesOfGeneric);
	}

	private static Stream<EClassifier> typesOfGeneric(EGenericType g) {
		return Stream.of(ifSet(g, EcorePackage.Literals.EGENERIC_TYPE__ECLASSIFIER),
				ifSet(g, EcorePackage.Literals.EGENERIC_TYPE__ERAW_TYPE),
				Stream.of(ifSet(g, EcorePackage.Literals.EGENERIC_TYPE__ELOWER_BOUND),
						ifSet(g, EcorePackage.Literals.EGENERIC_TYPE__EUPPER_BOUND), g.getETypeArguments().stream())
						.flatMap((i) -> i).map((i) -> (EGenericType) i).flatMap(CoverageData::typesOfGeneric))
				.flatMap((i) -> i).map((i) -> (EClassifier) i);
	}

	private static <U extends EObject> Stream<?> ifSet(U u, EStructuralFeature f) {
		if (u.eIsSet(f)) {
			return Stream.of(u.eGet(f));
		} else {
			return Stream.empty();
		}
	}
}
