package my.coverage.utils;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EStructuralFeature;

import coverage.CoverageFactory;
import coverage.CoveredFeature;

public class ClassifierCoverageData {
	private BigInteger hits = BigInteger.ZERO;
	private BigInteger inheritedHits = BigInteger.ZERO;
	private final Map<EStructuralFeature, FeatureCoverageData> data = new HashMap<>();

	public void hit() {
		this.hits = BigInteger.ONE.add(this.hits);
	}

	public BigInteger getHits() {
		return this.hits;
	}

	public void hitAsSuper() {
		this.inheritedHits = BigInteger.ONE.add(this.inheritedHits);
	}

	public BigInteger getInheritedHits() {
		return this.inheritedHits;
	}

	public void populate(EList<CoveredFeature> l) {
		data.entrySet().stream().map(ClassifierCoverageData::createCoveredFeature).forEach(l::add);
	}

	public boolean contains(EStructuralFeature f) {
		return data.containsKey(f);
	}

	private static CoveredFeature createCoveredFeature(Map.Entry<EStructuralFeature, FeatureCoverageData> e) {
		CoveredFeature r = CoverageFactory.eINSTANCE.createCoveredFeature();
		r.setCoveredFeature(e.getKey());
		r.setHits(e.getValue().getHits());
		return r;
	}

	public void hit(EStructuralFeature f) {
		data.computeIfAbsent(f, (x) -> new FeatureCoverageData()).hit();
	}

	public BigInteger getNumberOfHitStructuralFeatures() {
		return BigInteger.valueOf(data.size());
	}
}
