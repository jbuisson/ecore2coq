package my.coverage.ui.popup.actions;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import coverage.Coverage;
import coverage.CoverageFactory;
import coverage.CoverageModel;
import coverage.CoveragePackage;
import coverage.EcoreSummary;
import coverage.PerModel;
import my.coverage.activator.CoverageActivator;
import my.coverage.utils.CoverageData;
import my.transform.utils.MyResourceSetImpl;

public class Compute extends AbstractHandler {
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Job job = Job.create(this.getClass().getSimpleName(), (progressMonitor) -> doExecute(event, progressMonitor));
		job.schedule();
		return null;
	}

	private static Map<?, ?> saveOptions() {
		Map<String, Object> r = new HashMap<>();
		r.put(XMLResource.OPTION_URI_HANDLER, new URIHandlerImpl.PlatformSchemeAware());
		return r;
	}

	private void doExecute(ExecutionEvent event, IProgressMonitor progressMonitor) {
		SubMonitor sub = SubMonitor.convert(progressMonitor, 1);
		try {
			ISelection selection = HandlerUtil.getActiveMenuSelection(event);
			if (!selection.isEmpty() && selection instanceof IStructuredSelection) {
				CoverageModel model = CoverageFactory.eINSTANCE.createCoverageModel();
				CoverageData globalRaw = new CoverageData();
				IStructuredSelection structuredSelection = (IStructuredSelection) selection;
				ResourceSet resourceSet = new MyResourceSetImpl();
				Object[] resources = structuredSelection.toArray();
				SubMonitor first = sub.split(1).setWorkRemaining(resources.length);
				String base = null;
				for (Object i : resources) {
					if (first.isCanceled()) {
						throw new OperationCanceledException();
					}
					SubMonitor j = first.split(1);
					if (i instanceof IFile) {
						IFile file = (IFile) i;
						j.setTaskName(file.getName());
						base = file.getWorkspace().getRoot().getLocationURI().toString();
						CoverageData localRaw = new CoverageData();
						EList<EObject> roots = executeFor(globalRaw, localRaw, resourceSet, file, j);
						Coverage localData = createData(localRaw);
						PerModel perModel = CoverageFactory.eINSTANCE.createPerModel();
						perModel.setData(localData);
						perModel.getRoots().addAll(roots);
						model.getPerModel().add(perModel);
					}
					j.done();
				}
				first.done();
				if (base != null) {
					Coverage globalData = createData(globalRaw);
					Resource report = resourceSet.createResource(URI.createURI(base).appendSegment("report.coverage"));
					model.setGlobal(globalData);
					report.getContents().add(model);
					report.save(saveOptions());
					String csv = URI.createURI(base).appendSegment("coverage.csv").toFileString();
					try (PrintWriter writer = new PrintWriter(new FileWriter(csv))) {
						EStructuralFeature[] f = new EStructuralFeature[] { EcorePackage.Literals.ENAMED_ELEMENT__NAME,
								CoveragePackage.Literals.ECORE_SUMMARY__NUMBER_OF_DATA_TYPES,
								CoveragePackage.Literals.ECORE_SUMMARY__NUMBER_OF_ENUMS,
								CoveragePackage.Literals.ECORE_SUMMARY__NUMBER_OF_CLASSES,
								CoveragePackage.Literals.ECORE_SUMMARY__MAXIMUM_DEPTH_OF_INHERITANCE,
								CoveragePackage.Literals.ECORE_SUMMARY__MAXIMUM_WIDTH_OF_INHERITANCE,
								CoveragePackage.Literals.ECORE_SUMMARY__MAXIMUM_MULTIPLE_INHERITANCE,
								CoveragePackage.Literals.ECORE_GENERIC_METRICS__NUMBER_OF_STRUCTURAL_FEATURES,
								CoveragePackage.Literals.ECORE_GENERIC_METRICS__NUMBER_OF_ATTRIBUTES,
								CoveragePackage.Literals.ECORE_GENERIC_METRICS__NUMBER_OF_REFERENCES,
								CoveragePackage.Literals.ECORE_GENERIC_METRICS__NUMBER_OF_COMPOSITION_REFERENCES };
						String sep = "\t";
						writer.println(Arrays.stream(f).map(ENamedElement::getName).collect(Collectors.joining(sep)));
						model.getPerModel().forEach((m) -> {
							writer.println(Arrays.stream(f).map((s) -> get(m, s)).collect(Collectors.joining(sep)));
						});
					}
				}
			}
		} catch (OperationCanceledException e) {
		} catch (IOException e) {
			error(e);
		} finally {
			sub.done();
		}
	}

	private String get(PerModel m, EStructuralFeature f) {
		if (EcorePackage.Literals.ENAMED_ELEMENT__NAME.equals(f)) {
			EObject oneRoot = m.getRoots().get(0);
			Resource r = oneRoot.eResource();
			return r.getURI().toString();
		} else {
			EcoreSummary s = m.getData().getEcoreMetrics().getSummary();
			return s.eGet(f).toString();
		}
	}

	private Coverage createData(CoverageData raw) {
		Coverage data = CoverageFactory.eINSTANCE.createCoverage();
		raw.populate(data);
		data.setSummary(CoverageFactory.eINSTANCE.createSummary());
		raw.populate(data.getSummary());
		return data;
	}

	private EList<EObject> executeFor(CoverageData globalRaw, CoverageData localRaw, ResourceSet resourceSet,
			IFile file, SubMonitor monitor) {
		String location = file.getLocationURI().toString();
		URI rootURI = URI.createURI(location);
		monitor.setWorkRemaining(1);
		Resource resource = resourceSet.getResource(rootURI, true);
		EcoreUtil.resolveAll(resource);
		globalRaw.collectFrom(resource);
		localRaw.collectFrom(resource);
		return resource.getContents();
	}

	private static void error(Throwable t) {
		String pluginID = CoverageActivator.getInstance().getBundle().getSymbolicName();
		CoverageActivator.getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, t.getMessage(), t));
	}
}
