package my.coverage.activator;

import org.eclipse.ui.plugin.AbstractUIPlugin;

public class CoverageActivator extends AbstractUIPlugin {
	private static CoverageActivator instance = null;

	public static CoverageActivator getInstance() {
		return instance;
	}

	public CoverageActivator() {
		instance = this;
	}
}
