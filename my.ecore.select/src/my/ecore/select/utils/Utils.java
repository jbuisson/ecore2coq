package my.ecore.select.utils;

import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.navigator.resources.ProjectExplorer;

import my.ecore.select.activator.SelectActivator;

public class Utils {

	public static void select(List<Object> toSelect) {
		select(toSelect.toArray());
	}

	public static void select(Object... toSelect) {
		Display.getDefault().syncExec(() -> {
			try {
				final ProjectExplorer view = (ProjectExplorer) getActiveWindow().getActivePage()
						.showView(ProjectExplorer.VIEW_ID);
				view.selectReveal(new StructuredSelection(toSelect));
			} catch (PartInitException e) {
				error(e);
			}
		});
	}

	private static IWorkbenchWindow getActiveWindow() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final IWorkbenchWindow activeWindow = workbench.getActiveWorkbenchWindow();
		return activeWindow;
	}

	private static void error(Throwable t) {
		String pluginID = SelectActivator.getInstance().getBundle().getSymbolicName();
		SelectActivator.getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, t.getMessage(), t));
	}
}
