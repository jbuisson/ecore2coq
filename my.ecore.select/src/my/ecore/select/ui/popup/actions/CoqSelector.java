package my.ecore.select.ui.popup.actions;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.jobs.Job;

import my.ecore.select.utils.Utils;
import my.utils.WorkspaceHelper;

public class CoqSelector extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Job j = Job.create("select", (monitor) -> {
			final List<IResource> toSelect = WorkspaceHelper.listFiles(WorkspaceHelper.fileFilter(".v"), monitor);
			Utils.select(toSelect);
		});
		j.schedule();
		return null;
	}
}
