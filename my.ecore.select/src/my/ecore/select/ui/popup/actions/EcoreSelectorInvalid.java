package my.ecore.select.ui.popup.actions;

import java.util.List;
import java.util.function.Predicate;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.jobs.Job;

import my.ecore.select.utils.Utils;
import my.utils.WorkspaceHelper;

public class EcoreSelectorInvalid extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Job j = Job.create("select", (monitor) -> {
			Predicate<IResource> validate = WorkspaceHelper::validate;
			final List<IResource> toSelect = WorkspaceHelper
					.listFiles(WorkspaceHelper.fileFilter(".ecore").and(validate.negate()), monitor);
			Utils.select(toSelect);
		});
		j.schedule();
		return null;
	}
}
