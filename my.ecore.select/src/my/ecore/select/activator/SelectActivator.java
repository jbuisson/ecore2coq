package my.ecore.select.activator;

import org.eclipse.ui.plugin.AbstractUIPlugin;

public class SelectActivator extends AbstractUIPlugin {
	private static SelectActivator instance = null;

	public static SelectActivator getInstance() {
		return instance;
	}

	public SelectActivator() {
		instance = this;
	}
}
