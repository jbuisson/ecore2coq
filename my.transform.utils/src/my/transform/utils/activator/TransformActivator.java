package my.transform.utils.activator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;

public class TransformActivator extends Plugin {
	private static TransformActivator instance = null;

	public static TransformActivator getInstance() {
		return instance;
	}

	public TransformActivator() {
		instance = this;
	}

	public static void warning(String s) {
		String pluginID = getInstance().getBundle().getSymbolicName();
		getInstance().getLog().log(new Status(IStatus.WARNING, pluginID, s));
	}
}
