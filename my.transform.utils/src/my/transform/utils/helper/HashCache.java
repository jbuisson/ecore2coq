package my.transform.utils.helper;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class HashCache<T, U> implements Function<T, U> {
	private final Function<T, U> implementation;
	private final Map<T, U> cache = new ConcurrentHashMap<>();

	public HashCache(Function<T, U> f) {
		implementation = f;
	}

	@Override
	public U apply(T t) {
		return cache.computeIfAbsent(t, implementation);
	}
}
