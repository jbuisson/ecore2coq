package my.transform.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EStructuralFeature;

public class ReflectiveHookableTransformer extends HookableTransformer {
	private List<Method> methods = collectHookMethods(this);

	protected ReflectiveHookableTransformer() {
	}

	private static List<Method> collectHookMethods(Object self) {
		return Arrays.stream(self.getClass().getMethods())
				.filter((i) -> i.isAnnotationPresent(Rule.class) && i.getParameterCount() == 2)
				.collect(Collectors.toList());
	}

	@Override
	protected FeatureHandler getHandlerFor(EStructuralFeature feature) {
		List<Method> filtered = methods.stream().filter((i) -> i.getName().contentEquals(feature.getName()))
				.collect(Collectors.toList());
		FeatureHandler def = super.getHandlerFor(feature);
		return (source, target, targetFeature) -> {
			List<Method> valid = filtered.parallelStream().filter((i) -> {
				Class<?>[] params = i.getParameterTypes();
				return params[0].isInstance(source) && params[1].isInstance(target);
			}).collect(Collectors.toList());
			if (valid.isEmpty()) {
				def.processFeature(source, target, targetFeature);
			} else {
				List<Method> mostSpecific = valid.parallelStream().filter((i) -> {
					Class<?>[] iParams = i.getParameterTypes();
					return valid.stream().allMatch((j) -> {
						Class<?>[] jParams = j.getParameterTypes();
						return jParams[0].isAssignableFrom(iParams[0]) && jParams[1].isAssignableFrom(iParams[1]);
					});
				}).collect(Collectors.toList());
				if (mostSpecific.size() == 1) {
					try {
						mostSpecific.get(0).invoke(this, source, target);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						throw new AssertionError(mostSpecific.get(0).toString(), e);
					}
				} else {
					throw new AssertionError();
				}
			}
		};
	}
}
