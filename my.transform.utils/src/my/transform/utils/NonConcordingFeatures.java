package my.transform.utils;

import org.eclipse.emf.ecore.EStructuralFeature;

public class NonConcordingFeatures extends IllegalArgumentException {
	private static final long serialVersionUID = 8071536700108622213L;

	private final EStructuralFeature sourceFeature;
	private final EStructuralFeature targetFeature;

	public NonConcordingFeatures(EStructuralFeature sourceFeature, EStructuralFeature targetFeature) {
		super();
		this.sourceFeature = sourceFeature;
		this.targetFeature = targetFeature;
	}

	public EStructuralFeature getSourceFeature() {
		return sourceFeature;
	}

	public EStructuralFeature getTargetFeature() {
		return targetFeature;
	}
	
}
