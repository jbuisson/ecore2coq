package my.transform.utils;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMapUtil;

import my.transform.utils.activator.TransformActivator;
import my.transform.utils.helper.HashCache;

public class HookableTransformer {
	@FunctionalInterface
	public static interface FeatureHandler {
		void processFeature(EObject source, EObject target, EStructuralFeature targetFeature);
	}

	private final AtomicBoolean used = new AtomicBoolean(false);
	private final AtomicBoolean finishing = new AtomicBoolean(false);
	private final Map<EObject, EObject> copies = new ConcurrentHashMap<>();
	private final FeatureHandler defaultFeatureHandler = this::genericProcessFeature;
	private final Queue<Runnable> postActions = new ConcurrentLinkedQueue<>();
	private final Queue<Runnable> finishingActions = new ConcurrentLinkedQueue<>();
	private final HashCache<EStructuralFeature, FeatureHandler> handlerCache = new HashCache<>(this::getHandlerFor);

	protected final void registerFinishingAction(Runnable r) {
		finishingActions.add(r);
	}

	protected final void registerReferenceAction(Runnable r) {
		if (finishing.get()) {
			throw new AssertionError();
		}
		postActions.add(r);
	}

	protected EObject hookFindTransformedOf(EObject o) {
		return null;
	}

	protected final EObject findTransformedOf(EObject o) {
		EObject r = copies.get(o);
		if (r == null) {
			EObject s = hookFindTransformedOf(o);
			if (s == null) {
				TransformActivator.warning("no mapping for " + o);
			}
			return s;
		} else {
			return r;
		}
	}

	protected void prevalidate(EObject o) {
	}

	public final EObject transform(EObject o) {
		if (used.compareAndSet(false, true)) {
			prevalidate(o);
			EObject r = transformSubtree(o);
			runPostActions();
			if (finishing.compareAndSet(false, true)) {
				runFinishingActions();
				return r;
			} else {
				throw new AssertionError();
			}
		} else {
			throw new AssertionError();
		}
	}

	protected FeatureHandler getHandlerFor(EStructuralFeature feature) {
		return defaultFeatureHandler;
	}

	private EObject transformSubtree(EObject source) {
		if (finishing.get()) {
			throw new AssertionError();
		}
		if (copies.containsKey(source)) {
			TransformActivator.warning("already done: " + source.toString());
		}
		EClass remappedEClass = remapEClass(source.eClass());
		EObject target = EcoreUtil.create(remappedEClass);
		cloneInto(source, target);
		return target;
	}

	protected void hookMapping(EObject source, EObject target) {
	}

	protected final void registerTransformedOf(EObject source, EObject target) {
		copies.put(source, target);
		hookMapping(source, target);
	}

	protected final void cloneInto(EObject source, EObject target) {
		registerTransformedOf(source, target);
		if (finishing.get()) {
			throw new AssertionError();
		}
		EClass targetEClass = target.eClass();
		targetEClass.getEAllStructuralFeatures().stream()
				.forEach((targetFeature) -> processOneFeature(source, target, targetFeature));
	}

	private void processOneFeature(EObject source, EObject target, EStructuralFeature targetFeature) {
		if (targetFeature.isChangeable() && !targetFeature.isDerived()) {
			FeatureHandler fh = handlerCache.apply(targetFeature);
			if (targetFeature instanceof EReference && !((EReference) targetFeature).isContainment()) {
				registerReferenceAction(() -> fh.processFeature(source, target, (EReference) targetFeature));
			} else {
				fh.processFeature(source, target, targetFeature);
			}
		}
	}

	private void runPostActions() {
		runActions(postActions);
	}

	private void runFinishingActions() {
		runActions(finishingActions);
	}

	private void runActions(Queue<Runnable> actions) {
		while (!actions.isEmpty()) {
			Runnable i = actions.remove();
			i.run();
		}
	}

	protected final Function<EObject, EObject> maybeTransform(EReference sourceReference, EReference targetReference) {
		if (finishing.get()) {
			throw new AssertionError();
		}
		if (targetReference.isContainment()) {
			return this::transformSubtree;
		} else if (!sourceReference.isContainment() && !targetReference.isContainment()) {
			return this::findTransformedOf;
		} else {
			throw new AssertionError(new NonConcordingFeatures(sourceReference, targetReference));
		}
	}

	private void genericProcessFeature(EObject source, EObject target, EStructuralFeature targetFeature) {
		if (finishing.get()) {
			throw new AssertionError();
		}
		Optional<EStructuralFeature> optionalSourceFeature = revertEStructuralFeature(source.eClass(), targetFeature);
		optionalSourceFeature.ifPresent((sourceFeature) -> handleFeature(source, target, targetFeature, sourceFeature));
	}

	private void handleFeature(EObject source, EObject target, EStructuralFeature targetFeature,
			EStructuralFeature sourceFeature) throws AssertionError {
		if (source.eIsSet(sourceFeature)) {
			if (targetFeature instanceof EAttribute && sourceFeature instanceof EAttribute) {
				if (FeatureMapUtil.isFeatureMap(sourceFeature) && FeatureMapUtil.isFeatureMap(targetFeature)) {
					throw new AssertionError();
				} else if (!FeatureMapUtil.isFeatureMap(sourceFeature) && !FeatureMapUtil.isFeatureMap(targetFeature)) {
					target.eSet(targetFeature, source.eGet(sourceFeature));
				} else {
					throw new AssertionError(new NonConcordingFeatures(sourceFeature, targetFeature));
				}
			} else if (targetFeature instanceof EReference && sourceFeature instanceof EReference) {
				EReference targetReference = (EReference) targetFeature;
				EReference sourceReference = (EReference) sourceFeature;
				Function<EObject, EObject> transformer = maybeTransform(sourceReference, targetReference);
				if (sourceReference.isMany() && targetReference.isMany()) {
					@SuppressWarnings("unchecked")
					List<EObject> sourceList = (List<EObject>) source.eGet(sourceReference);
					List<EObject> targetList = sourceList.stream().map(transformer).collect(Collectors.toList());
					target.eSet(targetFeature, targetList);
				} else if (!sourceReference.isMany() && !targetReference.isMany()) {
					target.eSet(targetFeature, transformer.apply((EObject) source.eGet(sourceFeature)));
				} else {
					throw new AssertionError(new NonConcordingFeatures(sourceFeature, targetFeature));
				}
			} else {
				throw new AssertionError(new NonConcordingFeatures(sourceFeature, targetFeature));
			}
		}
	}

	protected EClass remapEClass(EClass c) {
		return c;
	}

	protected final EClass remapEClass(EClass c, Map<EClass, EClass> map, Set<EClass> forbiden) {
		if (forbiden.contains(c)) {
			throw new AssertionError(c.getName());
		} else {
			EClass ret = map.get(c);
			if (ret == null) {
				throw new AssertionError(c.getName());
			} else {
				return ret;
			}
		}
	}

	protected Optional<EStructuralFeature> revertEStructuralFeature(EClass sourceClass,
			EStructuralFeature targetFeature) {
		if (targetFeature.getEContainingClass().isSuperTypeOf(sourceClass)) {
			return Optional.of(targetFeature);
		} else {
			return Optional.ofNullable(sourceClass.getEStructuralFeature(targetFeature.getName()));
		}
	}
}
