package my.transform.utils;

import java.util.Map;
import java.util.TreeMap;
import java.util.function.Supplier;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

public class MyResourceSetImpl extends ResourceSetImpl {
	private Map<String, Supplier<Resource>> exceptions = new TreeMap<>();

	public MyResourceSetImpl() {
		exceptions.put("Ecore.ecore", () -> EcorePackage.eINSTANCE.eResource());
		exceptions.put("XMLType.ecore", () -> XMLTypePackage.eINSTANCE.eResource());
	}

	@Override
	public Resource getResource(URI uri, boolean loadOnDemand) {
		return handleExceptions(uri, () -> super.getResource(uri, loadOnDemand));
	}

	@Override
	protected Resource delegatedGetResource(URI uri, boolean loadOnDemand) {
		return handleExceptions(uri, () -> super.delegatedGetResource(uri, loadOnDemand));
	}

	private Resource handleExceptions(URI uri, Supplier<Resource> normalCourse) {
		// Hack: otherwise, the loaded resource does not properly merge with
		// the Ecore meta-model
		Supplier<Resource> exceptional = exceptions.get(uri.lastSegment());
		if (exceptional == null) {
			return normalCourse.get();
		} else {
			return exceptional.get();
		}
	}
}
