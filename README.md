# README #

This repository is a collection of Eclipse plugins in order to transform an Ecore metamodel into a Coq script that define a set of inductive types.

Assume an Ecore metamodel named `M`. The transformation results in a set of types `T`.
Now assume an instance `I` of `M`.  The transformation is such that `I` can be transformed into a value `V` whose type is `T`.

### Short description of the plugins ###

This repository contains several plugins. They can be classified depending on their role:

* Ecore-to-Coq transformation
    * `my.ecore2coq.ui` is the UI frontend. It adds a command to Eclipse's popup menus. The associated handler triggers all the transformation steps till Coq script generation. The `MyAbstractTransformerHandler` class implements a simple workflow controller, instantiated by the tasks listed in the `AllTheTransformations` class.
    * `my.ecore2coq` contains the nine first steps of the transformation, along with the involved metamodels. Each of these transformation steps is programmed by a single class, in the `my.ecore2coq` package:
        1. `E2L` links all the referenced Ecore metamodels in a single model, and replaces the `EPackage` concept with a naming scheme.
	2. `L2G` normalizes the representation of (generic) types, replacing Ecore's `EGenericType`.
	3. `G2S` removes any non-structural aspect from the metamodel. Removed elements include operations, derived features, transient features, volatile features, opposite features.
	4. `S2NR` removes cross-references from the metamodel. They are replaced with URIs. At the same time, multiplicities are expanded to lists (or other suitable containers).
	5. `NR2I` creates an implementation for each concrete class in the metamodel.
	6. `I2CI` percolates type parameters and structural features from classes to implementations, taking into account inheritance.
	7. `CI2F` duplicates implementations to attach them to all the specialized class. After this step, each class contains the exact set of its implementations.
	8. `F2J` translates the metamodel to the vocabulary of inductive types: classes are now called inductive types, and implementations are called constructors.
	9. `J2SI` groups and schedules inductive types, such that each inductive type (and its constructors) refers only to types in the same group or in preceding groupe.
    * `my.ecore2coq.gallina` is the tenth step. It contains a QVT operational transformation that issues Gallina/Vernacular commands and expressions.
        10. `SI2Gallina` invokes `si2gallina.qvto`, which builds Gallina expressions (mainly for constructors' types) and issues Vernacular commands.
    * `my.gallina` contains our home-made Gallina/Vernacular metamodel, as well as the Acceleo-based model-to-text generator.
        11. `coqgen.mtl` generates the textual Coq script.
    * `my.transform.utils` is our own Java-based transformation framework. It builds on deep copying a tree of EMF objects, with customizable hooks.
* Supporting tools
    * `my.coverage` is a tool that assesses how much a metamodel is covered by a set of models. It basically counts how many classes are instantiated and how many features are set.
    * `my.qvtcoverage.ui` loads QVTO coverage data and displays the covare view.
    * `my.gallina.ui` is an ad-hoc tools that is used to generate the standard library used by `my.ecore2coq.gallina` and `my.gallina`, from its XMI representation.
* Tests
    * `my.ecore2coq.test` duplicates the metamodels involved in the transformation, for testing purpose.
    * `my.ecore2coq.lambda` contains a metamodel for the lambda-calculus.
    * `my.xtext.lambda` contains an Xtext project that is used to generate a metamodel for the lambda-calculus.
    * `my.coq.lambda` contains a reference, hand-written, Coq script for the lambda-calculus.
* Miscellaneous
    * `my.drawing` contains a couple of useless class diagrams.

### Requirements ###

* Java 8 or above
* [Eclipse](http://eclipse.org)
    * Oxygen is known to be supported. The plugins may run on Neon as well.
    * Eclipse Modeling Tools is a good bundle to start with, augmented with the following features:
        * Acceleo
        * QVT Operational Code Coverage Tool
        * QVT Operational SDK
        * Xtext Complete SDK
* [Coq 8.6](http://coq.inria.fr)

### Set up ###

Depending on the needs, up to 3 instances of Eclipse might be launched nested into one another.

In each workspace, start with defining an [API baseline](http://help.eclipse.org/oxygen/index.jsp?topic=%2Forg.eclipse.pde.doc.user%2Ftasks%2Fapi_tooling_baseline.htm). To do so, open the `Window` menu and select `Preferences`. Under the `Plug-in Development` category, open the `API Baselines` page. Trigger the `Add Baseline...` button to open the wizard. Choose the `Target platform` option. Then name the new baseline and populate it using the `Running platform` tick.

1. _The first instance of Eclipse is optional._ It runs a workspace that contains only the `my.qvtcoverage.ui` project.
2. The second instance of Eclipse runs a workspace that contains the following projects: `my.ecore2coq.ui`, `my.ecore2coq`, `my.ecore2coq.gallina`, `my.gallina`, `my.transform.utils`, and `my.coverage`. It can be launched directly, or, it can be launched by the first instance of Eclipse if coverage data for the `my.ecore2coq.gallina/transforms/si2gallina.qvto` transformation need to be visualized.
    - Generate the EMF code:
        1. In the `my.ecore2coq` project, open the `model/derived.genmodel` file. Right-click on the root (named `Derived`) of this model, and select `Generate Model code`.
        2. In the `my.gallina` project, open the `model/gallina.genmodel` file. Right-click on the root (named `Gallina`) of this model, and select `Generate Model code`.
        3. In the `my.coverage` project, open the `model/Coverage.genmodel` file. Right-click on the root (named `Coverage`) of this model, and select `Generate Model code`.
    - Set up the metamodel mappings:
        1. Right-click on the `my.ecore2coq.gallina` project, then select `Properties`
	2. In the `QVT Settings` category, open the `Metamodel Mappings` page.
	3. Using the `Add` button, define the two following mappings:
	    - Source model URI `http://my/ScheduledInductive` mapped to `platform:/resource/my.ecore2coq/model/ScheduledInductive.ecore`
	    - Source model URI `http://my/gallina` mapped to `platform:/resource/my.gallina/model/gallina.ecore`
3. The third instance of Eclipse, launched by the second one, is the environment provided to end-users. For testing purpose, the workspace may contains `my.ecore2coq.test` and `my.ecore2coq.lambda`. In order to trigger the transformation, right-click on an Ecore metamodel, e.g., in package explorer, and, in the popup menu, select `Ecore2Coq` then `Ecore to all`. All the resulting files, including all the intermediate models, are stored in the same folder as the transformed Ecore metamodel. Last, the resulting Coq script (the `.v` file) can be compiled in a terminal, outside of Eclipse, using the `coqc` command.

Once the plugins are packaged, only the third instance of Eclipse is used.

The workspaces can be populated, after the repository has been locally cloned, using Eclipse's `Import` command, selecting the `Existing projects into workspace` wizard.