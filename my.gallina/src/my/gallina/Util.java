package my.gallina;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;

import gallina.File;
import gallina.GallinaPackage;

public class Util {
	private static Map<?, ?> saveOptions() {
		Map<String, Object> r = new HashMap<>();
		r.put(XMLResource.OPTION_URI_HANDLER, new URIHandlerImpl.PlatformSchemeAware());
		return r;
	}

	public static void updateCoqLib(ResourceSet resourceSet, URI target) throws IOException {
		URI source = URI.createPlatformPluginURI("my.gallina/model/CoqLib.xmi", false);
		Resource r = resourceSet.getResource(source, true);
		Resource t = resourceSet.createResource(target);
		t.getContents().addAll(EcoreUtil.copyAll(r.getContents()));
		t.save(saveOptions());
	}

	public static CoqLib loadCoqLib(ResourceSet resourceSet) {
		Resource r = loadCoqLibResource(resourceSet);
		return new CoqLib((File) r.getContents().get(0));
	}

	public static Resource loadCoqLibResource(ResourceSet resourceSet) {
		resourceSet.getPackageRegistry().put("http://my/gallina", GallinaPackage.eINSTANCE);
		URI source = URI.createPlatformPluginURI("my.gallina/model/CoqLib.xmi", false);
		Resource r = resourceSet.getResource(source, true);
		return r;
	}
}
