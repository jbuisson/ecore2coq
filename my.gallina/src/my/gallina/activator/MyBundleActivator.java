package my.gallina.activator;

import java.util.Map;
import java.util.stream.Stream;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class MyBundleActivator extends Plugin implements BundleActivator {
	private static MyBundleActivator instance = null;

	public static MyBundleActivator getInstance() {
		return instance;
	}

	public MyBundleActivator() {
		instance = this;
	}

	@Override
	public void start(BundleContext context) throws Exception {
		Map<String, Object> extensionToFactoryMap = Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap();
		Stream.of("gallina").forEach((s) -> {
			extensionToFactoryMap.put(s, new XMIResourceFactoryImpl());
		});
	}

	@Override
	public void stop(BundleContext context) throws Exception {
	}

	public static void error(Throwable t) {
		String pluginID = getInstance().getBundle().getSymbolicName();
		getInstance().getLog().log(new Status(IStatus.ERROR, pluginID, t.getMessage(), t));
	}
}
