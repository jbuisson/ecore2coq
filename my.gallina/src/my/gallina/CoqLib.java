package my.gallina;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import gallina.Binder;
import gallina.Command;
import gallina.Constructor;
import gallina.Definition;
import gallina.File;
import gallina.Inductive;
import gallina.InductiveBody;

public class CoqLib {
	private final Map<String, Binder> lib;

	public CoqLib(File f) {
		lib = f.getCommands().stream().flatMap(CoqLib::binders)
				.collect(Collectors.toConcurrentMap(Binder::getName, (i) -> i));
	}

	private static Stream<Binder> binders(Command c) {
		if (c instanceof Definition) {
			return Stream.of(((Definition) c).getBinder());
		} else if (c instanceof Inductive) {
			return ((Inductive) c).getBodies().stream().flatMap(CoqLib::binders);
		} else {
			throw new AssertionError(c.toString());
		}
	}

	private static Stream<Binder> binders(InductiveBody b) {
		return Stream.concat(Stream.of(b.getBinder()), b.getConstructors().stream().map(Constructor::getBinder));
	}

	public Binder get(String name) {
		return lib.get(name);
	}
}
